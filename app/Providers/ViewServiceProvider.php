<?php


namespace App\Providers;

use View;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
    public function register()
    {
        View::composer('*', function ($view) {
            return $view->with(app('view_service')->all());
        });
    }

    public function boot()
    {

    }
}