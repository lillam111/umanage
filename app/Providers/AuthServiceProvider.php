<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
    * The policy mappings for the application.
    *
    * @var array
    */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
    * Register any authentication / authorization services.
    *
    * @return void
    */
    public function boot()
    {
        $this->loadPolicies();
        $this->registerPolicies();
    }

    public function loadPolicies()
    {
        $gates = config('gates');
        foreach ($gates as $gate => $gate_level) {
            // Get the gate that we are attempting to define.
            $defined_gate = explode('\\', $gate);
            $defined_gate = $defined_gate[array_key_last($defined_gate)];
            // Define the gate that we have just iterated over.
            Gate::define($defined_gate, $gate);
        }
    }
}
