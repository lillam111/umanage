<?php

namespace App\Gates\Task;

use App\Gates\MasterGate;
use App\Models\Task\Task;
use App\Models\User\User;

class TaskGate extends MasterGate
{
    public function __construct(){}

    /**
    * Permission check to see if the authenticated user is able to view the particular task. (this will require
    * checking if the user is a part of the project, or the reporter of the task or the current task assignee)
    * if this passes true then the user has ability to view the task in question
    *
    * @param User $self
    * @param Task $task
    * @return bool
    */
    public function viewTask(User $self, Task $task): bool
    {
        if (
            $self->id === $task->reporter_user_id ||
            $self->id === $task->assigned_user_id
        ) {
            return true;
        }

        if ($self->can('ProjectController@viewProject', $task->project)) {
            return true;
        }

        return false;
    }

    /**
    * Permission check to see if the authenticated user is able to edit the particular task. (this will require
    * checking if the user is a part of the project, or the reporter of the task or the current task assignee)
    * if this passes true then the user has ability to edit the task in question
    *
    * @param User $self
    * @param Task $task
    * @return bool
    */
    public function editTask(User $self, Task $task): bool
    {
        if ($this->viewTask($self, $task)) {
            return true;
        }

        return false;
    }
}