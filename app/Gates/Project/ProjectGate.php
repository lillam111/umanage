<?php

namespace App\Gates\Project;

use App\Gates\MasterGate;
use App\Models\User\User;
use App\Models\Project\Project;

class ProjectGate extends MasterGate
{
    public function __construct(){}

    /**
    * Permission check to see if the authenticated user is able to view the particular project. (this will require
    * checking if the user is a part of the project, or the creator of the project. if this passes true then the user
    * has ability to view the project in question.
    *
    * @param User $self
    * @param Project $project
    * @return bool
    */
    public function viewProject(User $self, Project $project): bool
    {
        if ($this->check($self, 1)) {
            return true;
        }

        if (
            $project->creator_user_id === $self->id ||
            in_array($self->id, $project->user_contributors->pluck('user_id')->toArray())
        ) {
            return true;
        }

        return false;
    }

    /**
    * Permission check to see if the authenticated user is able to edit the particular project. (this will require
    * checking if the user is a part of the project, or the creator of the project. if this passes true then the user
    * has ability to edit the project in question.
    *
    * @param User $self
    * @param Project $project
    * @return bool
    */
    public function editProject(User $self, Project $project): bool
    {
        if ($this->viewProject($self, $project)) {
            return true;
        }

        if ($this->check($self, 1)) {
            return true;
        }

        return false;
    }
}