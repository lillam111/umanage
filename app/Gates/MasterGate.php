<?php

namespace App\Gates;

use App\Models\User\User;
use App\Models\Permission\Power;

class MasterGate
{
    public function __construct()
    {
        dd('here');
    }

    /**
    * @param User $self
    * @param $level
    * @return bool
    */
    public function check(User $self, $level): bool
    {
        $backtrace = debug_backtrace()[1];

        $class = $backtrace['class'];
        $function = $backtrace['function'];

        $self_roles = $self->user_role->role->powers->map(function ($power) {
            return $power->power;
        })->keyBy('name');

        if ($self_roles->get("$class@$function") instanceof Power) {
            return true;
        }

        return false;
    }
}