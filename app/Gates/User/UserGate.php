<?php

namespace App\Gates\User;

use App\Gates\MasterGate;
use App\Models\User\User;

class UserGate extends MasterGate
{
    public function __construct(){}

    /**
    * Permission to check to see whether or not the authenticated user is able to view the particular user (this will
    * require checking to see if the user is their own user, or if the user that is trying to look has any form of
    * permission in place that grants them the ability to do so)
    *
    * @param User $self
    * @param User $user
    * @return bool
    */
    public function viewUser(User $self, User $user): bool
    {
        if ($self->id === 1 || $self->id === $user->id) {
            return true;
        }
        return false;
    }

    /**
    * Permission to check to see whethher or not the authenticated user is able to edit the particular user (this will
    * require checking to see if the user is their own user, or or if the user that is trying to edit has any form of
    * permission in place that grants them the ability to do so).
    *
    * @param User $self
    * @param User $user
    * @return bool
    */
    public function editUser(User $self, User $user): bool
    {
        if ($self->id === 1 || $self->id === $user->id) {
            return true;
        }
        return false;
    }
}