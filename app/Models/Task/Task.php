<?php

namespace App\Models\Task;

use App\Models\User\User;
use App\Models\Project\Project;
use App\Models\Timelog\Timelog;
use Illuminate\Database\Eloquent\Model;
use App\Repositories\Task\TaskLogRepository;

class Task extends Model
{
    /**
    * pre-defining the table for this model, this would naturally be 'tasks' in the migration, and has been deliberately
    * modified as task. (singular) as we are bringing back multiple (task)s.
    *
    * @var string
    */
    protected $table = 'task';

    /**
    * This will contain everything that can be inserted into the table, this will be everything other than the
    * timestamp fields (created_at, updated_at). everything else that gets added will want to also be added into here
    *
    * @var array
    */
    protected $fillable = [
        'project_id',
        'name',
        'description',
        'task_issue_type_id',
        'reporter_user_id',
        'assigned_user_id',
        'task_status_id',
        'task_priority_id',
        'due_date',
    ];

    /**
    * These are the casts that the values in the database are going to come into the system as. All elements against
    * this model that is stored within the database are going to want to have their default defined cast.
    *
    * @var array
    */
    protected $casts = [
        'project_id' => 'int',
        'task_issue_type_id' => 'int',
        'reporter_user_id' => 'int',
        'assigned_user_id' => 'int',
        'task_status_id' => 'int',
        'task_priority_id' => 'int',
        'due_date' => 'datetime',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];

    /*
    * -----------------------------------------------------------------------------------------------------------------
    * Getters
    *
    * Logic from this point until the next titling is 100% to do with getting information around the specific model in
    * question, in this case: the Task
    *
    * -----------------------------------------------------------------------------------------------------------------
    */

    /*
    * -----------------------------------------------------------------------------------------------------------------
    * Logging
    *
    * Logic from this point until the next titling is 100% to do with logging information around the specific model in
    * question, in this case: the Task
    *
    * -----------------------------------------------------------------------------------------------------------------
    */

    /**
    * This method is specifically for logging any change against a task, all we need to do is pass in a log type,
    * (constant of the logging type), an old and a new value... the rest will take care of itself. This method isn't set
    * to return anything, no value is necessary.
    *
    * @param $log_type
    * @param null $log_old
    * @param null $log_new
    * @return void
    */
    public function log($log_type, $log_new = null, $log_old = null)
    {
        TaskLogRepository::logTask([
            'project_id' => $this->project_id,
            'task_id' => $this->id,
            'old' => $log_old,
            'new' => $log_new,
            'type' => $log_type,
            'extra' => $this->name
        ]);
    }

    /*
    * -----------------------------------------------------------------------------------------------------------------
    * Relationships
    *
    * The information from this point on will 100% be around the relationships that this specific model has. In this
    * specific instance: the Task
    *
    * -----------------------------------------------------------------------------------------------------------------
    */

    /**
    * This method is used so that we can get the direct project that this task is assigned to, this is useful for when
    * we are going to be looking at tasks on a singular basis and need to reference where this project comes from
    * as well as being able to simply grab the task's project badge.
    *
    * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    */
    public function project()
    {
        return $this->belongsTo(Project::class, 'project_id', 'id');
    }

    /**
    * Each task will have an assigned user, and providing that the task has an assigned user, then we are going to be
    * pre-fetching the user in question which will be responsible for this task in question, this will be for ease of
    * access to the user that is assigned to this particular task.
    *
    * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    */
    public function task_assigned_user()
    {
        return $this->belongsTo(User::class, 'assigned_user_id', 'id');
    }

    /**
    * Each task will have a reporter user, this is the user which has been the one who had made the task to begin with
    * this won't be changable at all, and the reporter will always remain the same, this will be for ease of access
    * to the user in question that had reported the task.
    *
    * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    */
    public function task_reporter_user()
    {
        return $this->belongsTo(User::class, 'reporter_user_id', 'id');
    }

    /**
    * A task can have many watchers, and this will be collecting all the watchers that are assigned to this task, this
    * table is a connector table, and doesn't have access to the direct user after calling this, thus you will need to
    * run task_watcher_user->user->something... in order to utilise the user model on this, this is just a pivot table.
    *
    * @return \Illuminate\Database\Eloquent\Relations\HasMany
    */
    public function task_watcher_users()
    {
        return $this->hasMany(TaskWatcherUser::class, 'task_id', 'id');
    }

    /**
    * Each task will be assigned an issue type, a type of issue that we are going to be working with, whether it's a
    * new feature, or a bug... then we know what task wants working first, or gives the user the ability to prioritise
    * the tasks that they want to work on first, this will give us ease of access to the TaskIssueType model.
    *
    * @return \Illuminate\Database\Eloquent\Relations\HasOne
    */
    public function task_issue_type()
    {
        return $this->hasOne(TaskIssueType::class, 'id', 'task_issue_type_id');
    }

    /**
    * Each task will be assigned a status, whether that is to do, in progress, deleted or done, possibly more to come
    * in future times, however, a status will always be assigned thus we are going to need direct ease of access
    * to this particular model.
    *
    * @return \Illuminate\Database\Eloquent\Relations\HasOne
    */
    public function task_status()
    {
        return $this->hasOne(TaskStatus::class, 'id', 'task_status_id');
    }

    /**
    * Each task will have a variety of checklists against them, which allows to have sub tasks within a task that is the
    * global, with this mind when looking at tasks you can also view the checklists of the task, this will have a sub
    * table that connects to task_checklist_item which will allow the ability to check and uncheck them as a list of
    * to do's
    *
    * @return \Illuminate\Database\Eloquent\Relations\HasMany
    */
    public function task_checklists()
    {
        return $this->hasMany(TaskChecklist::class, 'task_id', 'id')
            ->orderBy('order');
    }

    /**
    * Each task will have a set priority, and this method allows to efficiently have a priority against a task, each
    * task will have a priority set by default, which will be required when setting up a task, the default will be
    * medium, and then from there you can choose whether or not it wants to be higher or lower priority. this connects
    * to the task priority table and gives us access to the task priority model.
    *
    * @return \Illuminate\Database\Eloquent\Relations\HasOne
    */
    public function task_priority()
    {
        return $this->hasOne(TaskPriority::class, 'id', 'task_priority_id');
    }

    /**
    * Each task can have endless comments against them... this method allows us to efficiently grab all task comments
    * against a single task so that when looking over a task we can effectively look over the task comments as well.
    *
    * @return \Illuminate\Database\Eloquent\Relations\HasMany
    */
    public function task_comments()
    {
        return $this->hasMany(TaskComment::class, 'task_id', 'id');
    }

    /**
    * It is possible to log time against tasks, and with this in mind, when a user is logging multiple times against a
    * task, or even multiple users are logging time against this task, then we are going to want to collect all timelog
    * entries that were created for this task and collate them together to get a total amount of time worked on this.
    * this will be joined on task_id...
    *
    * @return \Illuminate\Database\Eloquent\Relations\HasMany
    */
    public function task_timelogs()
    {
        return $this->hasMany(Timelog::class, 'task_id', 'id');
    }
}
