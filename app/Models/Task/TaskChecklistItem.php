<?php

namespace App\Models\Task;

use Illuminate\Database\Eloquent\Model;
use App\Repositories\Task\TaskLogRepository;

class TaskChecklistItem extends Model
{
    /**
    * Set the table to the singular variation of task_checklist_items as we only store singular item (as a multiple)
    * and is better to call it by it's singular entity value. this needs setting as this model will be looking at the
    * table being "task_checklist_items"...
    *
    * @var string
    */
    protected $table = 'task_checklist_item';

    /**
    * This will contain everything that can be inserted into the table, this will be everything other than the
    * timestamp fields (created_at, updated_at). everything else that gets added will want to also be added into here
    *
    * @var array
    */
    protected $fillable = [
        'task_checklist_id',
        'name',
        'order',
        'is_checked'
    ];

    /**
    * This will contain everything that the database entries have, each element in the database will want an assigned
    * data type, and it is better to cast it to its' default datatype before we start manipulating methods and checks.
    *
    * @var array
    */
    protected $casts = [
        'task_checklist_id' => 'int',
        'name' => 'string',
        'order' => 'int',
        'is_checked' => 'boolean',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];

    /*
    * -----------------------------------------------------------------------------------------------------------------
    * Getters
    *
    * Logic from this point until the next titling is 100% to do with getting information around the specific model in
    * question, in this case: the TaskChecklistItem
    *
    * -----------------------------------------------------------------------------------------------------------------
    */

    /*
    * -----------------------------------------------------------------------------------------------------------------
    * Logging
    *
    * Logic from this point until the next titling is 100% to do with logging information around the specific model in
    * question, in this case: the TaskChecklistItem
    *
    * -----------------------------------------------------------------------------------------------------------------
    */

    /**
    * This method is specifically for logging any change against a task checklist item, all we need to do is pass in a
    * log type, (constant of the logging type), an old and a new value... the rest will take care of itself. This
    * method isn't set to return anything, no value is necessary.
    *
    * @param $log_type
    * @param null $log_old
    * @param null $log_new
    * @return void
    */
    public function log($log_type, $log_new = null, $log_old = null)
    {
        TaskLogRepository::logTask([
            'project_id' => $this->task_checklist->task->project_id,
            'task_id' => $this->task_checklist->task_id,
            'task_checklist_id' => $this->task_checklist_id,
            'task_checklist_item_id' => $this->id,
            'type' => $log_type,
            'old' => $log_old,
            'new' => $log_new,
            'extra' => $this->name
        ]);
    }

    /*
    * -----------------------------------------------------------------------------------------------------------------
    * Relationships
    *
    * The information from this point on will 100% be around the relationships that this specific model has. In this
    * specific instance: the TaskChecklistItem
    *
    * -----------------------------------------------------------------------------------------------------------------
    */

    /**
    * Each checklist item will belong to a checklist, this item is entirely for cascading back up to the top chain
    * from where this checklist item comes from, we can do checklist_item->checklist->task->project all the way to
    * where this really belongs. This is entirely for convenience and will be rarely used.
    *
    * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    */
    public function task_checklist()
    {
        return $this->belongsTo(TaskChecklist::class, 'task_checklist_id', 'id');
    }
}
