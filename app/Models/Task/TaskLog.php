<?php

namespace App\Models\Task;

use App\Models\Project\Project;
use App\Models\User\User;
use Illuminate\Database\Eloquent\Model;

class TaskLog extends Model
{
    // Task oriented constants.
    const TASK_MAKE = 1;
    const TASK_NAME = 2;
    const TASK_DESCRIPTION = 3;
    const TASK_DUE_DATE = 4;
    const TASK_DELETE = 5;

    // Task checklist oriented constants.
    const TASK_CHECKLIST_MAKE = 6;
    const TASK_CHECKLIST_NAME = 7;
    const TASK_CHECKLIST_DELETE = 8;

    // task checklist item oriented constants.
    const TASK_CHECKLIST_ITEM_MAKE = 9;
    const TASK_CHECKLIST_ITEM_NAME = 10;
    const TASK_CHECKLIST_ITEM_CHECKED = 11;
    const TASK_CHECKLIST_ITEM_DELETE = 12;

    /**
    * @var string
    */
    protected $table = 'task_log';

    /**
    * @var array
    */
    protected $fillable = [
        'user_id',
        'task_id',
        'project_id',
        'task_checklist_id',
        'task_checklist_item_id',
        'type',
        'old',
        'new',
        'extra',
        'when'
    ];

    /**
    * @var array
    */
    protected $casts = [
        'user_id' => 'int',
        'project_id' => 'int',
        'task_id' => 'int',
        'task_checklist_id' => 'int',
        'task_checklist_item_id' => 'int',
        'type' => 'int',
        'old' => 'string',
        'new' => 'string',
        'extra' => 'string',
        'when' => 'datetime'
    ];

    /**
    * @var bool
    */
    public $timestamps = false;

    /*
    * -----------------------------------------------------------------------------------------------------------------
    * Getters
    *
    * Logic from this point until the next titling is 100% to do with getting information around the specific model in
    * question, in this case: the TaskLog
    *
    * -----------------------------------------------------------------------------------------------------------------
    */

    /*
    * -----------------------------------------------------------------------------------------------------------------
    * Checkers
    *
    * All  of these methods from below are strictly going to be for checking whether ot not something is a specific type
    * of update, item etc etc.
    *
    * -----------------------------------------------------------------------------------------------------------------
    */

    /**
    * Method for finding out whether or not this entry has the update type that fits into the task updates. and if
    * it does, then we are working with tasks... this method is simply going to return either true or false depending on
    * type
    *
    * @return bool
    */
    public function isTaskEdit(): bool
    {
        if (in_array($this->type, [
            self::TASK_MAKE,
            self::TASK_NAME,
            self::TASK_DESCRIPTION,
            self::TASK_DUE_DATE,
            self::TASK_DELETE
        ])) {
            return true;
        }
        return false;
    }

    /**
    * Method for finding out whether or not this entry has the update type that fits into the task checklist updates
    * and if it does, then we are working with task checklists... this method is simply going to return either
    * true or false depending on type.
    *
    * @return bool
    */
    public function isTaskChecklistEdit(): bool
    {
        if (in_array($this->type, [
            self::TASK_CHECKLIST_MAKE,
            self::TASK_CHECKLIST_NAME,
            self::TASK_CHECKLIST_DELETE
        ])) {
            return true;
        }
        return false;
    }

    /**
    * Method for finding out whether or not this entry has the update type that fits into the task checklist item
    * updates and if it does, then we are working with task checklist items... this method is simply going to return
    * either true or false depending on type.
    *
    * @return bool
    */
    public function isTaskChecklistItemEdit(): bool
    {
        if (in_array($this->type, [
            self::TASK_CHECKLIST_ITEM_MAKE,
            self::TASK_CHECKLIST_ITEM_NAME,
            self::TASK_CHECKLIST_ITEM_CHECKED,
            self::TASK_CHECKLIST_ITEM_DELETE
        ])) {
            return true;
        }
        return false;
    }

    /*
    * -----------------------------------------------------------------------------------------------------------------
    * Relationships
    *
    * The information from this point on will 100% be around the relationships that this specific model has. In this
    * specific instance: the TaskLog
    *
    * -----------------------------------------------------------------------------------------------------------------
    */

    /**
    * Each task log will belong to a user; and when we bring back the logs we are going to need to know what user it was
    * that happened to make a change to the tasks in any way, so that we are able to report on it.
    *
    * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
    * Each task log will belong to a project, and when we bring back to logs we are going to need to know or at least
    * could potentially benefit from knowing which project that the logs belong to, this will allow us to acquire all
    * of a projects specific updates, and feed them to the project.
    *
    * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    */
    public function project()
    {
        return $this->belongsTo(Project::class, 'project_id', 'id');
    }

    /**
    * Each task log will need to belong to a task in some way, no matter the changes we are making anywhere around a
    * task it is a necessity that a task id is inserted into the database, in which, we can find out where directly
    * a change might have been made for the user... to report on this later.
    *
    * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    */
    public function task()
    {
        return $this->belongsTo(Task::class, 'task_id', 'id');
    }

    /**
    * Task logs can potentially be from a task checklist, a task id will also be assigned with this, however if the
    * task checklist id is present then we are going to be assuming that a change was made to the task checklist...
    * and we will report on this in kind depending on the situational data that we're dealing with.
    *
    * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    */
    public function task_checklist()
    {
        return $this->belongsTo(TaskChecklist::class, 'task_checklist_id', 'id');
    }

    /**
    * Task logs can potentially be from a task checklist item, a task id and a task checklist id will be assigned to
    * this also, if a task checklist id is present, then we know which checklist was modified when playing with the
    * checklist ids. likewise, we also know where the checklist resides when utilising the task id.
    *
    * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    */
    public function task_checklist_item()
    {
        return $this->belongsTo(TaskChecklistItem::class, 'task_checklist_item_id', 'id');
    }
}
