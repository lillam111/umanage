<?php

namespace App\Models\Task;

use App\Models\User\User;
use Illuminate\Database\Eloquent\Model;

class TaskComment extends Model
{
    /**
    * @var string
    */
    protected $table = 'task_comment';

    /**
    * This will want to contain everything that is fillable inside the database, if the element that should be fillable
    * is not inside the array, the element will not be insertable and will always enter as a null value, or error
    * if the element is a non nullable value
    *
    * @var array
    */
    protected $fillable = [
        'project_id',
        'task_id',
        'user_id',
        'parent_id',
        'content'
    ];

    /**
    * These are the casts that the values in the database are going to come into the system as. All elements against
    * this model that is stored within the database are going to want to have their default defined cast.
    *
    * @var array
    */
    protected $casts = [
        'parent_id' => 'int',
        'task_id' => 'int',
        'user_id' => 'int',
        'content' => 'string',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];

    /*
    * -----------------------------------------------------------------------------------------------------------------
    * Getters
    *
    * Logic from this point until the next titling is 100% to do with getting information around the specific model in
    * question, in this case: the TaskComment
    *
    * -----------------------------------------------------------------------------------------------------------------
    */

    /*
    * -----------------------------------------------------------------------------------------------------------------
    * Relationships
    *
    * The information from this point on will 100% be around the relationships that this specific model has. In this
    * specific instance: the TaskComment
    *
    * -----------------------------------------------------------------------------------------------------------------
    */

    /**
    * Each TaskComment that is made in the system, will be belonging to a task, a task comment cannot be made unless
    * it is against a task... this relationship will be for the efficiency of grabbing the task and where it belongs
    * should any system be in place to email this out, we can say this comment was made on this task, click here to
    * view (as an example).
    *
    * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    */
    public function task()
    {
        return $this->belongsTo(Task::class, 'task_id', 'id');
    }

    /**
    * Each TaskComment that is made in the system will be belonging to a user, a comment cannot be made unless it is
    * against a user... this relationship will be for the efficiency of grabbing the user; basically highlighting where
    * this comment came from and who made it, when it was made etc. (this will be highlighted on the visuals of task
    * comments)
    *
    * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
    * Comments taht have been replied to, will naturally have a parent  to which they have been assigned to. with  this
    * we are able to cascade back up to acquire the parent comment where the current comment will have came
    * from
    *
    * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    */
    public function parent()
    {
        return $this->belongsTo(TaskComment::class, 'parent_id', 'id');
    }

    /**
    * This method is for acquiring all replies that are against a comment, this is for convenience as well as being
    * able to map out all the replies of a comment. this will be as recursrive as possible. $comment->replies as $reply
    * $reply->replies etc. and keep it  going until there are no more commments...
    *
    * @return \Illuminate\Database\Eloquent\Relations\HasMany
    */
    public function replies()
    {
        return $this->hasMany(TaskComment::class, 'id', 'parent_id');
    }
}
