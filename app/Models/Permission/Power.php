<?php

namespace App\Models\Permission;

use Illuminate\Database\Eloquent\Model;

class Power extends Model
{
    /**
    * @var string
    */
    protected $table = 'power';

    /**
    * @var array
    */
    protected $fillable = [
        'name',
        'level'
    ];

    /**
    * @var array
    */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'level' => 'integer'
    ];

    /**
    * @var bool
    */
    public $timestamps = false;

    /*
    * -----------------------------------------------------------------------------------------------------------------
    * Relationships
    *
    * The information from this point on will 100% be around the relationships that this specific model has. In this
    * specific instance: the Power
    *
    * -----------------------------------------------------------------------------------------------------------------
    */

    /**
    * Each power will be assigned to a specific role, when we are looking at powers as a whole, and if they are ever
    * listed, we will be able to quickly  see with this permission which power has been assigned to which (roles) as
    * a plural. this  will be useful reporting to see what role can do what in the system.
    *
    * @return \Illuminate\Database\Eloquent\Relations\HasMany
    */
    public function roles()
    {
        return $this->hasMany(RolePower::class, 'power_id', 'id');
    }
}
