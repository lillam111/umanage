<?php

namespace App\Models\Permission;

use Illuminate\Database\Eloquent\Model;

class RolePower extends Model
{
    /**
    * @var string
    */
    protected $table = 'role_power';

    /**
    * @var array
    */
    protected $fillable = [
        'role_id',
        'power_id'
    ];

    /**
    * @var array
    */
    protected $casts = [
        'role_id' => 'integer',
        'power_id' => 'integer'
    ];

    /**
    * @var bool
    */
    public $timestamps = false;

    /*
    * -----------------------------------------------------------------------------------------------------------------
    * Relationships
    *
    * The information from this point on will 100% be around the relationships that this specific model has. In this
    * specific instance: the RolePower
    *
    * -----------------------------------------------------------------------------------------------------------------
    */

    /**
    * Each RolePower that exists in the system will have a single role that it belongs to, this method will be useful
    * for quickly returning the role itself; this will only have one. (this is a connector table connecting a role
    * to a power)
    *
    * @return \Illuminate\Database\Eloquent\Relations\HasOne
    */
    public function role()
    {
        return $this->hasOne(Role::class, 'role_id', 'id');
    }

    /**
    * Each RolePower that exists in the system will have a single power that it belongs to, this method will be useful
    * for quickly returning the power itself; this will oly have one (this is a connector table, connecting a power
    * to a role)
    *
    * @return \Illuminate\Database\Eloquent\Relations\HasOne
    */
    public function power()
    {
        return $this->hasOne(Power::class, 'id', 'power_id');
    }
}
