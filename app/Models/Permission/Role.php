<?php

namespace App\Models\Permission;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    /**
    * @var string
    */
    protected $table = 'role';

    /**
    * @var array
    */
    protected $fillable = [
        'name',
        'level'
    ];

    /**
    * @var array
    */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'level' => 'integer'
    ];

    /**
    * @var bool
    */
    public $timestamps = false;

    /*
    * -----------------------------------------------------------------------------------------------------------------
    * Relationships
    *
    * The information from this point on will 100% be around the relationships that this specific model has. In this
    * specific instance: the Role
    *
    * -----------------------------------------------------------------------------------------------------------------
    */

    /**
    * Each role will have a variety of users attached to them, this method will be acquiring all the users that exist
    * against a particular role. This method will mainly be used inside reporting purposes when looking at a list of
    * users and seeing what they're capable of doing.
    *
    * @return \Illuminate\Database\Eloquent\Relations\HasMany
    */
    public function users()
    {
        return $this->hasMany(RoleUser::class, 'user_id', 'id');
    }

    public function powers()
    {
        return $this->hasMany(RolePower::class, 'role_id', 'id');
    }
}
