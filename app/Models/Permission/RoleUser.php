<?php

namespace App\Models\Permission;

use App\Models\User\User;
use Illuminate\Database\Eloquent\Model;

class RoleUser extends Model
{
    /**
    * @var string
    */
    protected $table = 'role_user';

    /**
    * @var array
    */
    protected $fillable = [
        'role_id',
        'user_id'
    ];

    /**
    * @var array
    */
    protected $casts = [
        'role_id' => 'integer',
        'user_id' => 'integer'
    ];

    /**
    * @var bool
    */
    public $timestamps = false;

    /*
    * -----------------------------------------------------------------------------------------------------------------
    * Relationships
    *
    * The information from this point on will 100% be around the relationships that this specific model has. In this
    * specific instance: the RoleUser
    *
    * -----------------------------------------------------------------------------------------------------------------
    */

    /**
    * Each RoleUser in the system will be able to connect to a single role that exists in the system) this method will
    * be useful for reporting purposes when listing the users andd being able to grab the role that exists against the
    * particular RoleUser.
    *
    * @return \Illuminate\Database\Eloquent\Relations\HasOne
    */
    public function role()
    {
        return $this->hasOne(Role::class, 'id', 'role_id');
    }

    /**
    * Each RoleUser in the system will be able to connect to a single user that exists in the system) this method will
    * be useful for reporting purposes when listing the users and being able to grab the roles that exist against
    * the particular user
    *
    * @return \Illuminate\Database\Eloquent\Relations\HasOne
    */
    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}
