<?php

namespace App\Models\Project;

use Illuminate\Database\Eloquent\Model;

class ProjectUserContributor extends Model
{
    /**
    * @var string
    */
    protected $table = 'project_user_contributor';

    /**
    * @var array
    */
    protected $fillable = [
        'project_id',
        'user_id',
    ];

    /**
    * @var array
    */
    protected $casts = [
        'project_id' => 'int',
        'user_id' => 'int',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];

    /**
    * @var bool
    */
    public $timestamps = true;

    /*
    * -----------------------------------------------------------------------------------------------------------------
    * Getters
    *
    * Logic from this point until the next titling is 100% to do with getting information around the specific model in
    * question, in this case: the ProjectUserContributor
    *
    * -----------------------------------------------------------------------------------------------------------------
    */

    /*
    * -----------------------------------------------------------------------------------------------------------------
    * Relationships
    *
    * The information from this point on will 100% be around the relationships that this specific model has. In this
    * specific instance: the ProjectUserContributor
    *
    * -----------------------------------------------------------------------------------------------------------------
    */

    /**
    * This method is for being able to grab the user whilst looking at project user contributors. this is entirely for
    * convenience more than anything, if we are looking at the contributors of projects then it's nice to be able to
    * grab the user in question.
    *
    * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    */
    public function user()
    {
        return $this->belongsTo(\App\Models\User\User::class, 'user_id', 'id');
    }

    /**
    * This method is for being able to grab the project whilst looking at project user contributors. this is entirely
    * for convenience more than anything, if we are looking at the contributors of a project, or looking at the
    * relationship as a whole, then it's nice to grab the project in question with ease.
    *
    * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    */
    public function project()
    {
        return $this->belongsTo(Project::class, 'project_id', 'id');
    }
}
