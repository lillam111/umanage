<?php

namespace App\Models\Project;

use Illuminate\Database\Eloquent\Model;

class ProjectSetting extends Model
{
    /**
    * @var string
    */
    protected $table = 'project_setting';

    /**
    * @var string
    */
    protected $primaryKey = 'project_id';

    /**
    * @var array
    */
    protected $fillable = [
        'project_id',
        'view_id',
        'total_tasks',
        'completed_tasks'
    ];

    /**
    * @var array
    */
    protected $casts = [
        'project_id' => 'integer',
        'view_id' => 'integer',
        'total_tasks' => 'integer',
        'completed_tasks' => 'integer',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];

    /**
    * @var bool
    */
    public $timestamps = true;

    /*
    * -----------------------------------------------------------------------------------------------------------------
    * Getters
    *
    * Logic from this point until the next titling is 100% to do with getting information around the specific model in
    * question, in this case: the ProjectSetting
    *
    * -----------------------------------------------------------------------------------------------------------------
    */

    /*
    * -----------------------------------------------------------------------------------------------------------------
    * Relationships
    *
    * The information from this point on will 100% be around the relationships that this specific model has. In this
    * specific instance: the ProjectSetting
    *
    * -----------------------------------------------------------------------------------------------------------------
    */

    /**
    * Each ProjectSetting that gets inserted into the database will be associated to a Project - This will allow us to
    * view all the projectsettings in the system as a global and then associate where the ProjectSetting is belonging
    * this won't ever really be needed, however is in place for efficiency sake on the offchance that it might be
    * necessary
    *
    * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    */
    public function project()
    {
        return $this->belongsTo(Project::class, 'project_id', 'id');
    }
}
