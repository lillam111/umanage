<?php

namespace App\Models\Project;

use App\Models\Task\Task;
use App\Models\User\User;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    /**
    * @var string
    */
    protected $table = 'project';

    /**
    * @var array
    */
    protected $fillable = [
        'creator_user_id',
        'name',
        'code',
        'description',
        'color',
        'icon'
    ];

    /**
    * @var array
    */
    protected $casts = [
        'creator_user_id' => 'int',
        'name' => 'string',
        'code' => 'string',
        'description' => 'string',
        'color' => 'string',
        'icon' => 'string',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];

    /*
    * -----------------------------------------------------------------------------------------------------------------
    * Getters
    *
    * Logic from this point until the next titling is 100% to do with getting information around the specific model in
    * question, in this case: the Project
    *
    * -----------------------------------------------------------------------------------------------------------------
    */

    /**
    * This method is simply going to return a hash colour that we can use on the frontend for colouring a particular
    * item... if the colour is null, then it'll just return a blank string.
    *
    * @return string
    */
    public function getColor(): string
    {
        return $this->color !== null ? "#$this->color" : '';
    }

    public function getTaskNumberedProgress(): string
    {
        $total_tasks = $this->project_setting->total_tasks;
        $completed_tasks = $this->project_setting->completed_tasks;
        return "$completed_tasks/$total_tasks";
    }

    /**
     * This method for returning the progress of projects in terms of the tasks that sit inside them this will be
     * mapping out a percentage against the total tasks sitting, and completed tasks sitting. and will return a
     * calculation based on these two numbers, if they aren't found then we are going to be looking to return 0%;
     *
     * @return string
     */
    public function getTaskPercentageProgress(): string
    {
        $total_tasks = $this->project_setting->total_tasks;
        $completed_tasks = $this->project_setting->completed_tasks;

        if ($completed_tasks === 0) {
            return "0%";
        }

        $percentage = round(($completed_tasks / $total_tasks) * 100);
        return "$percentage%";
    }

    /*
    * -----------------------------------------------------------------------------------------------------------------
    * Relationships
    *
    * The information from this point on will 100% be around the relationships that this specific model has. In this
    * specific instance: the Project
    *
    * -----------------------------------------------------------------------------------------------------------------
    */

    /**
    * This method will be for returning all of the settings that are currently associated to a project. this will be
    * utilised for a variety of things, such like task statistics, how many tasks exist in this project, how many
    * completed tasks exist, what project view is the user desiring etc.
    *
    * @return \Illuminate\Database\Eloquent\Relations\HasOne
    */
    public function project_setting()
    {
        return $this->hasOne(ProjectSetting::class, 'project_id', 'id');
    }

    /**
    * This method is convenient for bringing in all the tasks that are currently belonging to a specific project... if
    * the user in question is looking at all of their projects (with: tasks) then we can iterate over all projects
    * and then iterate over all tasks that lie within a project.
    *
    * @return \Illuminate\Database\Eloquent\Relations\HasMany
    */
    public function tasks()
    {
        return $this->hasMany(Task::class, 'project_id', 'id');
    }

    /**
    * Each project will be created by a user, and this is more for conveniency when looking at projects to see what
    * user had created this particular project. We are able to call Project->creator_user->first_name etc. and just get
    * information about the one who made it.
    *
    * @return \Illuminate\Database\Eloquent\Relations\HasOne
    */
    public function creator_user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    /**
    * Each project can have multiple contributors that are essentially a linker table between project and users
    * if a user is connected to a project via being a contributor then the user will appear on the project itself
    * this is useful to quickly find out what users are currently on a project.
    *
    * @return \Illuminate\Database\Eloquent\Relations\HasMany
    */
    public function user_contributors()
    {
        return $this->hasMany(ProjectUserContributor::class, 'project_id', 'id');
    }
}
