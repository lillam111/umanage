<?php

namespace App\Models\User;

use App\Models\Task\Task;
use App\Models\Project\Project;
use App\Models\Permission\RoleUser;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
    * @var string
    */
    protected $table = 'user';

    /**
    * The attributes that are assignable.
    *
    * @var array
    */
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'password',
    ];

    /**
    * The attributes that should be hidden for arrays.
    *
    * @var array
    */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
    * The attributes that should be cast to native types.
    *
    * @var array
    */
    protected $casts = [
        'first_name' => 'string',
        'last_name' => 'string',
        'email' => 'string',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];

    /*
    * -----------------------------------------------------------------------------------------------------------------
    * Getters
    *
    * Logic from this point until the next titling is 100% to do with getting information around the specific model in
    * question, in this case: the User
    *
    * -----------------------------------------------------------------------------------------------------------------
    */

    /**
    * this method will be for returning the users full name, rather than writing out
    * $user->first_name . ' ' . $user->last_name, we can simply write $user->getFullName(); which will pretty much execute
    * the same logic set, but with less amount of work in the direct code. this will of course be checking whether or
    * not the user has a last name to user, otherwise this will be just spitting back the users first name.
    *
    * @return string
    */
    public function getFullName(): string
    {
        return "{$this->first_name} {$this->last_name}";
    }

    /**
    * this method will be returning the initials of the users name, Liam Taylor will be brought back as LT. this method
    * will be checking if the user has a last name, otherwise it will simply just use the first initial. otherwise it
    * will be spitting back both initials for the user in question
    *
    * @return string
    */
    public function getInitials(): string
    {
        $first_initial = $this->first_name !== null ?
            $this->first_name[0] : '';
        $last_initial = $this->last_name !== null ?
            $this->last_name[0] : '';

        return "$first_initial$last_initial";
    }

    /*
    * -----------------------------------------------------------------------------------------------------------------
    * Relationships
    *
    * The information from this point on will 100% be around the relationships that this specific model has. In this
    * specific instance: the User
    *
    * -----------------------------------------------------------------------------------------------------------------
    */

    /**
    * this method will be grabbing all of the projects that this user has against their name. this will be used in a
    * variety of locations, and this relationship model will be useful for grabbing a variety of user information
    * around the system
    *
    * @return \Illuminate\Database\Eloquent\Relations\HasMany
    */
    public function projects()
    {
        return $this->hasMany(Project::class, 'creator_user_id', 'id');
    }

    /**
    * this method will be grabbing all of the tasks that the user has reported in their name, this will be used in a
    * variety of locations, and this relationship model will be useful for grabbing everything that this user has
    * reported to the system from a task perspective.
    *
    * @return \Illuminate\Database\Eloquent\Relations\HasMany
    */
    public function tasks()
    {
        return $this->hasMany(Task::class, 'reporter_user_id', 'id');
    }

    /**
    * This method will be grabbing one entry from the user_detail table for this particular user. This table will
    * be for specific information regarding this user. Address, Job Title, phone number, postcode etc, this method
    * should in theory only ever be called on the /user page.
    *
    * @return \Illuminate\Database\Eloquent\Relations\HasOne
    */
    public function user_detail()
    {
        return $this->hasOne(UserDetail::class, 'user_id', 'id');
    }

    /**
    * This method will be grabbing one entry from the user_setting table for this particular user, This table will be
    * for intricate settings such as colour schemes; layout schemes, if the user is wanting a new layout or the
    * old layout etc etc. This user_setting relationship will return everything that the user has set for their account.
    *
    * @return \Illuminate\Database\Eloquent\Relations\HasOne
    */
    public function user_setting()
    {
        return $this->hasOne(UserSetting::class, 'user_id', 'id');
    }

    /**
    * This method will be grabbing a variety of roles that this user has assigned, each user can have more than one
    * one role, this will get them all. (this will be entirely used for permissions and seeing what this particular user
    * has the power to do around the entire system)
    *
    * @return \Illuminate\Database\Eloquent\Relations\HasOne
    */
    public function user_role()
    {
        return $this->hasOne(RoleUser::class, 'user_id', 'id');
    }
}
