<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

class UserSetting extends Model
{
    /**
    * @var string
    */
    protected $table = 'user_setting';

    /**
    * @var array
    */
    protected $fillable = [
        'user_id',
        'theme_color'
    ];

    /**
    * @var array
    */
    protected $casts = [
        'user_id' => 'int',
        'theme_color' => 'string'
    ];

    /**
    * @var bool
    */
    public $timestamps = false;

    /*
    * -----------------------------------------------------------------------------------------------------------------
    * Getters
    *
    * Logic from this point until the next titling is 100% to do with getting information around the specific model in
    * question, in this case: the UserSetting
    *
    * -----------------------------------------------------------------------------------------------------------------
    */

    /*
    * -----------------------------------------------------------------------------------------------------------------
    * Relationships
    *
    * The information from this point on will 100% be around the relationships that this specific model has. In this
    * specific instance: the UserSetting
    *
    * -----------------------------------------------------------------------------------------------------------------
    */

    /**
    * Each setting in the database will be belonging to a user, if we are looking to collect all setting users then it
    * quite could be possible that we are also after the user that the settings belong to, this is a 1 to 1 relationship
    * and should only ever belong to one user and one user should only ever have one setting set.
    *
    * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
