<?php

namespace App\Models\Journal;

use App\Models\User\User;
use Illuminate\Database\Eloquent\Model;

class Journal extends Model
{
    /**
    * @var string
    */
    protected $table = 'journal';

    /**
    * @var array
    */
    protected $fillable = [
        'user_id',
        'rating',
        'overall',
        'lowest_point',
        'highest_point',
        'when'
    ];

    /**
    * @var array
    */
    protected $casts = [
        'user_id' => 'int',
        'rating' => 'int',
        'overall' => 'string',
        'lowest_point' => 'string',
        'highest_point' => 'string',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];

    /**
    * @var bool
    */
    public $timestamps = true;

    /*
    * -----------------------------------------------------------------------------------------------------------------
    * Getters
    *
    * Logic from this point until the next titling is 100% to do with getting information around the specific model in
    * question, in this case: the Journal
    *
    * -----------------------------------------------------------------------------------------------------------------
    */

    /**
    * This method is strictly for returning a specific colour set when the user has opted to select a rating type, the
    * rating types from 5 - 1 are colour coded from purple, blue, green, orange, red. Purple being the best, red being
    * the worse.
    *
    * @return string
    */
    public function ratingColor(): string
    {
        switch ($this->rating) {
            case 1: return '#f0506e'; // A shade of red     | Worse rating
                break;
            case 2: return '#ffa500'; // A shade of orange  | Bad Rating
                break;
            case 3: return '#32d296'; // A shade of green   | Ok Rating
                break;
            case 4: return '#1e87f0'; // A shade of blue    | Good Rating
                break;
            case 5: return '#0037ff'; // A shade of purple  | Best Rating
                break;
            default: return '#b1b1b1'; // A shade of grey   | Default
        }
    }

    /*
    * -----------------------------------------------------------------------------------------------------------------
    * Relationships
    *
    * The information from this point on will 100% be around the relationships that this specific model has. In this
    * specific instance: the Journal.
    *
    * -----------------------------------------------------------------------------------------------------------------
    */

    /**
    * Each journal will be assigned to a singular user, this method will be minimally used, quite possibly only in
    * reporting purposes... this is an efficient and effective method for the retrieval of the user that is assigned to
    * a specific journal entry.
    *
    * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    */
    public function user()
    {
        return $this->belongsTo(User::class, 'id', 'user_id');
    }

    /**
    * Each joural will have a variety of achievements assigned to it which will obviously be hooked up to the
    * JournalAchievement model. This will return all achievements for this journal on a many to one type relationship
    *
    * @return \Illuminate\Database\Eloquent\Relations\HasMany
    */
    public function achievements()
    {
        return $this->hasMany(JournalAchievement::class, 'journal_id', 'id');
    }
}
