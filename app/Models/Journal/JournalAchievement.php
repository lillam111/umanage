<?php

namespace App\Models\Journal;

use Illuminate\Database\Eloquent\Model;

class JournalAchievement extends Model
{
    /**
    * @var string
    */
    protected $table = 'journal_achievement';

    /**
    * @var array
    */
    protected $fillable = [
        'journal_id',
        'name'
    ];

    /**
    * @var array
    */
    protected $casts = [
        'id' => 'int',
        'journal_id' => 'int',
        'name' => 'string',
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
    ];

    /**
    * @var bool
    */
    public $timestamps = true;

    /*
    * -----------------------------------------------------------------------------------------------------------------
    * Getters
    *
    * Logic from this point until the next titling is 100% to do with getting information around the specific model in
    * question, in this case: the JournalAchievement
    *
    * -----------------------------------------------------------------------------------------------------------------
    */

    /*
    * -----------------------------------------------------------------------------------------------------------------
    * Relationships
    *
    * The information from this point on will 100% be around the relationships that this specific model has. In this
    * specific instance: the JournalAchievement
    *
    * -----------------------------------------------------------------------------------------------------------------
    */

    /**
    * Each JournalAchievement that gets added into the system, will be attached to a journal. Because the
    * JournalAchievement in question will have a journal_id associated to it, we have the ability to collect the
    * journal from the database. This will offer the ability to be able to view all of the journal achievements and
    * highlight where the achievement is coming from and what day it is associated to.
    *
    * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    */
    public function journal()
    {
        return $this->belongsTo(Journal::class, 'id', 'journal_id');
    }
}
