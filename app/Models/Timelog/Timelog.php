<?php

namespace App\Models\Timelog;

use App\Models\Task\Task;
use App\Models\User\User;
use Illuminate\Database\Eloquent\Model;

class Timelog extends Model
{
    /**
    * @var string
    */
    protected $table = 'timelog';

    /**
    * @var array
    */
    protected $fillable = [
        'task_id',
        'user_id',
        'note',
        'from',
        'to',
        'time_spent'
    ];

    /**
    * @var array
    */
    protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
        'from' => 'datetime',
        'to' => 'datetime'
    ];

    /*
    * -----------------------------------------------------------------------------------------------------------------
    * Getters
    *
    * Logic from this point until the next titling is 100% to do with getting information around the specific model in
    * question, in this case: the Timelog
    *
    * -----------------------------------------------------------------------------------------------------------------
    */

    /*
    * -----------------------------------------------------------------------------------------------------------------
    * Relationships
    *
    * The information from this point on will 100% be around the relationships that this specific model has. In this
    * specific instance: the Timelog
    *
    * -----------------------------------------------------------------------------------------------------------------
    */

    /**
    * Every time logging entry will be connected to a user, each timelog entry will only ever be connected to One user
    * this will be an effective method for acquiring the user in question that we are dealing with...
    *
    * @return \Illuminate\Database\Eloquent\Relations\HasOne
    */
    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    /**
    * Every time logging entry will be connected to a task, there's no sense on logging time against nothing, and each
    * timelog entry that enters the database will have One task to bring back from it.
    *
    * @return \Illuminate\Database\Eloquent\Relations\HasOne
    */
    public function task()
    {
        return $this->hasOne(Task::class, 'id', 'task_id');
    }
}
