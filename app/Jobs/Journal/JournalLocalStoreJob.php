<?php

namespace App\Jobs\Journal;

use Illuminate\Bus\Queueable;
use App\Models\Journal\Journal;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldQueue;

class JournalLocalStoreJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $journals;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->journals = Journal::with('achievements')->get();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        foreach ($this->journals as $journal) {
            $put_journals[$journal->id] = [
                'id' => $journal->id,
                'user_id' => $journal->user_id,
                'rating' => $journal->rating,
                'overall' => $journal->overall,
                'lowest_point' => $journal->lowest_point,
                'highest_point' => $journal->highest_point,
                'when' => $journal->when,
                'created_at' => $journal->created_at,
                'updated_at' => $journal->updated_at
            ];

            if ($journal->achievements->isNotEmpty()) {
                foreach ($journal->achievements as $journal_achievement) {
                    $put_journals[$journal->id]['journal_achievements'][$journal_achievement->id] = [
                        'id' => $journal_achievement->id,
                        'journal_id' => $journal_achievement->journal_id,
                        'name' => $journal_achievement->name,
                        'created_at' => $journal_achievement->created_at,
                        'updated_at' => $journal_achievement->updated_at
                    ];
                }
            }
        }

        // create the file.
        Storage::disk('local')->put(
            'journals/journals.txt',
            json_encode($put_journals)
        );
    }
}
