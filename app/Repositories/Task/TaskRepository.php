<?php

namespace App\Repositories\Task;

use App\Models\Task\Task;
use App\Models\Task\TaskStatus;
use Illuminate\Support\Facades\Auth;

class TaskRepository
{
    /**
    * @param Task $task
    * @return string
    */
    public static function taskLink(Task $task): string
    {
        if (! $task instanceof Task) {
            return '';
        }

        $url = action('Task\TaskController@viewTaskGet', [$task->project->code, $task->id]);
        return "<a href='$url'>{$task->name}</a>";
    }

    /**
    * This method is entirely for bringing back the search and will be returning the html for the search data of tasks
    * the user will be searching for a task whether that's by a numerical value, or whether that's in the name of the
    * task... the data will be iterated over and be placed into a dropddown so that javascript can take this html and
    * dump it to the frontend.
    *
    * @param $tasks
    * @return string
    */
    public static function displayTaskDropdownSearch($tasks): string
    {
        $html = '<div class="dropdown open">';
            $html .= '<ul>';
                foreach ($tasks as $task) {
                    $html .= '<li>';
                        $html .= '<a data-task_id="' . $task->id . '">' . $task->name . '</a>';
                    $html .= '</li>';
                }
            $html .= '</ul>';
        $html .= '</div>';

        return $html;
    }

    /**
    * This method is designed for getting all tasks with some passed filter arguments, the filter arguments will be
    * required in object format. (object) $filters [];
    * the following variables that will be required in this are:
    *  (integer) project_id = 1
    *   (string) task_statuses = '1,2,3,4'
    *   (string) task_issue_types = '1,2,3,4'
    *   (string) task_priorities = '1,2,3,4'
    *   (string) search
    *  (boolean) pagination
    *  (integer) items_per_page
    *  (integer) page
    * @param array $filters
    * @return object
    */
    public static function getTasks($filters = [])
    {
        $tasks = Task::select('*');

        $tasks = $tasks->with([
            'project',
            'task_issue_type',
            'task_status',
            'task_priority'
        ]);

        // if the project id has been specified, then we are only going to be wanting to get the tasks that are currently
        // sitting within this particular passed project id, otherwise, if we have not set project, then we are looking
        // to receive all tasks that are within the system regardless of project.
        if (! empty($filters->project_id)) {
            $tasks = $tasks->where('task.project_id', '=', $filters->project_id);
        }

        // if the task statuses has been specified then we are only looking for tasks that are against the specific
        // task status ids that we are passing through to this filter, the status ids are going to be pushed up in a
        // comma separated value... which we will implode on a ',' and then look for tasks where in those ids.
        if (! empty($filters->task_statuses)) {
            $tasks = $tasks->whereIn('task.task_status_id', explode(',', $filters->task_statuses));
        }

        // if the task issue types has been specified then we are only looking for tasks that are against the specific
        // task issue type ids that we are passing through to this filter, the task issue ids are going to be pushed up
        // in a comma separated value... which we will implode on a ',' and then look for tasks in those passed ids.
        if (! empty($filters->task_issue_types)) {
            $tasks = $tasks->whereIn('task.task_issue_type_id', explode(',', $filters->task_issue_types));
        }

        // if the task priorities has been specified then we are only looking for tasks that are against the specific
        // task priority ids that we are passing through to this filter... the task pririty ids are going to be pushed
        // up in a comma separated value... which we will implode in a ',' and then look for tasks with those passed ids
        if (! empty($filters->task_priorities)) {
            $tasks = $tasks->whereIn('task.task_priority_id', explode(',', $filters->task_priorities));
        }

        // get the task statuses where they're not in the deleted type, or in the done type, we don't want to be showing
        // the deleted or completed tasks if the user has not requested to see them.
        // this only really wants to be taken into consideration if we have not passed in any filters for task statuses
        // otherwise, if we have some in stored in this variable, then we are just going to iterate past this logic set
        // as it is no longer necessary.
        if (empty($filters->task_statuses)) {
            $task_status_active_ids = TaskStatus::WhereIn('type', [
                TaskStatus::$TYPE_TODO,
                TaskStatus::$TYPE_IN_PROGRESS
            ])->get()->pluck('id')->toArray();

            $tasks = $tasks->whereIn('task.task_status_id', $task_status_active_ids);
        }

        // if we have passed through a search filter, then we are going to look through tasks that have the name similar
        // to what the user in question has opted to look for, currently all we are going to be searching for is the
        // tasks name solely.
        if (! empty($filters->search)) {
            $tasks = $tasks->whereRaw("task.name LIKE '%{$filters->search}%'");
        }

        // only returning the tasks in question, where the passed user (signed in user) is part of the task as a reporter
        // or an assigned user... this is a page specifically for what the user is needing to see.
        //$tasks = $tasks->where(function ($qry) {
        //    $qry->orWhere('reporter_user_id', '=', Auth::id())
        //        ->orWhere('assigned_user_id', '=', Auth::id());
        //});

        $total_pages = 0;

        // if the user has opted to have pagination included within the filtration system, then we are going to create
        // a pagination system to return  within the task segment...
        if (! empty($filters->pagination) && $filters->pagination === "true") {
            // clone the task query  so that we can get a direct count for the amount of tasks that we are going to be
            // returning to the user...
            $tasks_count_query = clone $tasks;
            $tasks_count = $tasks_count_query->count();

            // set the limit of tasks that we are going to be wanting to show the user per page...
            $tasks = ! empty($filters->items_per_page) ? $tasks->limit($filters->items_per_page) : $tasks;

            // get the offset (which will be a calculation of what page we are multipled by the tasks that we are going
            // to be showing to the user...
            $offset = ! empty($filters->page) && ! empty($filters->items_per_page)
                ? $filters->items_per_page * ($filters->page - 1) : 0;
            $tasks = $tasks->offset($offset);

            // the total number of pages will be calculated on the above variables where they're set... we are going to
            // be taking the total numbero f tasks and dividing it by the amount of items that can be displayed on the
            // page...
            $total_pages = ceil($tasks_count / $filters->items_per_page);
        }

        // return the data in an object format, when utilising this method and will need the tasks it'll be:
        // method::getTasks()->tasks... and this will be the standard norm across the entire system...
        return (object) [
            'tasks' => $tasks->get(),
            'pagination' => (object) [
                'page' => $filters->page,
                'total_pages' => $total_pages,
                'total_items' => $tasks_count
            ]
        ];
    }
}