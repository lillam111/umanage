<?php

namespace App\Services\View;

use App;
use Auth;
use View;

class ViewService
{
    private $title;
    private $page_title;
    private $ref;
    private $breadcrumbs;
    private $current_page;

    private $header = true;
    private $footer = true;

    /**
    * @return array
    */
    public function all(): array
    {
        return [
            'view_service' => (object) [
                'ref' => $this->get('ref') ?? null,
                'title' => env('APP_NAME') . " {$this->get('title')}",
                'page_title' => $this->get('page_title'),
                'current_page' => $this->get('current_page'),
                'breadcrumbs' => $this->generateBreadcrumbs(),
                'header' => $this->get('header'),
                'footer' => $this->get('footer'),
                'projects' => App\Models\Project\Project::where('creator_user_id', '=', Auth::id())->limit(6    )->get()
            ]
        ];
    }

    /**
    * Method for acquiring any of the items inside this particular class. ->get('title') will in fact, return whatever
    * value that is currently set against this object.
    *
    * @param $key
    * @return mixed
    */
    public function get($key)
    {
        return $this->$key;
    }

    /**
    * Method for unsetting any variable that currently sits inside this object, inside a specific instance. if the
    * specific areas of the system do not require the use of a variable inside here, we can unset it to save some
    * memory.
    *
    * @param $key
    * @return $this
    */
    public function remove($key)
    {
        unset($this->$key);
        return $this;
    }

    /**
    * This is a method for setting variables to the object. (There should only ever be one object for ViewService
    * in which we are going to be utilising this particular method for setting the variables necesary).
    *
    * @param $key
    * @param $value
    * @return $this
    */
    public function set($key, $value)
    {
        $this->$key = $value;
        return $this;
    }

    /**
    * Method for taking care of all breadcrumb logic, this will be taking all the information from the url at the time
    * and turning it into a readable breadcrumb path.
    *
    * @return string
    */
    public function generateBreadcrumbs(): string
    {
        $this->breadcrumbs = 'lol';
        return $this->breadcrumbs;
    }
}