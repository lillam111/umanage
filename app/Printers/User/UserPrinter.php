<?php

namespace App\Printers\User;

use App\Models\User\User;

class UserPrinter
{
    /**
    * Method for acquiring a passed user, in badge form. if there is ever a place where we are going to be utilising
    * a user display for the frontend then we are going to want to show the user, this will return their initials,
    * unless it has been requested that their full name is present, in which the full name will be inserted alongside
    * the badge.
    *
    * @param User $user
    * @param boolean $with_name
    * @return string
    */
    public static function userBadge($user = null, $with_name = false): string
    {
        if (! $user instanceof User) {
            return "";
        }

        $html = '';
        $html .= '<span class="badge user">';
            $html .= $user->getInitials();
        $html .= '</span>';

        if ($with_name) {
            $html .= "<span class='badge placeholder'>{$user->getFullName()}</span>";
        }

        return $html;
    }

    /**
    * Method for turning passed user into a linked format, this will be returning the users full name (if they have one
    * otherwise this is simply going to return their first name, if they have one).
    *
    * @param User $user
    * @return string
    */
    public static function linkedUser(User $user): string
    {
        if (! $user instanceof User) {
            return '';
        }

        $html = '<a href="' . action('User\UserController@viewUserGet', $user->id) . '">';
            $html .= $user->getFullName();
        $html .= '</a>';

        return $html;
    }
}