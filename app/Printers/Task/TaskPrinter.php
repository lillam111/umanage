<?php

namespace App\Printers\Task;

use App\Models\Task\Task;
use App\Models\Project\Project;

class TaskPrinter
{
    /**
    * @param Task $task
    * @return string
    */
    public static function getProjectBadge(Task $task): string
    {
        if (! $task instanceof Task || ! $task->project instanceof Project) {
            return '';
        }

        return "<span class='badge' style='background-color: {$task->project->getColor()}'>{$task->project->code}-{$task->id}</span>";
    }
}