<?php

namespace App\Http\Controllers\User;

use App\Models\User\User;
use Illuminate\Http\Request;
use App\Models\Timelog\Timelog;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\MasterController;
use App\Models\Project\ProjectUserContributor;

class UserController extends MasterController
{
    /**
    * when the user has signed in they are going to be needing a particular page that they are greeted with, this will
    * be the route to maintaining teh user's home screen.
    *
    * @param Request $request
    * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
    */
    public function viewUserDashboardGet(Request $request)
    {
        $user = Auth::user();

        // set the view service reference...
        $this->view_service
            ->set('title', "Dashboard - {$user->getFullName()}")
            ->set('ref', 'dashboard');

        // get the projects that are designated to this particular user, the ones that the user they themselves have
        // created
        $projects = Auth::user()->projects;

        // get all the time logs for this particular user and introduce a section on the page for that user in questions
        // most recent activity...
        $timelogs = Timelog::where('user_id', '=', $user->id)->paginate(5);

        // get all of the tasks that are within the project ids that we have collected from the above, so that we can
        // justify which one's we are going to want to show on the user's dashboard.
        // iterate over all projects and assign them to the tasks
        $tasks = collect();
        $projects->map(function ($project) use (&$tasks) {
            $tasks = $tasks->merge($project->tasks);
        });

        return view('user.view_user_dashboard', compact(
            'projects',
            'tasks',
            'timelogs'
        ));
    }

    /**
    * This method is entirely for rendering the frontend visuals for the user's login form. this particular page will be
    * void of the header and footer of the system because the user will not necessarily need to view these particular
    * elements until being signed in.
    *
    * @param Request $request
    * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
    */
    public function viewUserLoginGet(Request $request)
    {
        $this->view_service
            ->set('title', 'Login')
            ->set('current_page', 'login')
            ->set('header', false)  // disabling header, header isn't needed on this page.
            ->set('footer', false); // disabling footer, footer isn't needed on this page.

        return view('user.view_user_login');
    }

    /**
    * This method handles the posting of a user attempting to log into the system. all this method takes is a
    * email and a password and if they match the user's entry then the user will be redirected their designated
    * page, otherwise they will be redirected back to the sign in page with the corresponding errors...
    *
    * @param Request $request
    *       (string) $request->input('email')
    *       (string) $request->input('password')
    * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
    * @throws \Illuminate\Validation\ValidationException
    */
    public function viewUserLoginPost(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required|min:3'
        ]);

        if (Auth::attempt([
            'email' => $request->input('email'),
            'password' => $request->input('password')
        ])) {
            return redirect('/');
        }

        return back()->with('error', 'Wrong Login Details');
    }

    /**
    * this method is for returning all the users in the system to a page... this will be the default method of being
    * able to look at what users are currently in the system, currently with no restriction at all...
    *
    * @param Request $request
    * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
    */
    public function viewUsersGet(Request $request)
    {
        $users = User::all();

        $this->view_service->set('title', '- users');

        return view('user.view_users', compact(
            'users'
        ));
    }

    /**
    * this method is simply returning a single user page...
    *
    * @param Request $request
    * @param $user_id
    * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
    */
    public function viewUserGet(Request $request, $user_id)
    {
        // find the user that the requested user is trying to view, we are going to be wanting to accumulate a variety
        // of this particular user's data from the get go so that we can print a dashboard like view with a variety
        // of different data.
        $user = User::with([
            'projects',
            'tasks',
            'tasks.task_status',
            'tasks.task_priority',
            'tasks.task_issue_type',
            'user_role',
            'user_role.role'
        ])->where('id', '=', $user_id)->first();

        // if the user we have searched for somehow doesn't happen to be an instance of user... then we are going
        // to want to redirect the user back to the user/1 page... utilising it's method, this in theory
        // should only ever happen if the user has opted to free search this in the url...
        if (! $user instanceof User || Auth::user()->cannot('UserGate@viewUser', $user)) {
            return redirect()
                ->action('User\UserController@viewUsersGet');
        }

        // when we're looking at the user in question then we are going to be gathering the users people that are attached
        // to some of their projects, this will give an i   dea for the user on the amount of people they are going to be
        // working with on a variety of different projects. this can get quite large, as we are going to be looking at
        // a user project collection, so if the same user is connected to a variety of other projects, then we are
        // only wanted to get unique entries.
        $users_i_work_with = ProjectUserContributor::select('*')
            ->with([
                'user'
            ])
            ->whereIn('project_id', $user->projects->pluck('id')->toArray())
            ->get()->pluck('user_id')->toArray();

        $users_i_work_with = User::whereIn('id', $users_i_work_with)->get();

        $this->view_service->set('title', " - {$user->getFullName()}")
                           ->set('ref', 'account');

        return view('user.view_user', compact(
            'user',
            'users_i_work_with'
        ));
    }

    /**
    * @param Request $request
    * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
    */
    public function viewUserEditGet(Request $request)
    {
        // todo this method needs fleshing out for everything that this method is going to need to acquire...
        // todo this method also needs the view service setting the title of the page...
        return view('user.view_user_edit');
    }

    public function viewUserEditPost(Request $request)
    {
        // todo this method needs fleshing out and placing all the logic in place that i'm going to be needing for the
        //  user editing process...
    }

    /**
    * This method is simply just for signing the user out, the user needs a method for being able to exit the system,
    * the logout method will direct the user back to the login page, as that is the only page a user should be able to
    * access whilst not logged in, or even forgot password.
    *
    * @param Request $request
    * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
    */
    public function userLogout(Request $request)
    {
        Auth::logout();
        return redirect('/login');
    }
}
