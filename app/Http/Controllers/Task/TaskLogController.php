<?php

namespace App\Http\Controllers\Task;

use App\Models\Task\TaskLog;
use Illuminate\Http\Request;
use App\Http\Controllers\MasterController;
use App\Repositories\Task\TaskLogRepository;

class TaskLogController extends MasterController
{
    /**
    * @param Request $request
    *      (integer) $request->input('task_id')
    *      (integer) $request->input('page')
    * @return \Illuminate\Http\JsonResponse
    * @throws \Throwable
    */
    public function ajaxViewTaskLogsGet(Request $request)
    {
        $task_id = (int) $request->input('task_id');
        $page    = (int) $request->input('page') ?: 1;
        $limit   = (int) 5;
        $offset  = (int) $limit * ($page - 1);

        // acquire the total number of task logs in the system so that we are going to be able to calculate how many
        // pages there are. if (the current page is the page we are on, then the pagination will need to be cancelled
        $task_log_count = TaskLog::where('task_id', '=', $task_id)->count();

        // acquire the total number of pages that we are going to be able to show, this will be needed to see whether or
        // not we can go left or right.
        $total_pages = ceil($task_log_count / $limit);

        // acquire all the task logs that are sitting in the system against this particular task, and then we are going
        // to offset by a page, as well as limmit it to a small number so that the user will be able to flick through
        // the logs in the system.
        $task_logs = TaskLog::where('task_id', '=', $task_id)
            ->orderBy('when', 'desc')
            ->limit($limit)
            ->offset($offset)
            ->get();

        $task_logs = TaskLogRepository::sortTaskLogs($task_logs->keyBy('id'));

        return response()->json([
            'html' => view('library.task.task_logs.view_task_logs', compact(
                'task_logs',
                'page',
                'total_pages'
            ))->render()
        ]);
    }
}
