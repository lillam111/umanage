<?php

namespace App\Http\Controllers\Task;

use App\Models\Task\Task;
use App\Models\Task\TaskLog;
use Illuminate\Http\Request;
use App\Models\Project\Project;
use Illuminate\Support\Facades\Auth;
use App\Repositories\Task\TaskRepository;
use App\Http\Controllers\MasterController;
use App\Models\Project\ProjectUserContributor;
use App\Repositories\Timelog\TimelogRepository;

class TaskController extends MasterController
{
    /**
    * This method is strictly for showing the tasks page, this will give the user all the tasks that the user has
    * against their name and list them all. this method will be taking request parameters which will allow the user
    * to search, order and do other things with the data that show on this page...
    *
    * @param Request $request
    * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
    */
    public function viewTasksGet(Request $request)
    {
        $tasks = Task::select('*')
            ->with([
                'project'
            ])
            ->where(function ($qry) {
                $qry->where('reporter_user_id', '=', Auth::id())
                    ->orWhere('assigned_user_id', '=', Auth::id());
            })
            ->orderBy('task_priority_id', 'asc')
            ->get();

        return view('task.view_tasks', compact(
            'tasks'
        ));
    }

    /**
    * This method is strictly for showing a singular task that the user has opted to look at, this will be taking the
    * project code as well as the task id and will look as such: /3/1 (project 3) (task 1) and will attempt to find a
    * combination with the two matching values and if it does, it will show that task, otherwise it will redirect the
    * user back to the view tasks page.
    *
    * @param Request $request
    * @param $code
    * @param $task_id
    * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
    */
    public function viewTaskGet(Request $request, $project_code, $task_id)
    {
        $project = Project::where('code', '=', $project_code)->first();

        // checking whether or not the project that we're looking at exists in the system, after checking that this
        // exists, we then check to see  if the signed in user trying to view it... has the permission to view this
        // particular task/project combination.
        if (! $project instanceof Project || Auth::user()->cannot('ProjectGate@viewProject', $project)) {
            return redirect()->action('Project\ProjectController@viewProjectGet', $project->id);
        }

        $task = Task::select('*')
            ->with([
                'project',
                'project.user_contributors',
                'task_assigned_user',
                'task_watcher_users',
                'task_checklists',
                'task_checklists.task_checklist_items',
                'task_timelogs',
                'task_timelogs.user'
            ])
            ->where('id', '=', $task_id)
            ->where('project_id', '=', $project->id)
            ->first();

        if (! $task instanceof Task || Auth::user()->cannot('TaskGate@viewTask', $task)) {
            return redirect()->action('Project\ProjectController@viewProjectGet', $project->id);
        }

        // assuming we have made it here - we have done everything that we needed with the project... we can now unset
        // the project and save some memory space, we won't be utilising the project beyond this point as we will have
        // it attached onto (task).
        unset($project);

        // acquire all of the users for this project, so we are able to add this to the watchers, and the assignee
        // of the project, we are only going to want to reference users that are within this project spec, and only
        // those that have been granted permission to view will be showing up within the task additions.
        $task->project_users = $task->project->user_contributors->map(function ($project_user) {
            return $project_user->user;
        });

        // Acquire all the task timelogging, along with the total amount of time logged, this will be grabbing all time
        // logging of all users that has ever placed against the selected task.
        $task->task_timelogs = TimelogRepository::sortTaskTimelogs($task);
        $task->total_time_logged = TimelogRepository::getTotalTimeLogged($task);

        $this->view_service->set('title', " - Task - {$task->name}");

        return view('task.view_task', compact(
            'task'
        ));
    }

    /**
    * this method is enabling the user to be able to search for a particular set of tasks... this will look for tasks
    * that have the name of the passed variable: [name]. this method will also be taking a number and will try find
    * tasks that are matching in that task id passed... if i search for 1, then we want task (1, 10, 11, 111, 1111)
    * etc.
    *
    * @param Request $request
    *      (integer) $request->input('name')
    * @return string
    */
    public function ajaxSearchTasksGet(Request $request): string
    {
        $name = $request->input('name');
        $tasks = Task::whereRaw("name like '%$name%'")
            ->get();

        if ($tasks->isEmpty()) {
            return '';
        }

        return TaskRepository::displayTaskDropdownSearch($tasks);
    }

    /**
    * This method simply collects all theh tasks that are passed, we build up an array of filters that will be passed
    * to the task repository which will get the task data and assort the data based on the passed filters that we have
    * passed through to the system
    *
    * @param Request $request
    *       (string) $request->input('title')
    *      (integer) $request->input('project_id')
    *       (string) $request->input('task_statuses')
    *       (string) $request->input('task_issue_types')
    *       (string) $request->input('task_priorities')
    *       (string) $request->input('search')
    * @return string
    */
    public function ajaxViewTasksGet(Request $request): string
    {
        $filters = (object) [
            'project_id'       => (int) $request->input('project_id'),
            'task_statuses'    => $request->input('task_statuses'),
            'task_issue_types' => $request->input('task_issue_types'),
            'task_priorities'  => $request->input('task_priorities'),
            'search'           => (string) $request->input('search'),
            'pagination'       => $request->input('pagination'),
            'items_per_page'   => (integer) $request->input('items_per_page'),
            'page'             => (integer) $request->input('page') ?: 1
        ];

        $title = $request->input('title');

        $task_data = TaskRepository::getTasks($filters);

        $tasks         = $task_data->tasks;
        $page          = $task_data->pagination->page;
        $total_pages   = $task_data->pagination->total_pages;
        $total_results = $task_data->pagination->total_items;

        return view('library.task.task.ajax_view_tasks', compact(
            'tasks',
            'title',
            'page',
            'total_pages',
            'total_results'
        ));
    }

    /**
    * Method for creating tasks in the system, thihs will essentially take  the bare necessities in order for making a
    * task, however there is a possibility that the user may want to create a fully fledged task with everything
    * specced out in which we will be giving the user the ability to simplify create oen and then come back to the
    * default later.
    *
    * @param Request $request
    *      (integer) $request->input('project_id')
    *       (string) $request->input('name')
    *       (string) $request->input('description')
    * @return \Illuminate\Http\JsonResponse
    */
    public function ajaxMakeTaskPost(Request $request)
    {
        $project_id       = (int) $request->input('project_id');
        $name             = (string) $request->input('name');
        $description      = (string) $request->input('description');
        $reporter_user_id = (int) Auth::id();
        $assigned_user_id = null;
        $task_status_id   = (int) 1;
        $task_priority_id = (int) 1;
        $due_date         = null;

        $task = Task::create([
            'project_id'       => $project_id,
            'name'             => $name,
            'description'      => $description,
            'reporter_user_id' => $reporter_user_id,
            'assigned_user_id' => $assigned_user_id,
            'task_status_id'   => $task_status_id,
            'task_priority_id' => $task_priority_id ,
            'due_date'         => $due_date
        ]);

        $task->log(TaskLog::TASK_MAKE, $task->name);
        $task->project->project_setting->increment('total_tasks');

        return response()->json([
           'response' => 'Task has been successfully created'
        ]);
    }

    /**
    * This particular method will be a globalised method which handles all the editing of tasks (singular) to which
    * will return a Json response so that the javascript can alert the user that something has happened. this method
    * will take simply a field, and a value. (nothing specific) so that this can be used universally across all updates
    * to a task.
    *
    * @param Request $request
    *      (integer) $request->input('task_id')
    *       (string) $request->input('field')
    *     (variable) $request->input('value')
    * @return \Illuminate\Http\JsonResponse
    */
    public function ajaxEditTaskPost(Request $request)
    {
        $task_id = (int) $request->input('task_id');
        $field   = (string) $request->input('field');
        $value   = $request->input('value');

        $task = Task::where('id', '=', $task_id)->first();

        // check which field we are working with in order to decide which constant we are going to be pushing into the
        // task log specifically.
        $task_log_constant_variable = 'TASK_' . mb_strtoupper($field);
        $task_log_constant = constant("App\Models\Task\TaskLog::{$task_log_constant_variable}");

        // Preparing the log information for this task edit... the log type will be decided in the switch case; seeing
        // as this is a global method for updating tasks.
        $task->log($task_log_constant, $value, $task->$field);

        // after we have finished logging the particular change to the task, we are now able to update the item in the
        // database.
        $task->update([$field => $value]);

        return response()->json([
           'response' => 'You have updated the task...'
        ]);
    }
}