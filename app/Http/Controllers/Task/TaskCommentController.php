<?php

namespace App\Http\Controllers\Task;

use Auth;
use App\Models\Task\Task;
use Illuminate\Http\Request;
use App\Models\Task\TaskComment;
use App\Http\Controllers\MasterController;

class TaskCommentController extends MasterController
{
    /**
    * This method will be viewing the task comments via ajax, this will be called on initial load of the task page...
    * so we are offsetting the initial page load. this also gives us the ability to refresh the section should we need
    * to. This method will be requiring a task_id by default, so that we can grab the current existing comments against
    * the task in question.
    *
    * @param Request $request
    *      (integer) $request->input('task_id')
    *      (integer) $request->input('page')
    * @return \Illuminate\Http\JsonResponse
    * @throws \Throwable
    */
    public function ajaxViewTaskCommentsGet(Request $request)
    {
        $task_id = $request->input('task_id');
        $page    = (int) $request->input('page') ?: 1;
        $limit   = (int) 5;
        $offset  = (int) $limit * ($page - 1);

        // lets grab the task that we are going to be attaching a comment to, we are going to need to see if this
        // particular task in question exists for us to connect this comment to, if it does proceed, if it does not
        // we are going to want to end here...
        $task = Task::where('id', '=', $task_id)->first();

        $task_comments_count = TaskComment::where('task_id', '=', $task_id)->count();

        // acquire the total number of pages that we are going to be able to show, this will be needed to see whether or
        // not we can go left or right.
        $total_pages = ceil($task_comments_count / $limit);

        // acquire all of the task comments that are for this particular task, this will return every comment that exists
        // with the passed task id.
        $task_comments = TaskComment::where('task_id', '=', $task_id)
            ->orderBy('created_at', 'desc')
            ->limit($limit)
            ->offset($offset)
            ->get();

        return response()->json([
            'html' => view('library.task.task_comments.view_task_comments', compact(
                'task_comments',
                'task',
                'page',
                'total_pages'
            ))->render()
        ]);
    }

    /**
    * This method is entirely for submitting the ajax post request in order for making a task comment. this method will
    * be requiring a task id by default, as this particular method is utilised for connecting a comment to a task.
    *
    * @param Request $request
    *      (integer) $request->input('task_id')
    *       (string) $request->input('content')
    * @return \Illuminate\Http\JsonResponse
    */
    public function ajaxMakeTaskCommentPost(Request $request)
    {
        $task_id = $request->input('task_id');
        $content = $request->input('content');

        $comment = TaskComment::create([
            'task_id' => $task_id,
            'user_id' => Auth::id(),
            'content' => $content
        ]);

        return response()->json([
            'response' => 'Comment has been successfully been created',
            'comment' => [
                'content' => $comment->content,
                'created_at' => $comment->created_at
            ],
            'user' => [
                'name' => Auth::user()->getFullName(),
                'initials' => Auth::user()->getInitials()
            ],
        ]);
    }

    /**
    * This method allows the user to delete a particular task comment, this method will be returning json so that the
    * user will be alerted in kind on the frontend instantly.
    *
    * @param Request $request
    *      (integer) $request->input('task_comment_id')
    * @return \Illuminate\Http\JsonResponse
    */
    public function ajaxDeleteTaskCommentPost(Request $request)
    {
        $task_comment_id = $request->input('task_comment_id');

        TaskComment::where('id', '=', $task_comment_id)->delete();

        return response()->json([
            'response' => 'Successfully deleted comment'
        ]);
    }
}
