<?php

namespace App\Http\Controllers\Journal;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Journal\Journal;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\MasterController;
use App\Jobs\Journal\JournalLocalStoreJob;

class JournalController extends MasterController
{
    /**
    * @param Request $request
    *       (string) $request->input('date')
    * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
    */
    public function viewJournalsGet(Request $request)
    {
        $user = Auth::user();

        $date = ! empty($request->input('date')) ?
            Carbon::parse($request->input('date')) :
            Carbon::now();

        $this->view_service->set('title', "Journals - {$user->getFullName()}");

        return view('journal.view_journals', compact(
            'date'
        ));
    }

    /**
    * @param Request $request
    *       (string) $request->input('date')
    *       (string) $request->input('direction')
    * @return \Illuminate\Http\JsonResponse
    * @throws \Throwable
    */
    public function ajaxViewJournalsGet(Request $request)
    {
        // the date, which will be by default today (coming from the view page) and when the ajax call is hit here,
        // this will be coming either with a passed date, or the date that is today.
        $date = Carbon::parse($request->input('date'));

        // direction by default will either be a blank string, left or right. if direction isn't set then  this will
        // simply be ignored.
        $direction = $request->input('direction');

        // if the direction is set, and is designated to be going left, then we are looking to cycle through dates into
        // the past and grabbing all journals that are beyond today backwards.
        if (! empty($direction) && $direction === 'left') {
            $date = $date->subMonth(1);
        }

        // if the direction is set and designated to be going right, then we are looking to cycle through dates into the
        // future and grabbing all journals that are beyond today, forwards.
        if (! empty($direction) && $direction === 'right') {
            $date = $date->addMonth(1);
        }

        // grab the start of the month from the date that we have passed, so that we can calculate how many days are in
        // this particular month from the very beginning.
        $start_of_month = $date->startOfMonth();

        // grab the total amount of days that are in this particular month from the date object, from the start of the
        // month.
        $days_in_month = $start_of_month->daysInMonth;

        // acquire the starting day in the format of text (Saturday, Sunday) etc.
        $starting_day = $start_of_month->format('l');

        // get the days from Sunday - Saturday. (Sunday, Monday, Tuesday, Wednesday...)
        $days = Carbon::getDays();

        // initiate the dates array that we will be passing through  to the blade file.
        $dates = [];

        // get the starting day key that we can increment from and apply to the keys of each journal day.
        $day_key = array_search($starting_day, $days);

        // acquire all the journals, that are from the start of the month, to the end of the month.
        $journals = Journal::with(['achievements'])
            ->where('user_id', '=', Auth::id())
            ->where('when', '>=', Carbon::parse($date)->startOfMonth()->format('Y-m-d'))
            ->where('when', '<=', Carbon::parse($date)->endOfMonth()->format('Y-m-d'))
            ->get()->keyBy('when');

        for ($day_increment = 1; $day_increment < ($days_in_month + 1); ++ $day_increment) {
            // getting our journal key...
            $journal_key = "{$date->format('Y-m')}-" . ($day_increment < 10 ? '0' : '') . "$day_increment";

            // if the day key is 7, then we have reached the end and should be set back to the beginning. we don't have a
            // 7 value in the day key... thus we are needing a reset.
            if ($day_key === 7) {
                $day_key = 0;
            }

            $reference_day = $days[$day_key];

            $dates[$journal_key] = (object) [
                'title' => "$reference_day <span class='uk-text-small'>{$date->format('M')} $day_increment</span>",
                'journal' => ! empty($journals->get($journal_key)) ? $journals->get($journal_key) : null
            ];

            // when we have finished iterating over this particular item, unset it. as we have just turned this particular
            // entry into an object.
            if ($journals->get($journal_key) instanceof Journal) {
                $journals->forget($journal_key);
            }

            // increment the day key, so that we are able to gather the next day in the iteratable instance.
            $day_key += 1;
        }

        return response()->json([
            'date' => $date->format('Y-m'),
            'date_display' => $date->format('F Y'),
            'html' => view('library.journal.ajax_view_journals', compact('dates'))->render()
        ]);
    }

    /**
    * @param Request $request
    * @param $date
    * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
    */
    public function viewJournalGet(Request $request, $date)
    {
        $user = Auth::user();
        $user_id = $user->id;

        $journal = Journal::where('when', '=', $date)
            ->where('user_id', '=', $user_id)
            ->first();

        $yesterday = Carbon::parse($date)->subDay(1);
        $tomorrow = Carbon::parse($date)->addDay(1);

        // if we do have a journal entry, then... we are quite possibly going to need to make it so that we can reference
        // it on the frontend. as the id of the journal will be required for the achievements concept.
        if (! $journal instanceof Journal) {
            $journal = Journal::create([
                'user_id' => $user_id,
                'when' => $date
            ]);

            // assuming the user has clicked on a particular view for journal entries, then we are going to
            // want to dispatch a job that locally stores the journal entries, so that when it comes to
            // wiping the database at any point, I will nicely be able to put them back in (temporary code)
            dispatch(new JournalLocalStoreJob());
        }

        $this->view_service->set('title', "Journal - {$user->getFullName()}'s $date");

        return view('journal.view_journal', compact(
            'date',
            'journal',
            'yesterday',
            'tomorrow'
        ));
    }

    /**
    * @param Request $request
    *      (integer) $request->input('journal_id')
    *       (string) $request->input('field')
    *     (variable) $request->input('value')
    * @return \Illuminate\Http\JsonResponse
    */
    public function ajaxEditJournalPost(Request $request)
    {
        $journal_id = $request->input('journal_id');
        $field = $request->input('field');
        $value = $request->input('value');

        Journal::where('id', '=', $journal_id)
            ->update([
                $field => $value
            ]);

        // if the user has opted to edit a journal post, then we are going to be wanting to reflect these changes
        // in the local store, so that when it comes to wiping the database, when putting these back in via the seeder
        // the data is going to be accurate (temporary code)
        dispatch(new JournalLocalStoreJob());

        return response()->json([
            'response' => "successfully updated the field'"
        ]);
    }

    /**
    * This method is specifically for deleting the journal entry in question, if the user has clicked on a date, (a
    * journal entry will have been created from that, however if this was unintentional and they don't want the entry
    * then they are going to need a means of deletion... this will control the deletion).
    * when the deletion has gone through successfully, the user will be returned a redirection link which will be
    * processed via the javascript.
    *
    * @param Request $request
    *      (integer) $request->input('journal_id')
    * @return \Illuminate\Http\JsonResponse
    */
    public function ajaxDeleteJournalPost(Request $request)
    {
        $user_id = Auth::id();
        $journal_id = $request->input('journal_id');

        Journal::where('id', '=', $journal_id)
            ->where('user_id', '=', $user_id)->delete();

        // assuming the user has opted to delete the journal, we are going to need to update the job, to re-store the
        // data set that no longer inludes the specific journal entry that we have just asked to delete. (temporary code)
        dispatch(new JournalLocalStoreJob());

        return response()->json([
            'response' => action('Journal\JournalController@viewJournalsGet'),
        ]);
    }

    /**
    * This method is specifically for showing the daily progress of the users ratings. this will be a page to highlight
    * occurrences quickly throughout the month, how much has been rated in comparison to how many achievements happened
    * that day.
    *
    * @param Request $request
    *      (integer) $request->input('user_id')
    *      (integer) $request->input('date')
    * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
    */
    public function viewJournalsReportGet(Request $request)
    {
        $user = Auth::user();

        // lets check whether or not we have a date passed through the url parameters, if we do then we are going to be
        // parsing this as a date in the format of Year Month Day. otherwise, we are going to take todays date in the
        // format of Year Month Day.
        $date = $request->input('date');
        $date = ! empty($date) ?
            Carbon::parse($date) :
            Carbon::now();

        $this->view_service->set('title', "Journal Report - {$user->getFullName()}");

        return view('journal.journal_report.view_journal_report', compact(
            'date'
        ));
    }

    /**
    * This method is entirely for grabbing journals in report format, this will be returning all the data in a json
    * oriented object so that the frontend can process what is going on here; all values will be viewed inside the
    * view_journals_report.js file, after ajaxing has happened. This method is entirely a cycle, and for as many times
    * as the user will keep clicking next or back, will depend on the values that get returned (based on the past
    * date)
    *
    * @param Request $request
    *       (string) $request->input('date')
    *       (string) $request->input('direction')
    * @return \Illuminate\Http\JsonResponse
    */
    public function ajaxViewJournalsReportGet(Request $request)
    {
        // Acquiring teh date that the report is going to be generated off; the user will be able to cycle through the
        // months, and in doing so, providing there is a month set, we will be utilising this month, otherwise, this
        // month will be set to today.
        $date = Carbon::parse($request->input('date'));

        // checking to see whether or not we have a direction, by default unless requested by the user this will be null
        // or '', and if this is the case we are going to iterate on over past this logic. however if it is set then
        // we are either going to be incrementing a month, or decrementing a month and then returning this specific
        // date and altering it for when we grab the necessary data for the month in question.
        $direction = $request->input('direction');
        if (! empty($direction) && $direction === 'left') {
            $date = $date->subMonth(1);
        }

        if (! empty($direction) && $direction === 'right') {
            $date = $date->addMonth(1);
        }

        // acquire all the journals that have a when between the start and end of the month for the month of which has
        // been designated above.
        $journals = Journal::where('user_id', '=', Auth::id())
            ->with('achievements')
            ->where('when', '>=', Carbon::parse($date)->startOfMonth())
            ->where('when', '<=', Carbon::parse($date)->endOfMonth())
            ->get();

        // pre-defining all the variables that this method is going to be returning to the client, allowing the
        // javascript to take effect at this point. all variables will be defined a default so that we have the knowledge
        // of knowing that the values are in fact set.
        $journal_labels                     = [];
        $journal_ratings                    = [];
        $journal_ratings_colors             = [];
        $journal_achievements               = [];
        $journal_achievements_colors        = [];
        $journal_highest_word_counts        = [];
        $journal_highest_word_counts_colors = [];
        $journal_lowest_word_counts         = [];
        $journal_lowest_word_counts_colors  = [];

        // pre-defining all the variables in respect to the statistic set of data, all of this will be defaulted to 0
        // so that when we are referring to this back in the code, it won't flag up as undefined, but simply instead
        // print out a 0, as there will be no information on the respective variables.
        $total_achievements                 = 0;
        $total_1_star_days                  = 0;
        $total_2_star_days                  = 0;
        $total_3_star_days                  = 0;
        $total_4_star_days                  = 0;
        $total_5_star_days                  = 0;

        foreach ($journals as $key => $journal) {
            $journal_labels[]                     = $journal->when;
            $journal_ratings[]                    = $journal->rating;
            $journal_ratings_colors[]             = $journal->ratingColor();
            $journal_achievements[]               = $journal->achievements->count();
            $journal_achievements_colors[]        = '#40e0d0';

            $star_days                            = "total_{$journal->rating}_star_days";
            if (isset($$star_days)) $$star_days   += 1;

            $total_achievements                   += $journal->achievements->count();

            $journal_highest_word_counts[]        = strlen($journal->highest_point);
            $journal_highest_word_counts_colors[] = '#32d296';
            $journal_lowest_word_counts[]         = strlen($journal->lowest_point);
            $journal_lowest_word_counts_colors[]  = '#f0506e';
        }

        // Returning all of the above data, in a json-ised formatted data set so that javascript has an easier time
        // reading the data that we have returned. this is a build up of all the data above, and will be utilised in
        // reporting. (for the month in particular that has been passed)
        return response()->json([
            'journal_ratings'                    => $journal_ratings,
            'journal_ratings_colors'             => $journal_ratings_colors,
            'journal_achievements'               => $journal_achievements,
            'journal_achievements_colors'        => $journal_achievements_colors,
            'journal_highest_word_counts'        => $journal_highest_word_counts,
            'journal_highest_word_counts_colors' => $journal_highest_word_counts_colors,
            'journal_lowest_word_counts'         => $journal_lowest_word_counts,
            'journal_lowest_word_counts_colors'  => $journal_lowest_word_counts_colors,
            'labels'                             => $journal_labels,
            'total_achievements'                 => $total_achievements,
            'total_1_star_days'                  => $total_1_star_days,
            'total_2_star_days'                  => $total_2_star_days,
            'total_3_star_days'                  => $total_3_star_days,
            'total_4_star_days'                  => $total_4_star_days,
            'total_5_star_days'                  => $total_5_star_days,
            'date'                               => $date->format('Y-m'),
            'date_display'                       => $date->format('F Y')
        ]);
    }
}
