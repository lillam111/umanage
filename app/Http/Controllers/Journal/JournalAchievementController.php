<?php

namespace App\Http\Controllers\Journal;

use Illuminate\Http\Request;
use App\Jobs\Journal\JournalLocalStoreJob;
use App\Models\Journal\JournalAchievement;
use App\Http\Controllers\MasterController;

class JournalAchievementController extends MasterController
{
    /**
    * @param Request $request
    *      (integer) $request->input('journal_id')
    * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
    */
    public function ajaxViewJournalAchievementsGet(Request $request)
    {
        $journal_id = $request->input('journal_id');
        $journal_achievements = JournalAchievement::where('journal_id', '=', $journal_id)
            ->get();

        return view('journal_achievement.ajax_view_journal_achievements', compact(
            'journal_achievements'
        ));
    }

    /**
    * @param Request $request
    *      (integer) $request->input('journal_id')
    *      (integer) $request->input('journal_achievement')
    * @return \Illuminate\Http\JsonResponse
    */
    public function ajaxMakeJournalAchievementPost(Request $request)
    {
        $journal_id = $request->input('journal_id');
        $journal_achievement = $request->input('journal_achievement');

        JournalAchievement::create([
            'name' => $journal_achievement,
            'journal_id' => $journal_id
        ]);

        dispatch(new JournalLocalStoreJob());

        return response()->json([
            'response' => 'Successfully added achivement'
        ]);
    }

    /**
    * @param Request $request
    *      (integer) $request->input('journal_achievement_id')
    *       (string) $request->input('field')
    *     (variable) $request->input('value')
    * @return \Illuminate\Http\JsonResponse
    */
    public function ajaxEditJournalAchievementPost(Request $request)
    {
        $journal_achievement_id = $request->input('journal_achievement_id');
        $field = $request->input('field');
        $value = $request->input('value');

        JournalAchievement::where('id', '=', $journal_achievement_id)
            ->update([
                $field => $value
            ]);

        dispatch(new JournalLocalStoreJob());

        return response()->json([
            'response' => 'This has successfully been updated'
        ]);
    }

    /**
    * @param Request $request
    *      (integer) $request->input('journal_id')
    *      (integer) $request->input('journal_request_id')
    * @return \Illuminate\Http\JsonResponse
    */
    public function ajaxDeleteJournalAchievementPost(Request $request)
    {
        $journal_id = $request->input('journal_id');
        $journal_achievement_id = $request->input('journal_achievement_id');

        JournalAchievement::where('journal_id', '=', $journal_id)
            ->where('id', '=', $journal_achievement_id)->delete();

        dispatch(new JournalLocalStoreJob());

        return response()->json([
            'response' => 'Successfully deleted achievement from this journal'
        ]);
    }
}
