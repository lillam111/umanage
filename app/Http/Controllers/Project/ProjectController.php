<?php

namespace App\Http\Controllers\Project;

use Illuminate\Http\Request;
use App\Models\Project\Project;
use App\Models\Task\TaskStatus;
use Illuminate\Support\Facades\Auth;
use App\Models\Project\ProjectSetting;
use App\Http\Controllers\MasterController;

class ProjectController extends MasterController
{
    /**
    * This method is for returning the projects view to the user, this will return just a simple view with all the
    * projects that the specific user in question can see.
    *
    * @param Request $request
    * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
    */
    public function viewProjectsGet(Request $request)
    {
        // get all of the projects that this user is allowed to see, we may also want to collect all the projects that
        // the user has been given permission to see... if you are yourself (the auth user, requesting user) inside of
        // another project as a contributor, then you are going to be able to see all of those projects on your
        // dashboards.
        $projects = Project::select('*')
            ->with(['tasks'])
            ->where('creator_user_id', '=', Auth::id())
            ->get();

        return view('project.view_projects', compact(
            'projects'
        ));
    }

    public function ajaxViewProjectsGet(Request $request)
    {
        $user_id = Auth::id();

        if (! empty($request->input('user_id'))) {
            $user_id = $request->input('user_id');
        }

        $title = $request->input('title');

        $projects = Project::select('*')
            ->with(['tasks'])
            ->where('creator_user_id', '=', $user_id)
            ->get();

        return view('library.project.ajax_view_projects', compact(
            'projects',
            'title'
        ));
    }

    /**
    * This method is for returning a project specifically, we are going to be looking for a project with the passed id
    * and then we are going to check whether or not this user in question can see this particular project... if the
    * signed in user cannot see the project the user will be sent back to the view projects page.
    *
    * @param Request $request
    *       (string) $request->input('task_statuses')
    *       (string) $request->input('task_issue_types')
    *       (string) $request->input('task_priorities')
    * @param $project_id
    * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
    */
    public function viewProjectGet(Request $request, $project_id)
    {
        $project = Project::select('*')
            ->with([
                'user_contributors',
                'user_contributors.user',
                'tasks' => function ($qry) {
                    $qry->where('task_status_id', '!=', TaskStatus::$TYPE_DELETED);
                }
            ])
            ->where('id', '=', $project_id)
            ->first();

        // if we have gone to this particular url with filters in mind, then we are going to objectify the filters
        // so that we can pass them through in one object string rather than defining a bulk amount of filter variables.
        $filters = (object) [
            'task_statuses' => $request->input('task_status') ?? '',
            'task_issue_types' => $request->input('task_issue_type') ?? '',
            'task_priorities' => $request->input('task_priority') ?? ''
        ];

        // if there is no project for the above query, then we can assume that the user either doesn't have the project
        // that they're requesting, or they're trying to view someone else's project that their profile doesn't have
        // access to... thus we are going to need to redirect the user back.
        if (! $project instanceof Project) {
            return redirect()->action('Project\ProjectController@viewProjectsGet');
        }

        // we are going to check to see if this particular user that is signed in, is able to view the project that
        // they're navigating to, if not then we are going to return the user back to the previous page...
        if (Auth::user()->cannot('ProjectGate@viewProject', $project)) {
            return redirect()->action('Project\ProjectController@viewProjectsGet');
        }

        $this->view_service->set('title', " - Project - {$project->name}");

        return view('project.view_project',  compact(
            'project',
            'filters'
        ));
    }

    /**
    * This method is entirely for creating projects, this will be requiring the necessities for what a project is needed
    * otherwise the values will be entered as null. The only things that are necessary from a passed reference is the
    * name of the project.
    *
    * @param Request $request
    *                $request->input('name')
    *                $request->input('code')
    *                $request->input('description')
    *                $request->input('icon')
    *                $request->input('color')
    * @return \Illuminate\Http\JsonResponse
    */
    public function ajaxMakeProjectPost(Request $request)
    {
        // Create the project.
        $project = Project::create([
            'creator_user_id' => Auth::id(),
            'name' => $request->input('name'),
            'code' => $request->input('code'),
            'description' => $request->input('description'),
            'icon' => $request->input('icon'),
            'color' => $request->input('color'),
        ]);

        // once the project has been created... create the project settings.
        ProjectSetting::create([
            'project_id' => $project->id,
            'view_id' => 1,
            'total_tasks' => 0,
            'completed_tasks' => 0
        ]);

        return response()->json([
            'response' => 'project successfully created'
        ]);
    }
}
