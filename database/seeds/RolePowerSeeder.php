<?php

use App\Models\Permission\Role;
use Illuminate\Database\Seeder;
use App\Models\Permission\Power;
use App\Models\Permission\RolePower;

class RolePowerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = Role::all();
        $powers = Power::all();

        // delete all that currently exist int he system, we are going  to be refreshing all the permissions into the
        // necessary roles.
        RolePower::whereNotNull('power_id')->delete();

        foreach ($roles as $role) {
            foreach ($powers as $power) {
                if ($role->level <= $power->level) {
                    RolePower::create([
                        'role_id' => $role->id,
                        'power_id' => $power->id
                    ]);
                }
            }
        }
    }
}
