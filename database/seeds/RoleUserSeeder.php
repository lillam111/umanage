<?php

use App\Models\Permission\Role;
use Illuminate\Database\Seeder;
use App\Models\Permission\RoleUser;

class RoleUserSeeder extends Seeder
{
    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run()
    {
        $roles = Role::all()->keyBy('name');

        $user_roles = [
            1 => $roles->get('Developer')->id
        ];

        foreach ($user_roles as $user_id => $role_id) {
            RoleUser::updateOrCreate(['role_id' => $role_id, 'user_id' => $user_id], [
                'role_id' => $role_id,
                'user_id' => $user_id
            ]);
        }
    }
}
