<?php

use Illuminate\Database\Seeder;
use App\Models\Task\TaskComment;

class TaskCommentSeeder extends Seeder
{
    public $key = 0;

    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run()
    {
        $comments = [
            $this->increment() => (object) [
                'content' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla cursus justo id libero cursus, vitae lacinia dui dapibus. Donec vitae metus faucibus, fermentum turpis sit amet, fringilla felis. Proin vitae fringilla lorem. Praesent a lacinia risus, ac venenatis lorem. Vestibulum nec orci eu urna molestie vulputate sit amet ut lacus.</p>',
                'user_id' => 1,
            ],
            $this->increment() => (object) [
                'content' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla cursus justo id libero cursus, vitae lacinia dui dapibus. Donec vitae metus faucibus, fermentum turpis sit amet, fringilla felis. Proin vitae fringilla lorem. Praesent a lacinia risus, ac venenatis lorem. Vestibulum nec orci eu urna molestie vulputate sit amet ut lacus.</p>',
                'user_id' => 2,
            ],
            $this->increment() => (object) [
                'content' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla cursus justo id libero cursus, vitae lacinia dui dapibus. Donec vitae metus faucibus, fermentum turpis sit amet, fringilla felis. Proin vitae fringilla lorem. Praesent a lacinia risus, ac venenatis lorem. Vestibulum nec orci eu urna molestie vulputate sit amet ut lacus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla cursus justo id libero cursus, vitae lacinia dui dapibus. Donec vitae metus faucibus, fermentum turpis sit amet, fringilla felis. Proin vitae fringilla lorem. Praesent a lacinia risus, ac venenatis lorem. Vestibulum nec orci eu urna molestie vulputate sit amet ut lacus.</p>',
                'user_id' => 3,
            ],
            $this->increment() => (object) [
                'content' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla cursus justo id libero cursus, vitae lacinia dui dapibus. Donec vitae metus faucibus, fermentum turpis sit amet, fringilla felis. Proin vitae fringilla lorem. Praesent a lacinia risus, ac venenatis lorem. Vestibulum nec orci eu urna molestie vulputate sit amet ut lacus.</p>',
                'user_id' => 1,
            ],
            $this->increment() => (object) [
                'content' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla cursus justo id libero cursus, vitae lacinia dui dapibus. Donec vitae metus faucibus, fermentum turpis sit amet, fringilla felis. Proin vitae fringilla lorem. Praesent a lacinia risus, ac venenatis lorem. Vestibulum nec orci eu urna molestie vulputate sit amet ut lacus.</p>',
                'user_id' => 1,
            ],
        ];

        $bar = $this->command->getOutput()->createProgressBar(count($comments));

        foreach ($comments as $comment_id => $comment) {
            TaskComment::updateOrCreate(['id' => $comment_id], [
                'id' => $comment_id,
                'content' => $comment->content,
                'user_id' => $comment->user_id,
                'task_id' => 1,
            ]);
            $bar->advance();
        }
        $bar->finish();
    }

    public function increment()
    {
        $this->key += 1;
        return $this->key;
    }
}
