<?php

use Illuminate\Database\Seeder;
use App\Models\Project\Project;
use App\Models\Project\ProjectSetting;

class ProjectSeeder extends Seeder
{
    public $key = 0;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $projects = [
            $this->increment() => (object) [ 'creator_user_id' => 1, 'name' => 'uManage',   'code' => 'UM', 'icon' => 'fa fa-edit',   'color' => '1e87f0' ],
            $this->increment() => (object) [ 'creator_user_id' => 1, 'name' => 'Portfolio', 'code' => 'PF', 'icon' => 'fa fa-edit',   'color' => 'f0506e' ],
            $this->increment() => (object) [ 'creator_user_id' => 1, 'name' => 'Games',     'code' => 'GM', 'icon' => 'fa fa-edit',   'color' => 'ffa500' ],
            $this->increment() => (object) [ 'creator_user_id' => 1, 'name' => 'Research',  'code' => 'RE', 'icon' => 'fa fa-search', 'color' => '228C22' ],
            $this->increment() => (object) [ 'creator_user_id' => 2, 'name' => 'Zutile',    'code' => 'ZU', 'icon' => 'fa fa-search', 'color' => '1e87f0' ],
            $this->increment() => (object) [ 'creator_user_id' => 1, 'name' => 'Languages', 'code' => 'LG', 'icon' => 'fa fa-globe',  'color' => '181818' ]
        ];

        $bar = $this->command->getOutput()->createProgressBar(count($projects));

        foreach ($projects as $project_id => $project) {
            Project::updateOrCreate(['id' => $project_id], [
                'id' => $project_id,
                'creator_user_id' => $project->creator_user_id,
                'name' => $project->name,
                'code' => $project->code,
                'icon' => $project->icon,
                'color' => $project->color
            ]);

            ProjectSetting::updateOrCreate(['project_id' => $project_id], [
                'project_id' => $project_id,
                'view_id' => 1,
                'total_tasks' => 0,
                'completed_tasks' => 0
            ]);

            $bar->advance();
        }

        $bar->finish();
    }

    public function process()
    {

    }

    public function increment()
    {
        $this->key += 1;
        return $this->key;
    }
}