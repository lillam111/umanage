<?php

use App\Models\Task\Task;
use App\Models\User\User;
use Illuminate\Database\Seeder;
use App\Models\Task\TaskWatcherUser;

class TaskWatcherUserSeeder extends Seeder
{
    /**
    * Run the database seeds. This will apply every user in the system as a watcher to evrery task in the system...
    * so that I am able to test the relationships of the task watchers... this will want truncating and only should ever
    * be ran so that you can test relationships
    *
    * @return void
    */
    public function run()
    {
        $tasks = Task::select('*')->with(['project'])->get();
        $users = User::all();

        $bar = $this->command->getOutput()->createProgressBar($tasks->count());

        foreach ($tasks as $task) {
            foreach ($users as $user) {
                $task_watcher_user = TaskWatcherUser::where('task_id', '=', $task->id)
                    ->where('user_id', '=', $user->id)->first();

                if (! $task_watcher_user instanceof TaskWatcherUser) {
                    $task_watcher_user = new TaskWatcherUser();
                }

                $task_watcher_user->user_id = $user->id;
                $task_watcher_user->task_id = $task->id;
                $task_watcher_user->save();
            }
            $bar->advance();
        }
        $bar->finish();
    }
}
