<?php

use Carbon\Carbon;
use App\Models\Task\Task;
use App\Models\User\User;
use App\Models\Timelog\Timelog;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Repositories\Timelog\TimelogRepository;

class TimelogSeeder extends Seeder
{
    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run()
    {
        $users = User::all();
        $tasks = Task::all();

        DB::transaction(function() use ($users, $tasks) {
            $bar = $this->command->getOutput()->createProgressBar(count($users));
            foreach ($users as $user) {
                foreach ($tasks as $task) {
                    Timelog::create([
                        'task_id' => $task->id,
                        'user_id' => $user->id,
                        'note' => 'lol',
                        'from' => Carbon::parse(Carbon::now())->subHour(1),
                        'to' => Carbon::now(),
                        'time_spent' => TimelogRepository::translateTimespent('1h')
                    ]);
                }
                $bar->advance();
            }
            $bar->finish();
        });

        DB::commit();
    }
}
