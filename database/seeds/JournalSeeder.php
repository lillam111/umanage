<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class JournalSeeder extends Seeder
{
    /**
    * This method is 100% reading from a local file that constantly gets saved to whenever we are making amends or
    * creating new journal entries. this is for being able to retain the very important information, whilst retaining
    * the ability to fresh migrate the database.
    *
    * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
    * @return void
    */
    public function run()
    {
        $journals = collect(json_decode(
            Storage::disk('local')
                ->get('journals/journals.txt')
        ));

        DB::transaction(function () use ($journals) {
            $bar = $this->command->getOutput()->createProgressBar($journals->count());
            foreach ($journals as $journal) {
                DB::table('journal')->insert([
                    'id' => $journal->id,
                    'user_id' => $journal->user_id,
                    'rating' => $journal->rating,
                    'overall' => $journal->overall,
                    'lowest_point' => $journal->lowest_point,
                    'highest_point' => $journal->highest_point,
                    'when' => Carbon::parse($journal->when),
                    'created_at' => Carbon::parse($journal->created_at),
                    'updated_at' => Carbon::parse($journal->updated_at)
                ]);

                if (! empty($journal->journal_achievements)) {
                    foreach ($journal->journal_achievements as $journal_achievement) {
                        DB::table('journal_achievement')->insert([
                            'id' => $journal_achievement->id,
                            'journal_id' => $journal_achievement->journal_id,
                            'name' => $journal_achievement->name,
                            'created_at' => Carbon::parse($journal_achievement->created_at),
                            'updated_at' => Carbon::parse($journal_achievement->updated_at)
                        ]);
                    }
                }
                $bar->advance();
            }
            $bar->finish();
        });

        DB::commit();
    }
}
