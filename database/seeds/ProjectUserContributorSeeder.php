<?php

use App\Models\User\User;
use App\Models\Project\Project;
use Illuminate\Database\Seeder;
use App\Models\Project\ProjectUserContributor;

class ProjectUserContributorSeeder extends Seeder
{
    /**
    * Run the database seeds.
    * This seeder is specifically targetted for inserting all users against all projects, that aren't their own. so
    * that I am able to test the connections between users and the people that they are working with.
    *
    * @return void
    */
    public function run()
    {
        $projects = Project::all();
        $users = User::all();

        $bar = $this->command->getOutput()->createProgressBar($users->count());

        foreach ($users as $user) {
            foreach ($projects as $project) {
                // if this user is the owner of the project, then don't bother contributing to this particular project
                if ($project->creator_user_id === $user->id) {
                    continue;
                }

                ProjectUserContributor::updateOrCreate([
                    'user_id' => $user->id,
                    'project_id' => $project->id
                ]);
            }
            $bar->advance();
        }
        $bar->finish();
    }
}
