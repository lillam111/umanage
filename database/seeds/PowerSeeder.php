<?php

use Illuminate\Database\Seeder;
use App\Models\Permission\Power;

class PowerSeeder extends Seeder
{
    public $key = 0;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $powers = config('gates');
        foreach ($powers as $power => $power_level) {
            $power_id = $this->increment();
            Power::updateOrCreate(['id' => $power_id], [
                'id' => $power_id,
                'name' => $power,
                'level' => $power_level
            ]);
        }
    }

    public function increment()
    {
        $this->key += 1;
        return $this->key;
    }
}
