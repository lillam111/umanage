<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call([
             // user seeders
             UserTableSeeder::class,
             // project seeders
             ProjectSeeder::class,
             ProjectUserContributorSeeder::class,
             // task seeders
             TaskStatusSeeder::class,
             TaskPrioritySeeder::class,
             TaskIssueTypeSeeder::class,
             TaskSeeder::class,
             TaskCommentSeeder::class,
             TaskWatcherUserSeeder::class,
             // journal seeders
             JournalSeeder::class,
             // timelog seeders
             TimelogSeeder::class,
             // permission seeders
             RoleSeeder::class,
             RoleUserSeeder::class,
             PowerSeeder::class,
             RolePowerSeeder::class,
         ]);
    }
}
