<?php

use App\Models\Task\Task;
use Illuminate\Database\Seeder;

class TaskSeeder extends Seeder
{
    public $key = 0;

    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run()
    {
        $tasks = [
            $this->increment() => (object) [
                'project_id' => 1,
                'name' => 'Setup user themes',
                'description' => '<p>The user is going to want to be able to have their own theme set up, colouring, imaging etc and a place to manage all of this.</p>',
                'task_issue_type_id' => 1,
                'reporter_user_id' => 1,
                'assigned_user_id' => null,
                'task_status_id' => 1,
                'task_priority_id' => 1,
                'due_date' => null
            ],
            $this->increment() => (object) [
                'project_id' => 1,
                'name' => 'Set up task filters',
                'description' => '<p>The filtration system is going to need to allow task break down so that I am capable of searching for something specific</p>',
                'task_issue_type_id' => 1,
                'reporter_user_id' => 1,
                'assigned_user_id' => null,
                'task_status_id' => 1,
                'task_priority_id' => 3,
                'due_date' => null
            ],
            $this->increment() => (object) [
                'project_id' => 1,
                'name' => 'Create ability to add tasks',
                'description' => '<p>We need the ability to be able to create tasks...</p>',
                'task_issue_type_id' => 1,
                'reporter_user_id' => 1,
                'assigned_user_id' => null,
                'task_status_id' => 1,
                'task_priority_id' => 3,
                'due_date' => null
            ],

            $this->increment() => (object) [
                'project_id' => 2,
                'name' => 'Plan CMS build',
                'description' => '<p>The old CMS (Content management System) was recently deleted, because it wasn\'t the best usable... this will need redoing so that I am going to be able to manage the content on the portfolio website.</p>',
                'task_issue_type_id' => 2,
                'reporter_user_id' => 1,
                'assigned_user_id' => null,
                'task_status_id' => 1,
                'task_priority_id' => 5,
                'due_date' => null
            ],
            $this->increment() => (object) [
                'project_id' => 2,
                'name' => 'Fix News segment on home page',
                'description' => '<p>The homepage news section is reporting laravel news when it just wants to be news...</p>',
                'task_issue_type_id' => 2,
                'reporter_user_id' => 1,
                'assigned_user_id' => null,
                'task_status_id' => 1,
                'task_priority_id' => 4,
                'due_date' => null
            ],

            $this->increment() => (object) [
                'project_id' => 3,
                'name' => 'Create Console Commander',
                'description' => '<p>Im wanting  to create a javascript oriented game that is entirely focused around the console being the cotroller for the game</p>',
                'task_issue_type_id' => 1,
                'reporter_user_id' => 1,
                'assigned_user_id' => null,
                'task_status_id' => 1,
                'task_priority_id' => 4,
                'due_date' => null
            ],
            $this->increment() => (object) [
                'project_id' => 6,
                'name' => 'Spanish',
                'description' => '',
                'task_issue_type_id' => 1,
                'reporter_user_id' => 1,
                'assigned_user_id' => null,
                'task_status_id' => 1,
                'task_priority_id' => 4,
                'due_date' => null
            ],
            $this->increment() => (object) [
                'project_id' => 6,
                'name' => 'French',
                'description' => '',
                'task_issue_type_id' => 1,
                'reporter_user_id' => 1,
                'assigned_user_id' => null,
                'task_status_id' => 1,
                'task_priority_id' => 4,
                'due_date' => null
            ],
            $this->increment() => (object) [
                'project_id' => 6,
                'name' => 'German',
                'description' => '',
                'task_issue_type_id' => 1,
                'reporter_user_id' => 1,
                'assigned_user_id' => null,
                'task_status_id' => 1,
                'task_priority_id' => 4,
                'due_date' => null
            ],
        ];

        $this->process($tasks);
    }

    public function process($tasks)
    {
        $bar = $this->command->getOutput()->createProgressBar(count($tasks));
        foreach ($tasks as $task_id => $task) {
            $task = Task::updateOrCreate(['id' => $task_id], [
                'id' => $task_id,
                'project_id' => $task->project_id,
                'name' => $task->name,
                'description' => $task->description,
                'task_issue_type_id' => $task->task_issue_type_id,
                'reporter_user_id' => $task->reporter_user_id,
                'assigned_user_id' => $task->assigned_user_id,
                'task_status_id' => $task->task_status_id,
                'task_priority_id' => $task->task_priority_id,
                'due_date' => $task->due_date,
            ]);

            $task->project->project_setting->increment('total_tasks');
            $bar->advance();
        }
        $bar->finish();
    }

    public function increment()
    {
        $this->key += 1;
        return $this->key;
    }
}
