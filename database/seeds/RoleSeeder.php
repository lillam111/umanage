<?php

use Illuminate\Database\Seeder;
use App\Models\Permission\Role;

class RoleSeeder extends Seeder
{
    public $key = 0;

    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run()
    {
        $roles = [
            $this->increment() => (object) [
                'name' => 'Developer',
                'level' => 1
            ],
            $this->increment() => (object) [
                'name' => 'Admin',
                'level' => 2
            ],
        ];

        $this->process($roles);
    }

    /**
    * @param $roles
    */
    public function process($roles)
    {
        foreach ($roles as $role_id => $role) {
            Role::updateOrCreate(['id' => $role_id], [
                'id' => $role_id,
                'name' => $role->name,
                'level' => $role->level
            ]);
        }
    }

    /**
    * @return int
    */
    public function increment()
    {
        $this->key += 1;
        return $this->key;
    }
}