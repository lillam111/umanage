<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class JournalModule extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->setupJournalModule();
        $this->setupJournalAchievements();
    }

    public function setupJournalModule()
    {
        if (! file_exists('app/Models/Journal/Journal.php')) {
            Artisan::call('make:model Models/Journal/Journal');
        }

        if (! file_exists('app/Http/Controllers/Journal/JournalController.php')) {
            Artisan::call('make:controller Journal/JournalController');
        }

        Schema::create('journal', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedSmallInteger('rating')->nullable();
            $table->longText('overall')->nullable();
            $table->longText('lowest_point')->nullable();
            $table->longText('highest_point')->nullable();
            $table->date('when', 20);
            $table->timestamps();
        });

        Schema::table('journal', function (Blueprint $table) {
            $table->foreign('user_id')
                ->references('id')
                ->on('user')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    public function setupJournalAchievements()
    {
        if (! file_exists('app/Models/Journal/JournalAchievement.php')) {
            Artisan::call('make:model Models/Journal/JournalAchievement');
        }

        if (! file_exists('app/Http/Controllers/Journal/JournalAchievementController.php')) {
            Artisan::call('make:controller Journal/JournalAchievementController');
        }

        Schema::create('journal_achievement', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('journal_id');
            $table->string('name', 191);
            $table->timestamps();
        });

        Schema::table('journal_achievement', function (Blueprint $table) {
            $table->foreign('journal_id')
                ->references('id')
                ->on('journal')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
