<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsModule extends Migration
{
    /**
    * Run the migrations.
    *
    * @return void
    * This method is for setting up the project module, this will iterate over everything 1x1 in the order specified in
    * the method. please consider keys when making amendments and ensure that the keys that are required are in place
    * for the new elements added.
    */
    public function up()
    {
        $this->setupProjectModule();
        $this->setupProjectSettingsModule();
        $this->setupProjectUserContributorModule();
    }

    public function setupProjectModule()
    {
        if (! file_exists('app/Models/Project/Project.php')) {
            Artisan::call('make:model Models/Project/Project');
        }

        if (! file_exists('app/Http/Controllers/Project/ProjectController.php')) {
            Artisan::call('make:controller Project/ProjectController');
        }

        Schema::create('project', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('creator_user_id');
            $table->string('name', 191);
            $table->string('code', 2)
                ->comment('if left blank, will be generated from first 2 characters of title')
                ->unique();
            $table->longText('description')->nullable();
            $table->string('icon', 30)->nullable();
            $table->string('color', 10)->nullable();
            $table->timestamps();
        });

        Schema::table('project', function (Blueprint $table) {
            // Foreign Key
            $table->foreign('creator_user_id')
                ->references('id')
                ->on('user')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    public function setupProjectSettingsModule()
    {
        if (! file_exists('app/Models/Project/ProjectSetting.php')) {
            Artisan::call('make:model Models/Project/ProjectSetting');
        }

        if (! file_exists('app/Http/Controllers/Project/ProjectSettingController.php')) {
            Artisan::call('make:controller Project/ProjectSettingController');
        }

        Schema::create('project_setting', function (Blueprint $table) {
            $table->unsignedBigInteger('project_id');
            $table->unsignedSmallInteger('view_id')->comment('Enum column, values stored in code.');

            // Task Statistics...
            $table->integer('total_tasks')->default(0);
            $table->integer('completed_tasks')->default(0);

            // timestamps.
            $table->timestamps();
        });

        Schema::table('project_setting', function (Blueprint $table) {
            $table->primary('project_id');
            $table->foreign('project_id')
                ->references('id')
                ->on('project')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    public function setupProjectUserContributorModule()
    {
        if (! file_exists('app/Models/Project/ProjectUserContributor.php')) {
            Artisan::call('make:model Models/Project/ProjectUserContributor');
        }

        if (! file_exists('app/Http/Controllers/Project/ProjectUserContributorController.php')) {
            Artisan::call('make:controller Project/ProjectUserContributorController');
        }

        Schema::create('project_user_contributor', function (Blueprint $table) {
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('project_id');
            $table->timestamps();
        });

        Schema::table('project_user_contributor', function (Blueprint $table) {
            $table->primary(['user_id', 'project_id'], 'user_project_contributor_id');

            $table->foreign('user_id')
                ->references('id')
                ->on('user')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('project_id')
                ->references('id')
                ->on('project')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
    * Reverse the migrations.
    *
    * @return void
    * This method is for stripping the database down of everything that was added above. this shouldn't really ever be
    * called... one should never really need to revert their migrations as you will just delete the database and start
    * over but this could be worth while to have in place in order for a case where i might need to, this will also need
    * to strip them out in the reverse order of which they're made in.
    */
    public function down()
    {
        Schema::dropIfExists('project_user_contributor');
        Schema::dropIfExists('project_setting');
        Schema::dropIfExists('project');
    }
}
