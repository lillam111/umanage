<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PermissionModule extends Migration
{
    /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        $this->setupPowerModule();
        $this->setupRoleModule();
        $this->setupRoleUserModule();
        $this->setupRolePowerModule();
    }

    /**
    * @return void
    */
    public function setupPowerModule()
    {
        if (! file_exists('app/Models/Permission/Power.php')) {
            Artisan::call('make:model Models/Permission/Power');
        }

        Schema::create('power', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 191);
            $table->smallInteger('level');
        }); return;
    }

    /**
    * @return void
    */
    public function setupRoleModule()
    {
        if (! file_exists('app/Models/Permission/Role.php')) {
            Artisan::call('make:model Models/Permission/Role');
        }

        Schema::create('role', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 191);
            $table->smallInteger('level');
        }); return;
    }

    /**
    * @return void
    */
    public function setupRoleUserModule()
    {
        if (! file_exists('app/Models/Permission/RoleUser.php')) {
            Artisan::call('make:model Models/Permission/RoleUser');
        }

        Schema::create('role_user', function (Blueprint $table) {
            $table->unsignedBigInteger('role_id');
            $table->unsignedBigInteger('user_id');
        });

        Schema::table('role_user', function (Blueprint $table) {
            $table->primary(['role_id', 'user_id'], 'role_user_id');

            $table->foreign('role_id')
                ->references('id')
                ->on('role')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('user_id')
                ->references('id')
                ->on('user')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        }); return;
    }

    /**
    * @return void
    */
    public function setupRolePowerModule()
    {
        if (! file_exists('app/Models/Permission/RolePower.php')) {
            Artisan::call('make:model Models/Permission/RolePower');
        }

        Schema::create('role_power', function (Blueprint $table) {
            $table->unsignedBigInteger('role_id');
            $table->unsignedBigInteger('power_id');
        });

        Schema::table('role_power', function (Blueprint $table) {
            $table->primary(['role_id', 'power_id'], 'role_power_id');

            $table->foreign('role_id')
                ->references('id')
                ->on('role')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('power_id')
                ->references('id')
                ->on('power')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        }); return;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}