<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksModule extends Migration
{
    /**
    * Run the migrations.
    * This method is for setting up the task system in general, all things related to tasks will be set up within this
    * particular method set. up will be running through all the set up processes that are set within this class...
    * the process in which these are handled will need to be in order of which keys are needing to be set, a key cannot
    * be set on a row that isn't there, so be careful when adding to this ordering and take keys into consideration
    *
    * @return void
    */
    public function up()
    {
        $this->setupTaskPriorityModule();
        $this->setupTaskStatusModule();
        $this->setupTaskIssueTypeModule();
        $this->setupTaskModule();
        $this->setUpTaskFileModule();
        $this->setupTaskCommentModule();
        $this->setupTaskWatcherUserModule();
        $this->setupTaskChecklistModule();
        $this->setUpTaskChecklistItemModule();
        $this->setupTaskLogModule();
    }

    public function setupTaskModule()
    {
        if (! file_exists('app/Models/Task/Task.php')) {
            Artisan::call('make:model Models/Task/Task');
        }

        if (! file_exists('app/Http/Controllers/Task/TaskController.php')) {
            Artisan::call('make:controller Task/TaskController');
        }

        Schema::create('task', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('project_id');
            $table->unsignedBigInteger('reporter_user_id')->nullable();
            $table->unsignedBigInteger('assigned_user_id')->nullable();
            $table->unsignedBigInteger('task_issue_type_id')->nullable();
            $table->unsignedBigInteger('task_status_id')->nullable();
            $table->unsignedBigInteger('task_priority_id')->nullable();
            $table->string('name', 191);
            $table->longText('description')->nullable();
            $table->dateTime('due_date')->nullable()->default(null);
            $table->timestamps();
        });

        Schema::table('task', function (Blueprint $table) {
            $table->foreign('project_id')
                ->references('id')
                ->on('project')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('reporter_user_id')
                ->references('id')
                ->on('user')
                ->onUpdate('cascade')
                ->onDelete('set null');

            $table->foreign('assigned_user_id')
                ->references('id')
                ->on('user')
                ->onUpdate('cascade')
                ->onDelete('set null');

            $table->foreign('task_issue_type_id')
                ->references('id')
                ->on('task_issue_type')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('task_status_id')
                ->references('id')
                ->on('task_status')
                ->onUpdate('cascade')
                ->onDelete('set null');

            $table->foreign('task_priority_id')
                ->references('id')
                ->on('task_priority')
                ->onUpdate('cascade')
                ->onDelete('set null');
        });
    }

    public function setupTaskStatusModule()
    {
        if (! file_exists('app/Models/Task/TaskStatus.php')) {
            Artisan::call('make:model Models/Task/TaskStatus');
        }

        if (! file_exists('app/Http/Controllers/Task/TaskStatusController.php')) {
            Artisan::call('make:controller Task/TaskStatusController');
        }

        Schema::create('task_status', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedSmallInteger('type');
            $table->string('name', 191);
            $table->string('color',  10)->nullable();
            $table->timestamps();
        });
    }

    public function setupTaskPriorityModule()
    {
        if (! file_exists('app/Models/Task/TaskPriority.php')) {
            Artisan::call('make:model Models/Task/TaskPriority');
        }

        if (! file_exists('app/Http/Controllers/Task/TaskPriorityController.php')) {
            Artisan::call('make:controller Task/TaskPriorityController');
        }

        Schema::create('task_priority', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 191);
            $table->string('color', 10)->nullable();
            $table->string('icon', 30)->nullable();
            $table->timestamps();
        });
    }

    public function setupTaskIssueTypeModule()
    {
        if (! file_exists('app/Models/Task/TaskIssueType.php')) {
            Artisan::call('make:model Models/Task/TaskIssueType');
        }

        if (! file_exists('app/Http/Controllers/Task/TaskIssueTypeController.php')) {
            Artisan::call('make:controller Task/TaskIssueTypeController');
        }

        Schema::create('task_issue_type', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 191);
            $table->string('color', 10)->nullable();
            $table->string('icon', 30)->nullable();
            $table->timestamps();
        });
    }

    public function setupTaskCommentModule()
    {
        if (! file_exists('app/Models/Task/TaskComment.php')) {
            Artisan::call('make:model Models/Task/TaskComment');
        }

        if (! file_exists('app/Http/Controllers/Task/TaskCommentController.php')) {
            Artisan::call('make:controller Task/TaskCommentController');
        }

        Schema::create('task_comment', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('parent_id')->nullable();
            $table->unsignedBigInteger('task_id');
            $table->unsignedBigInteger('user_id');
            $table->longText('content');
            $table->timestamps();
        });

        Schema::table('task_comment', function (Blueprint $table) {
            $table->foreign('parent_id')
                ->references('id')
                ->on('task_comment')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('task_id')
                ->references('id')
                ->on('task')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('user_id')
                ->references('id')
                ->on('user')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    public function setupTaskWatcherUserModule()
    {
        if (! file_exists('app/Models/Task/TaskWatcherUser')) {
            Artisan::call('make:model Models/Task/TaskWatcherUser');
        }

        if (! file_exists('app/Http/Controllers/Task/TaskWatcherUserController.php')) {
            Artisan::call('make:controller Task/TaskWatcherUserController');
        }

        Schema::create('task_watcher_user', function (Blueprint $table) {
            $table->unsignedBigInteger('task_id');
            $table->unsignedBigInteger('user_id');
            $table->timestamps();
        });

        Schema::table('task_watcher_user', function (Blueprint $table) {
            // primary key
            $table->primary(['task_id', 'user_id'], 'task_project_user_watcher_id');
            // foreign key
            $table->foreign('task_id')
                ->references('id')
                ->on('task')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('user_id')
                ->references('id')
                ->on('user')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    public function setupTaskChecklistModule()
    {
        if (! file_exists('app/Models/Task/')) {
            Artisan::call('make:model Models/Task/TaskChecklist');
        }

        if (! file_exists('app/Http/Controllers/Task/TaskChecklistController.php')) {
            Artisan::call('make:controller Task/TaskChecklistController');
        }

        Schema::create('task_checklist', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger( 'task_id');
            $table->string('name', 191);
            $table->unsignedSmallInteger('order')->nullable();
            $table->timestamps();
        });

        Schema::table('task_checklist', function (Blueprint $table) {
            $table->foreign('task_id')
                ->references('id')
                ->on('task')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    public function setUpTaskChecklistItemModule()
    {
        if (! file_exists('app/Models/Task/TaskChecklistItem.php')) {
            Artisan::call('make:model Models/Task/TaskChecklistItem');
        }

        if (! file_exists('app/Http/Controllers/Task/TaskChecklistItemController.php')) {
            Artisan::call('make:controller Task/TaskChecklistItemController');
        }

        Schema::create('task_checklist_item', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('task_checklist_id');
            $table->string('name', 191);
            $table->unsignedSmallInteger('order')->nullable();
            $table->boolean('is_checked')->default(0);
            $table->timestamps();
        });

        Schema::table('task_checklist_item', function (Blueprint $table) {
            // Foreign Keys
            $table->foreign('task_checklist_id')
                ->references('id')
                ->on('task_checklist')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    public function setUpTaskFileModule()
    {
        if (! file_exists('app/Models/Task/TaskFile.php')) {
            Artisan::call('make:model Models/Task/TaskFile');
        }

        if (! file_exists('app/Http/Controllers/Task/TaskFileController.php')) {
            Artisan::call('make:controller Task/TaskFileController');
        }

        Schema::create('task_file', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('task_id');
            $table->text('file');
            $table->string('mimetype');
            $table->timestamps();
        });

        Schema::table('task_file', function (Blueprint $table) {
            // Foreign Keys
            $table->foreign('task_id')
                ->references('id')
                ->on('task')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    public function setupTaskLogModule()
    {
        if (! file_exists('app/Models/Task/TaskLog.php')) {
            Artisan::call('make:model Models/Task/TaskLog');
        }

        if (! file_exists('app/Http/Controllers/Task/TaskLogController.php')) {
            Artisan::call('make:controller Task/TaskLogController');
        }

        Schema::create('task_log', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id')->nullable();
            $table->unsignedBigInteger('project_id')->nullable();
            $table->unsignedBigInteger('task_id')->nullable();
            $table->unsignedBigInteger('task_checklist_id')->nullable();
            $table->unsignedBigInteger('task_checklist_item_id')->nullable();
            $table->unsignedTinyInteger('type');
            $table->longText('old')->nullable();
            $table->longText('new')->nullable();
            $table->longText('extra')->nullable();
            $table->dateTime('when');
        });

        Schema::table('task_log', function (Blueprint $table) {
            $table->foreign('user_id')
                ->references('id')
                ->on('user')
                ->onUpdate('set null')
                ->onDelete('set null');

            $table->foreign('project_id')
                ->references('id')
                ->on('project')
                ->onUpdate('set null')
                ->onDelete('set null');

            $table->foreign('task_id')
                ->references('id')
                ->on('task')
                ->onUpdate('cascade')
                ->onDelete('set null');

             $table->foreign('task_checklist_id')
                ->references('id')
                ->on('task_checklist')
                ->onUpdate('cascade')
                ->onDelete('set null');

             $table->foreign('task_checklist_item_id')
                ->references('id')
                ->on('task_checklist_item')
                ->onUpdate('cascade')
                ->onDelete('set null');
        });
    }

    /**
    * Reverse the migrations.
    *
    * @return void
    * This method strips down the system... (database wise)... and will remove all of the tables that exist, which all
    * should exist. this method shouldn't really ever be called however. but this is just in here as a fallback if i
    * ever need to reset everything inside the task module.
    */
    public function down()
    {
        Schema::dropIfExists('task_status');
        Schema::dropIfExists('task_priority');
        Schema::dropIfExists('task_issue_type');
        Schema::dropIfExists('task');
        Schema::dropIfExists('task_file');
        Schema::dropIfExists('task_comment');
        Schema::dropIfExists('task_watcher_user');
        Schema::dropIfExists('task_checklist');
        Schema::dropIfExists('task_checklist_item');
        Schema::dropIfExists('task_log');
    }
}
