<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
    * Run the migrations.
    *
    * This method is called and once calling this it will iterate over all the methods that are stated within this,
    * from top to bottom... when creating more additions please take into consideration the keys that are needed and
    * required, the order will want to be in which the key constraints are needed to be set...
    *
    * @return void
    */
    public function up()
    {
        $this->setupUserModule();
        $this->setupUserDetailsModule();
        $this->setupUserSettingsModule();
    }

    public function setupUserModule()
    {
        if (! file_exists('app/Models/User/User.php')) {
            Artisan::call('make:model Models/User/User');
        }

        if (! file_exists('app/Http/Controllers/User/UserController.php')) {
            Artisan::call('make:controller User/UserController');
        }

        Schema::create('user', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('first_name', 191);
            $table->string('last_name', 191);
            $table->string('email')->unique();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    public function setupUserDetailsModule()
    {
        if (! file_exists('app/Models/User/UserDetail.php')) {
            Artisan::call('make:model Models/User/UserDetail');
        }

        if (! file_exists('app/Http/Controllers/User/UserDetailController.php')) {
            Artisan::call('make:controller User/UserDetailController');
        }

        Schema::create('user_detail', function (Blueprint $table) {
            $table->unsignedBigInteger('user_id');
            $table->string('job_title', 191)->nullable();
            $table->string('address_line_1', 191)->nullable();
            $table->string('address_line_2', 191)->nullable();
            $table->string('town', 191)->nullable();
            $table->string('city', 191)->nullable();
            $table->string('postcode', 50)->nullable();
        });

        Schema::table('user_detail', function (Blueprint $table) {
            $table->foreign('user_id')
                ->references('id')
                ->on('user')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    public function setupUserSettingsModule()
    {
        if (! file_exists('app/Models/User/UserSetting.php')) {
            Artisan::call('make:model Models/User/UserSetting');
        }

        if (! file_exists('app/Http/Controllers/User/UserSettingController.php')) {
            Artisan::call('make:controller User/UserSettingController');
        }

        Schema::create('user_setting', function (Blueprint $table) {
            $table->unsignedBigInteger('user_id');
            $table->string('theme_color', 12)->nullable();
        });

        Schema::table('user_setting', function (Blueprint $table) {
            $table->foreign('user_id')
                ->references('id')
                ->on('user')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
    * Reverse the migrations.
    *
    * This wants to be in the opposite order in which that the run method is going, this will be because the key
    * constraints that are set will affect the downing of each table... doing this in reverse order will be the perfect
    * way to ensure that tables are going to be downed without running into a key constraint rule
    *
    * @return void
    */
    public function down()
    {
        Schema::dropIfExists('user_setting');
        Schema::dropIfExists('user_detail');
        Schema::dropIfExists('user');
    }
}
