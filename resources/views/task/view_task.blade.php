@extends('template.master')
@section('styles')
    <link href="{{ asset('assets/vendor/summernote/summernote-lite.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('views/task/task_checklists/view_task_checklists.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('views/task/task_comments/view_task_comments.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('views/task/task_timelogs/view_task_logs.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('views/task/view_task.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('scripts')
    <script src="{{ asset('assets/vendor/summernote/summernote-lite.js') }}"></script>
    <script src="{{ asset('views/task/task_checklists/view_task_checklists.js') }}"></script>
    <script src="{{ asset('views/task/task_comments/view_task_comments.js') }}"></script>
    <script src="{{ asset('views/task/task_timelogs/view_task_logs.js') }}"></script>
    <script src="{{ asset('views/task/view_task.js') }}"></script>
@endsection
@section('body')
    <div id="task"
         data-project_id="{{ $task->project_id }}"
         data-task_id="{{ $task->id }}"
         data-edit_task_url="{{ action('Task\TaskController@ajaxEditTaskPost') }}">
        <div class="banner banner_strip" style="background-color: {{ $task->project->getColor() }}" uk-sticky>
            <div class="uk-flex">
                <div class="uk-width-auto uk-flex uk-flex-middle">
                    {!! TaskPrinter::getProjectBadge($task) !!}
                </div>
                <div class="uk-width-expand uk-flex uk-flex-middle">
                    <h2 class="task_name" contenteditable>{{ $task->name }}</h2>
                </div>
                <div class="uk-width-auto uk-flex uk-flex-middle">
                    <a class="task_info"><i class="fa fa-info-circle"></i></a>
                </div>
            </div>
        </div>

        <div class="task_content">
            {{-- Task Description --}}
            <div class="section">
                <h2>Description</h2>
                <div class="description">
                    @if (! empty($task->description))
                        {!! $task->description !!}
                    @else
                        <span class="placeholder">Enter a description</span>
                    @endif
                </div>
                <div class="description_options uk-margin-small-top uk-hidden">
                    <a class="save save_description">Save</a>
                    <a class="cancel cancel_description">Cancel</a>
                </div>
            </div>

            {{--Task Checklists--}}
            <div class="section">
                <h2>Sub Tasks</h2>
                <div class="task_checklists_wrapper"
                     data-make_task_checklist_url="{{ action('Task\TaskChecklistController@ajaxMakeTaskChecklistPost') }}"
                     data-view_task_checklists_url="{{ action('Task\TaskChecklistController@ajaxViewTaskChecklistsGet') }}"
                     data-edit_task_checklist_url="{{ action('Task\TaskChecklistController@ajaxEditTaskChecklistPost') }}"
                     data-delete_task_checklist_url="{{ action('Task\TaskChecklistController@ajaxDeleteTaskChecklistPost') }}"
                     data-edit_task_checklist_order_url="{{ action('Task\TaskChecklistController@ajaxEditTaskChecklistOrderPost') }}"
                     data-make_task_checklist_item_url="{{ action('Task\TaskChecklistItemController@ajaxMakeTaskChecklistItemPost') }}"
                     data-view_task_checklist_items_url="{{ action('Task\TaskChecklistItemController@ajaxViewTaskChecklistItemsGet') }}"
                     data-edit_task_checklist_item_url="{{ action('Task\TaskChecklistItemController@ajaxEditTaskChecklistItemPost') }}"
                     data-edit_task_checklist_item_order_url="{{ action('Task\TaskChecklistItemController@ajaxEditTaskChecklistItemOrderPost') }}"
                     data-delete_task_checklist_item_url="{{ action('Task\TaskChecklistItemController@ajaxDeleteTaskChecklistItemPost') }}"
                ></div>
                <div class="new_task_checklist uk-flex">
                    <div class="uk-width-expand">
                        <input class="new_task_checklist_input" placeholder="New Checklist..." />
                    </div>
                    <div class="uk-width-auto task_checklist_save_wrapper uk-hidden">
                        <a class="uk-button save_new_task_checklist uk-button-primary disabled"><i class="fa fa-check"></i></a>
                    </div>
                </div>
            </div>

            {{-- Task Comments --}}
            <div class="section">
                <h2>Comments</h2>
                <div class="task_comments"
                     data-page="1"
                     data-view_task_comments_url="{{ action('Task\TaskCommentController@ajaxViewTaskCommentsGet') }}"
                     data-delete_task_comment_url="{{ action('Task\TaskCommentController@ajaxDeleteTaskCommentPost') }}">
                </div>
            </div>

            {{-- Task History --}}
            <div class="section">
                <h2>History</h2>
                <div class="task_logs"
                     data-page="1"
                     data-view_task_logs_url="{{ action('Task\TaskLogController@ajaxViewTaskLogsGet') }}">
                </div>
            </div>
        </div>

        {{-- Task  Sidebar  (Information about the task) --}}
        <div class="task_sidebar">
            <div class="uk-grid uk-grid-small" uk-grid>
                {{-- Task Reporter --}}
                <div class="uk-width-1-4">
                    <label class="task_label">Reporter</label>
                </div>
                <div class="uk-width-3-4">
                    @if ($task->task_reporter_user !== null)
                        {!! UserPrinter::userBadge($task->task_reporter_user, true) !!}
                    @endif
                </div>
                {{-- Task Issue Type --}}
                <div class="uk-width-1-4">
                    <label class="task_label">Issue Type</label>
                </div>
                <div class="uk-width-3-4">
                    @if ($task->task_issue_type !== null)
                        {{ $task->task_issue_type->name }}
                    @endif
                </div>
                {{-- Task Priority --}}
                <div class="uk-width-1-4">
                    <label class="task_label">Priority</label>
                </div>
                <div class="uk-width-3-4">
                    @if ($task->task_priority !== null)
                        {{ $task->task_priority->name }}
                    @endif
                </div>
                {{-- Task Assigned To --}}
                <div class="uk-width-1-4">
                    <label class="task_label">Assignee</label>
                </div>
                <div class="uk-width-3-4">
                    @if ($task->task_assigned_user !== null)
                        {!! UserPrinter::userBadge($task->task_assigned_user, true) !!}
                    @else
                        <span class="badge user">?</span><span class="badge placeholder">Assignee</span>
                    @endif
                </div>
                {{-- Task Watchers --}}
                <div class="uk-width-1-4">
                    <label class="task_label">Watchers</label>
                </div>
                <div class="uk-width-3-4">
                    @foreach ($task->task_watcher_users as $task_watcher_user)
                        <div style="margin-bottom: 10px;">
                            {!! UserPrinter::userBadge($task_watcher_user->user) !!}
                            {{ $task_watcher_user->user->getFullName() }}
                        </div>
                    @endforeach
                </div>
                {{-- Task Due Date --}}
                <div class="uk-width-1-4">
                    <label class="task_label">Due Date</label>
                </div>
                <div class="uk-width-3-4">
                    {{ $task->due_date !== null ? $task->due_date->format('D d M Y') : 'Add Due Date' }}
                </div>
                {{-- Timelogging --}}
                @if (! empty($task->task_timelogs))
                    <div class="uk-width-1-4">
                        <label class="task_label">Timelogs</label>
                    </div>
                    <div class="uk-width-3-4">
                        @foreach ($task->task_timelogs as $timelog)
                            <div class="task_timelog_row uk-flex">
                                <div class="uk-width-auto task_timelog_user">
                                    {!! UserPrinter::userBadge($timelog->user) !!}
                                </div>
                                <div class="uk-width-auto task_timelog_time_spent">
                                    {{ TimelogRepository::convertTimelogTimeSpent($timelog->time_spent) }}
                                </div>
                                <div class="uk-width-expand uk-flex uk-flex-middle task_timelog_time_spent_percent">
                                    <div class="progress">
                                        <div class="progress_percent" style="width: {{ (($timelog->time_spent / $task->total_time_logged) * 100) }}%"></div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection