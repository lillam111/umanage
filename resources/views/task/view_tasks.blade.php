@extends('template.master')
@section('styles')
    <link href="{{ asset('views/task/view_task_list.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('scripts')
    <script src="{{ asset('views/task/view_tasks.js') }}"></script>
@endsection
@section('body')
    <div class="uk-container">
        <h2>My Tasks</h2>
        <div class="tasks"
             data-get_tasks_url="{{ action('Task\TaskController@ajaxViewTasksGet') }}">
        </div>
    </div>
@endsection