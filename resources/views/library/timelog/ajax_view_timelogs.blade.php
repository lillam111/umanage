<div class="timelog_box_wrapper">
    @foreach ($timelogs as $timelog)
        <div class="timelog_entry">
            <div class="timelog_title">
                <b>{{ TimelogRepository::shortContent($timelog->task->name) }}</b>
            </div>
            <div class="timelog_content">
                <p>{{ TimelogRepository::shortContent($timelog->note) }}</p>
            </div>
            <div class="timelog_task_project">
                <span class="badge" style="background-color: {{ $timelog->task->project->getColor() }}">{{ $timelog->task->project->code }}-{{ $timelog->task->id }}</span>
            </div>
            <div class="timelog_time_spent">
                <b>{{ TimelogRepository::convertTimelogTimeSpent($timelog->time_spent) }}</b>
            </div>
        </div>
    @endforeach
</div>