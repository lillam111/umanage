@if(! empty($title))
    <h2>{{ $title }}</h2>
@endif
<div class="uk-grid uk-grid-small user_project_grid" uk-grid uk-height-match="target: .user_project">
    @foreach($projects as $project)
        <div class="uk-width-1-4@xl uk-width-1-3@m uk-width-1-2@s user_project_wrapper">
            <div class="user_project" style="background-color: {{ $project->getColor() }}">
                <a href="{{ action('Project\ProjectController@viewProjectGet', $project->id) }}">
                    <div class="user_project_title uk-flex">
                        <div class="uk-width-auto uk-flex uk-flex-middle project_image">
                            <div class="uk-flex uk-flex-middle uk-flex-center">
                                <i class="{{ $project->icon }}"></i>
                            </div>
                        </div>
                        <div class="uk-width-expand">
                            <h2>{{ $project->name }}</h2><span>Software Project</span>
                        </div>
                        <div class="uk-widt-auto uk-flex uk-flex-middle">
                            <span class="project_progress">{{ $project->getTaskNumberedProgress() }}</span>
                        </div>
                    </div>
                    <div class="user_project_content">
                        <div class="progress"><div class="progress_percent" style="width: {{ $project->getTaskPercentageProgress() }};"></div></div>
                    </div>
                </a>
            </div>
        </div>
    @endforeach
    <div class="uk-width-1-4@xl uk-width-1-3@m uk-width-1-2@s user_project_wrapper add_new_project">
        <div class="user_project">
            <a>
                <div class="user_project_title uk-flex">
                    <div class="uk-width-auto uk-flex uk-flex-middle">
                        <div class="user_project_image uk-flex uk-flex-middle uk-flex-center">
                            <i class="fa fa-plus"></i>
                        </div>
                    </div>
                    <div class="uk-width-expand">
                        <h2>New Project</h2>
                        <span>Software Project</span>
                    </div>
                </div>
            </a>
        </div>
    </div>
</div>