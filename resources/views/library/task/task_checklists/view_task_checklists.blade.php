{{-- This particular file is entirely ajaxed, this file will be loaded after the view_task.blade has been loaded in
prior to this trying to be called in, if the request is made successfully to the task checklists then we are going to
be rendering this particular checklist sets of elements... --}}
<div class="task_checklists" uk-sortable="handle: .task_checklist_options_dropdown_wrapper">
    @foreach ($task_checklists as $task_checklist)
        <div class="task_checklist uk-width-1-1 box_wrapper" data-task_checklist_id="{{ $task_checklist->id }}">
            <div class="task_checklist_name_wrapper uk-flex">
                <div class="task_checklist_name uk-width-expand" contenteditable>{!! $task_checklist->name !!}</div>
                <div class="uk-width-auto task_checklist_options_wrapper uk-hidden">
                    <a class="fa fa-check uk-button uk-button-primary disabled save_task_checklist_name"></a>
                    <a class="fa fa-close uk-button uk-button-danger cancel_task_checklist_name"></a>
                </div>
                <div class="uk-width-auto task_checklist_options_dropdown_wrapper">
                    <a class="fa fa-ellipsis-h uk-button dropdown_button"></a>
                    <div class="dropdown">
                        <ul>
                            {{-- Deleting Task Checklists, JavaScript is attached to this particular
                            Element, if you are changing the name of this class, this will need to be
                            Changed within the javascript as well... --}}
                            <li><a class="delete_task_checklist"><i class="fa fa-trash"></i> Delete</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="checklist_group_progress">
                <div class="progress">
                    <div class="progress_percent" style="width: {{ $task_checklist->getTaskChecklistItemPercentProgress() }}"></div>
                </div>
            </div>
            <div class="task_checklist_items" uk-sortable="handle: .task_checklist_item_options_dropdown_wrapper; group: task_checklist_items">
                @foreach ($task_checklist->task_checklist_items as $task_checklist_item)
                    <div class="task_checklist_item {{ $task_checklist_item->is_checked ? 'complete' : '' }}" data-task_checklist_item_id="{{ $task_checklist_item->id }}">
                        <div class="task_checklist_item_name_wrapper uk-flex">
                            <div class="uk-width-auto uk-flex uk-flex-middle task_checklist_item_checkbox_wrapper">
                                <div>
                                    <input type="checkbox"
                                           class="uk-checkbox task_checklist_item_checkbox"
                                           {{ $task_checklist_item->is_checked ? 'checked' : '' }}
                                    />
                                </div>
                            </div>
                            <div class="task_checklist_item_name uk-width-expand" contenteditable>{!! $task_checklist_item->name !!}</div>
                            <div class="uk-width-auto task_checklist_item_options_wrapper uk-hidden">
                                <a class="fa fa-check uk-button uk-button-primary disabled save_task_checklist_item_name"></a>
                                <a class="fa fa-close uk-button uk-button-danger cancel_task_checklist_item_name"></a>
                            </div>
                            <div class="uk-width-auto task_checklist_item_options_dropdown_wrapper">
                                <a class="fa fa-ellipsis-h uk-button dropdown_button"></a>
                                <div class="dropdown">
                                    <ul>
                                        {{-- Deleting Task Checklist Items, JavaScript is attached to this particular
                                        Element, if you are changing the name of this class, this will need to be
                                        Changed within the javascript as well... --}}
                                        <li><a class="delete_task_checklist_item"><i class="fa fa-trash"></i> Delete</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="new_task_checklist_item uk-flex">
                <div class="uk-width-expand">
                    <input class="new_task_checklist_item_input" placeholder="New Checklist Item..." />
                </div>
                <div class="uk-width-auto task_checklist_item_save_wrapper uk-hidden">
                    <a class="uk-button save_new_task_checklist_item uk-button-primary disabled"><i class="fa fa-check"></i></a>
                </div>
            </div>
        </div>
    @endforeach
</div>