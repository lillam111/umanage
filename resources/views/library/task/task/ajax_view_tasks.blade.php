{{-- This particular file is utilised for universally displaying tasks wherever they are required when utilising ajax,
this is the file that will be returned every time we are requesting to get some tasks from the system and can return
them in this particular format, all tasks will be standardised to the same visuals no matter where we are looking at
them. --}}
{{-- If we have opted to having a title iwth the tasks, then t his is where the title of the tasks is going
to be placed... --}}
@if(! empty($title))
    <h2 class="box_title">{{ $title }}</h2>
@endif
{{-- Iterate over all the tasks... --}}
@foreach ($tasks as $task)
    <a href="{{ action('Task\TaskController@viewTaskGet', [$task->project->code, $task->id]) }}">
        <div class="task_row uk-flex">
            <div class="uk-width-expand">
{{--                <p>{!! \App\Printers\User\UserPrinter::userBadge($task->task_assigned_user, true) !!}</p>--}}
                <span class="task_name">
                    {!! TaskPrinter::getProjectBadge($task) !!} {{ $task->name }}
                </span>
            </div>
        </div>
    </a>
@endforeach
{{-- If there aren't any results then we are going to be letting the user know that there are no results for
the tasks/project/filteringg that they're trying to view... --}}
@if ($tasks->isEmpty())
    <p>Looks like there aren't any results...</p>
@endif
{{-- If the pagination has been invoked, and that the total pages are greater than 1, then we can begin to
looking to adding the pagination (previous and next) buttons for the task system --}}
<div class="pagination uk-flex">
    <div class="uk-width-expand">
        Total Tasks: {{ $total_results }}
    </div>
    <div class="uk-width-auto">
        @if($page > 1)
            <a class="paginate_previous">Previous</a>
        @endif
    </div>
    <div class="uk-width-auto">
        Page: {{ $page }}{{ (int) $total_pages !== 0 ? '/ ' . $total_pages : '' }}
    </div>
    <div class="uk-width-auto uk-text-right">
        @if ($page < $total_pages)
            <a class="paginate_next">Next</a>
        @endif
    </div>
</div>