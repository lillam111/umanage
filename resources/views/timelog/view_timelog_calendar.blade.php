@extends('template.master')
@section('styles')
    <link href="{{ asset('assets/vendor/datepicker/datepicker.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('views/timelog/view_timelog_calendar.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('scripts')
    <script src="{{ asset('assets/vendor/datepicker/datepicker.js') }}"></script>
    <script src="{{ asset('views/timelog/view_timelog_calendar.js') }}"></script>
@endsection
@section('body')
    <div class="banner banner_strip" uk-sticky>
        <div class="uk-flex">
            <div class="uk-width-expand uk-flex uk-flex-middle">
                <h2>{{ Auth::user()->getFullName() }}'s Timelogs</h2>
            </div>
        </div>
    </div>

    <div class="uk-container timelog_calendar"
         data-view_timelogs_url="{{ action('Timelog\TimelogController@ajaxViewTimelogsCalendarGet') }}"
         data-current_date="{{ $days->monday->format('d.m.Y') }}">
        <div class="box_wrapper timelog_date_picker_wrapper">
            <div class="uk-flex">
                <div class="uk-width-auto">
                    <a class="timelog_calendar_left fa fa-arrow-left"></a>
                </div>
                <div class="date uk-width-expand uk-flex uk-flex-middle uk-flex-center">
                    <span>{{ $days->monday->format('d.m.Y') }} - {{ $days->sunday->format('d.m.Y') }}</span>
                </div>
                <div class="uk-width-auto">
                    <a class="timelog_calendar_right fa fa-arrow-right"></a>
                </div>
            </div>
        </div>
        <div class="timelogs"></div>
    </div>

    <div id="add_timelog_modal" uk-modal
        data-make_timelog_url="{{ action('Timelog\TimelogController@ajaxMakeTimelogPost') }}"
        data-search_tasks_url="{{ action('Task\TaskController@ajaxSearchTasksGet') }}">
        <div class="uk-modal-dialog">
            <button class="uk-modal-close-default" type="button" uk-close></button>
            <div class="uk-modal-header">
                <h2 class="uk-modal-title">Add Timelog</h2>
            </div>
            <div class="uk-modal-body" uk-overflow-auto>
                <form class="uk-form uk-grid uk-grid-small" uk-grid>
                    <div class="uk-width-1-1 task_search_input_wrapper">
                        <input type="text" id="task_id" placeholder="Search issue...">
                        <i class="fa fa-spin fa-spinner"></i>
                    </div>
                    <div class="uk-width-1-1">
                        <textarea placeholder="Note..." id="timelog_note"></textarea>
                    </div>
                    <div class="uk-width-1-3">
                        <input type="text" id="time_spent" placeholder="Time spent...">
                    </div>
                    <div class="uk-width-1-3">
                        <input type="text" id="from" placeholder="From...">
                    </div>
                    <div class="uk-width-1-3">
                        <input type="text" id="to" placeholder="To...">
                    </div>
                </form>
            </div>
            <div class="uk-modal-footer uk-text-right">
                <button class="uk-button uk-button-default uk-modal-close" type="button">Cancel</button>
                <button class="uk-button uk-button-primary make_timelog" type="button">Save</button>
            </div>
        </div>
    </div>
@endsection