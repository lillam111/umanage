@extends('template.master')
@section('styles')
    <link href="{{ asset('assets/vendor/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('views/task/view_task_list.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('views/project/view_project.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('scripts')
    <script src="{{ asset('assets/vendor/select2/select2.min.js') }}"></script>
    <script src="{{ asset('views/project/view_project.js') }}"></script>
    <script src="{{ asset('views/task/view_tasks.js') }}"></script>
@endsection
@section('body')
    <div class="banner banner_strip" style="background-color: {{ $project->getColor() }}">
        <div class="uk-grid">
            <div class="uk-width-expand uk-flex uk-flex-middle">
                <div>
                    <h2>{{ $project->name }}</h2>
                </div>
            </div>
            <div class="uk-width-auto uk-flex uk-flex-middle">
                <a class="project_settings_button" href="{{ action('Project\ProjectSettingController@viewProjectSettingsGet', $project->id) }}"><i class="fa fa-cogs"></i></a>
                <a class="project_advanced_search"><i class="fa fa-search"></i></a>
            </div>
        </div>
    </div>
    {{-- The  progress of the project, in the visualisation of a percentage bar. --}}
    <div class="user_project_content">
        <div class="progress"><div class="progress_percent" style="width: {{ $project->getTaskPercentageProgress() }}"></div></div>
    </div>

    <div class="project_content">
        <div class="tasks"
             data-page="1"
             data-pagination="true"
             data-items_per_page="50"
             data-get_tasks_url="{{ action('Task\TaskController@ajaxViewTasksGet') }}"
             data-project_id="{{ $project->id }}">
        </div>
    </div>

    <div class="project_sidebar">
        {{-- Statuses --}}
        <h2>Filters <a class="close_project_sidebar"><i class="fa fa-close"></i></a></h2>
        <input type="text" id="task_search" placeholder="Search...">
        <div class="uk-grid uk-grid-small">
            <div class="uk-width-1-1">
                <h3>Task Statuses</h3>
                <input type="hidden" id="task_statuses" value="{{ $filters->task_statuses }}">
                {!! TaskStatusRepository::getTaskStatusCheckboxFilters(
                    explode(',', $filters->task_statuses)
                ) !!}
            </div>
            {{-- Issue Types --}}
            <div class="uk-width-1-1">
                <h3>Task Issue Types</h3>
                <input type="hidden" id="task_issue_types" value="{{ $filters->task_issue_types }}">
                {!! TaskIssueTypeRepository::getTaskIssueTypeCheckboxFilters(
                    explode(',', $filters->task_issue_types)
                ) !!}
            </div>
            {{-- Task Priorities --}}
            <div class="uk-width-1-1">
                <h3>Task Priorities</h3>
                <input type="hidden" id="task_priorities" value="{{ $filters->task_priorities }}">
                {!! TaskPriorityRepository::getTaskPriorityCheckboxFilters(
                    explode(',', $filters->task_priorities)
                ) !!}
            </div>
        </div>
        <a class="clear_filters uk-button uk-button-small uk-button-primary">Clear</a>
    </div>
@endsection