@extends('template.master')
@section('styles')
    <link href="{{ asset('views/user/view_user.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('views/task/view_task_list.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('views/project/view_projects.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('views/timelog/view_timelogs.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('scripts')
    <script src="{{ asset('views/task/view_tasks.js') }}"></script>
    <script src="{{ asset('views/project/view_projects.js') }}"></script>
    <script src="{{ asset('views/timelog/view_timelogs.js') }}"></script>
@endsection
@section('body')
    <div class="banner banner_strip">
        <h2>Hello {{ Auth::user()->getFullName() }}</h2>
    </div>
    <div class="section">
        <div class="uk-grid uk-grid-medium" uk-grid>
            <div class="uk-width-2-3">
                <h2 class="section_title">My Projects</h2>
                <div class="projects"
                     data-view_projects_url="{{ action('Project\ProjectController@ajaxViewProjectsGet') }}">
                </div>
            </div>
            <div class="uk-width-1-3">
                <h2 class="section_title">My Tasks</h2>
                <div class="tasks"
                     data-pagination="true"
                     data-items_per_page="10"
                     data-page="1"
                     data-get_tasks_url="{{ action('Task\TaskController@ajaxViewTasksGet') }}">
                </div>
            </div>
        </div>
    </div>
    <div class="section gray">
        <div class="uk-grid uk-grid-medium" uk-grid>
            <div class="uk-width-1-2">
                <h2 class="section_title">Recent Activity</h2>
                <div class="timelogs_box" data-get_timelogs_url="{{ action('Timelog\TimelogController@ajaxViewTimelogsGet') }}"></div>
            </div>
            <div class="uk-width-1-2">
                <h2 class="section_title">Recent History</h2>
            </div>
        </div>
    </div>
@endsection