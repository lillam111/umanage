@extends('template.master')
@section('styles')
    <link href="{{ asset('views/user/view_users.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('body')
    <div class="uk-container">
        <h2 class="title_small">Users</h2>
        <div class="box_wrapper">
            <table class="uk-table uk-table-striped uk-table-small">
                <thead>
                    <tr>
                        <th style="width: 50px;">id</th>
                        <th>User</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($users as $user)
                        <tr>
                            <td>{{ $user->id }}</td>
                            <td>{!! UserPrinter::userBadge($user) !!} {!! UserPrinter::linkedUser($user) !!}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection