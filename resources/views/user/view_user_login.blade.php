@extends('template.master')
@section('body')
    <div class="login_wrapper">
        <div class="uk-grid uk-grid-collapse" uk-grid>
            <div class="uk-width-1-3 login_title uk-flex uk-flex-middle uk-flex-center">
                <div>
                    <h2>{{ env('APP_NAME') }}</h2>
                    <a class="social_icon"><i class="fa fa-facebook"></i></a>
                    <a class="social_icon"><i class="fa fa-twitter"></i></a>
                    <a class="social_icon"><i class="fa fa-youtube"></i></a>
                </div>
            </div>
            <div class="uk-width-2-3 login_form">
                <form class="" method="POST" action="{{ action('User\UserController@viewUserLoginPost') }}">
                <p>Login to your account</p>
                <input type="email" name="email" class="uk-width-1-1" placeholder="Enter email" autocomplete="off" />
                <input type="password" name="password" class="uk-width-1-1" placeholder="Enter password" autocomplete="off" />
                <button class="uk-button uk-button-primary">Login</button>
                <p><a href="">Forgot password</a><a href="">Register an account</a></p>
                @csrf
            </div>
        </div>
    </div>
@endsection