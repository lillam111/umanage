@extends('template.master')
@section('styles')
    <link href="{{ asset('views/user/view_user.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('views/task/view_task_list.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('views/project/view_projects.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('views/task/view_tasks.js') }}" rel="stylesheet" type="text/css" />
@endsection
@section('scripts')
    <script src="{{ asset('views/user/view_user.js') }}"></script>
    <script src="{{ asset('views/project/view_projects.js') }}"></script>
    <script src="{{ asset('views/task/view_tasks.js') }}"></script>
@endsection
@section('body')
    {{-- The User's Banner Strip --}}
    <div class="banner banner_strip">
        <h2>{{ $user->getFullName() }}</h2>
    </div>
    <div class="section">
        {{-- User Profile Image --}}
        <div class="user_profile_wrapper">
            <div class="uk-grid uk-grid-small" uk-grid>
                <div class="uk-width-1-3@m">
                    {{-- User Profile Name, and Profile Details --}}
                    <h2 class="section_title">About</h2>
                    <div class="uk-flex form_element_row">
                        <div class="uk-width-auto uk-flex uk-flex-middle">
                            <div><i class="fa fa-question"></i></div>
                        </div>
                        <div class="uk-width-expand">
                            <div>
                                @can('UserGate@editUser', $user)
                                    <input type="text" placeholder="Your First Name" value="{{ $user->first_name }}" />
                                @else
                                    <div class="placeholder">{{ $user->first_name }}</div>
                                @endcan
                            </div>
                        </div>
                    </div>
                    <div class="uk-flex form_element_row">
                        <div class="uk-width-auto uk-flex uk-flex-middle">
                            <div><i class="fa fa-question"></i></div>
                        </div>
                        <div class="uk-width-expand">
                            <div>
                                @can('UserGate@editUser', $user)
                                    <input type="text" placeholder="Your Last Name" value="{{ $user->last_name }}" />
                                @else
                                    <div class="placeholder">{{ $user->last_name }}</div>
                                @endcan
                            </div>
                        </div>
                    </div>
                    <div class="uk-flex form_element_row">
                        <div class="uk-width-auto uk-flex uk-flex-middle">
                            <div><i class="fa fa-briefcase"></i></div>
                        </div>
                        <div class="uk-width-expand">
                            <div>
                                @can('UserGate@editUser', $user)
                                    <input type="text" placeholder="Your Job Title" />
                                @else
                                    <div class="placeholder">Your Job Title</div>
                                @endcan
                            </div>
                        </div>
                    </div>
                    <div class="uk-flex form_element_row">
                        <div class="uk-width-auto uk-flex uk-flex-middle">
                            <div><i class="fa fa-briefcase"></i></div>
                        </div>
                        <div class="uk-width-expand">
                            @can('UserGate@editUser', $user)
                                <input type="text" placeholder="Your Department" />
                            @else
                                <div class="placeholder">Your Department</div>
                            @endcan
                        </div>
                    </div>
                    <div class="uk-flex form_element_row">
                        <div class="uk-width-auto uk-flex uk-flex-middle">
                            <div><i class="fa fa-address-book"></i></div>
                        </div>
                        <div class="uk-width-expand">
                            @can('UserGate@editUser', $user)
                                <input type="text" placeholder="Your Address" />
                            @else
                                <div class="placeholder">Your Address</div>
                            @endcan
                        </div>
                    </div>
                    <div class="uk-flex form_element_row">
                        <div class="uk-width-auto uk-flex uk-flex-middle">
                            <div><i class="fa fa-clock-o"></i></div>
                        </div>
                        <div class="uk-width-expand">
                            @can('UserGate@editUser', $user)
                                <input type="text" placeholder="Your Timezone" />
                            @else
                                <div class="placeholder">Your Timezone</div>
                            @endcan
                        </div>
                    </div>
                </div>
                <div class="uk-width-1-3@m">
                    <h2 class="section_title">Contact</h2>
                    <div class="uk-flex form_element_row">
                        <div class="uk-width-auto uk-flex uk-flex-middle">
                            <div><i class="fa fa-envelope"></i></div>
                        </div>
                        <div class="uk-width-expand">
                            @can('UserGate@editUser', $user)
                                <input type="text" placeholder="Your Email" value="{{ $user->email }}" />
                            @else
                                <div class="placeholder">{{ $user->email }}</div>
                            @endcan
                        </div>
                    </div>
                </div>
                @if($user->user_role !== null)
                    <div class="uk-width-1-3@m">
                        <h2 class="section_title">Roles</h2>
                        <div class="uk-flex form_element_row">
                            <div class="uk-width-auto uk-flex uk-flex-middle">
                                <div><i class="fa fa-briefcase"></i></div>
                            </div>
                            <div class="uk-width-expand">
                                <div class="placeholder">{{ $user->user_role->role->name }}</div>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
    <div class="section gray">
        <h2>{{ $user->getFullName() }}'s Projects</h2>
        <div class="projects"
             data-view_projects_url="{{ action('Project\ProjectController@ajaxViewProjectsGet') }}">
        </div>
    </div>
    <div class="section">
        <h2>{{ $user->first_name  }} works with</h2>
        <div class="uk-grid uk-grid-small" uk-grid>
            @foreach ($users_i_work_with as $user_i_work_with)
                <div class="uk-width-1-5@xl uk-width-1-4@m uk-width-1-2">
                    <div class="user_card">
                        <a href="{{ action('User\UserController@viewUserGet', [$user_i_work_with->id]) }}">
                            <div class="fake_image uk-flex uk-flex-middle uk-flex-center bg_blue">
                                <i class="fa fa-user"></i>
                            </div>
                            <h2>{{ $user_i_work_with->getFullName() }}</h2>
                            <span>Job Description</span>
                        </a>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection