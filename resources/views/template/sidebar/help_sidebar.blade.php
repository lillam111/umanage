<div id="help_sidebar" class="sidebar uk-flex">
    <div class="uk-width-auto">
        <a class="sidebar_close"><i class="fa fa-close"></i></a>
    </div>
    <div class="uk-width-expand">
        <h2>Help</h2>
        <div class="help_row uk-flex">
            <div class="uk-width-expand">Favourite Sidebar</div>
            <div class="uk-width-auto uk-flex uk-flex-middle"><span class="help_badge">ctrl+f</span></div>
        </div>
        <div class="help_row uk-flex">
            <div class="uk-width-expand">Search Sidebar</div>
            <div class="uk-width-auto uk-flex uk-flex-middle"><span class="help_badge">ctrl+s</span></div>
        </div>
        <div class="help_row uk-flex">
            <div class="uk-width-expand">Create Sidebar</div>
            <div class="uk-width-auto uk-flex uk-flex-middle"><span class="help_badge">ctrl+c</span></div>
        </div>
        <div class="help_row uk-flex">
            <div class="uk-width-expand">Help Sidebar</div>
            <div class="uk-width-auto uk-flex uk-flex-middle"><span class="help_badge">ctrl+h</span></div>
        </div>
        <div class="help_row uk-flex">
            <div class="uk-width-expand">Notification Sidebar</div>
            <div class="uk-width-auto uk-flex uk-flex-middle"><span class="help_badge">ctrl+n</span></div>
        </div>
        <div class="help_row uk-flex">
            <div class="uk-width-expand">Account Sidebar</div>
            <div class="uk-width-auto uk-flex uk-flex-middle"><span class="help_badge">ctrl+a</span></div>
        </div>
    </div>
</div>