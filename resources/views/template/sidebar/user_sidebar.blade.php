<div id="user_sidebar" class="sidebar uk-flex">
    <div class="uk-width-auto">
        <a class="sidebar_close"><i class="fa fa-close"></i></a>
    </div>
    <div class="uk-width-expand uk-flex-middle">
        <h2>{{ Auth::user()->getFullName() }}</h2>
        <hr />
        <ul>
            <li><a href="{{ action('User\UserController@viewUserGet', Auth::id()) }}"><i class="fa fa-user"></i> My Account</a></li>
            <li><a href=""><i class="fa fa-edit"></i> Edit Account</a></li>
            <li><a href=""><i class="fa fa-cog"></i> Account Settings</a></li>
            <li><a href="{{ action('Task\TaskController@viewTasksGet') }}"><i class="fa fa-tasks"></i> My Tasks</a></li>
            <li><a href="{{ action('User\UserController@userLogout') }}"><i class="fa fa-sign-out"></i> Log out</a></li>
        </ul>
        <hr />
        <h3>Sanity</h3>
        <ul>
            <li><a href="{{ action('Journal\JournalController@viewJournalsGet') }}"><i class="fa fa-sticky-note"></i> Journals</a></li>
            <li><a href="{{ action('Journal\JournalController@viewJournalsReportGet') }}"><i class="fa fa-line-chart"></i> Report</a></li>
        </ul>
        <hr />
        <h3>Time tracking</h3>
        <ul>
            <li><a href="{{ action('Timelog\TimelogController@viewTimelogCalendarGet') }}"><i class="fa fa-calendar"></i> Timelogging</a></li>
        </ul>
        <hr />
        <h3>My Projects</h3>
        <ul>
            @foreach ($view_service->projects as $project)
                <li><a href="{{ action('Project\ProjectController@viewProjectGet', $project->id) }}">{{ $project->name }}</a></li>
            @endforeach
        </ul>
    </div>
</div>