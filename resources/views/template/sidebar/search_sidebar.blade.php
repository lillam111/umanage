<div id="search_sidebar" class="sidebar uk-flex">
    <div class="uk-width-auto">
        <a class="sidebar_close"><i class="fa fa-close"></i></a>
    </div>
    <div class="uk-width-expand">
        <input type="text" class="search_sidebar_input" placeholder="Search..." />
    </div>
</div>