<div id="add_sidebar" class="sidebar uk-flex"
    data-make_project_url="{{ action('Project\ProjectController@ajaxMakeProjectPost') }}"
    data-make_task_url="{{ action('Task\TaskController@ajaxMakeTaskPost') }}">
    <div class="uk-width-auto">
        <a class="sidebar_close"><i class="fa fa-close"></i></a>
    </div>
    <div class="uk-width-expand">
        <div class="add_sidebar_options">
            <div class="sidebar_add_tab_options">
                <a class="sidebar_add_new_project active"><i class="fa fa-plus"></i> Add New Project</a>
                <a class="sidebar_add_new_task"><i class="fa fa-plus"></i> Add New Task</a>
            </div>
        </div>
        {{-- Adding a new project... --}}
        <div class="add_new_project active">
            <div class="uk-grid uk-grid-small">
                {{-- Project New Title --}}
                <div class="uk-width-1-4">
                    <label>Name</label>
                </div>
                <div class="uk-width-3-4">
                    <input type="text" placeholder="Project Name..." class="project_name" />
                </div>
                {{-- Project Code --}}
                <div class="uk-width-1-4">
                    <label>Code</label>
                </div>
                <div class="uk-width-3-4">
                    <input type="text" placeholder="Project Code..." class="project_code" />
                </div>
                {{-- Project Description --}}
                <div class="uk-width-1-4">
                    <label>Description</label>
                </div>
                <div class="uk-width-3-4">
                    <textarea placeholder="Project Description..." class="project_description"></textarea>
                </div>
                {{-- Project Icon --}}
                <div class="uk-width-1-4">
                    <label>Icon</label>
                </div>
                <div class="uk-width-3-4">
                    <input type="text" placeholder="Project Icon..." class="project_icon" />
                </div>
                {{-- Project Color --}}
                <div class="uk-width-1-4">
                    <label>Color</label>
                </div>
                <div class="uk-width-3-4">
                    <input type="text" placeholder="Project Colour..." class="project_color" />
                </div>
                {{-- Project Save Button --}}
                <div class="uk-width-1-1 uk-text-right">
                    <a class="save_new_project uk-button">Save</a>
                </div>
            </div>
        </div>
        {{-- Adding a new task... --}}
        <div class="add_new_task">
            <div class="uk-grid uk-grid-small">
                {{-- Task title --}}
                <div class="uk-width-1-4">
                    <label>Name</label>
                </div>
                <div class="uk-width-3-4">
                    <input type="text" placeholder="Task Title..." class="task_name" />
                </div>
                {{-- Task Description --}}
                <div class="uk-width-1-4">
                    <label>Description</label>
                </div>
                <div class="uk-width-3-4">
                    <textarea placeholder="Task Description..." class="task_description"></textarea>
                </div>
                {{-- Task Project --}}
                <div class="uk-width-1-4">
                    <label>Project</label>
                </div>
                <div class="uk-width-3-4">
                    <select class="task_project">
                        @foreach ($view_service->projects as $project)
                            <option value="{{ $project->id }}">{{ $project->name }}</option>
                        @endforeach
                    </select>
                </div>
                {{-- Task Save Button --}}
                <div class="uk-width-1-1 uk-text-right">
                    <a class="save_new_task uk-button">Save</a>
                </div>
            </div>
        </div>
    </div>
</div>