@php($view_service = App::make('view_service')->all()['view_service'])
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <title>{{ $view_service->title }}</title>
        {{-- Style Yielding --}}
        @yield('styles')
        <link href="{{ asset('assets/vendor/uikit/uikit.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/vendor/fontawesome/fontawesome.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/css/app.css') }}" rel="stylesheet" type="text/css" />
    </head>
    <body class="{{ $view_service->current_page }}">
        {{-- Header --}}
        @if($view_service->header === true)
            @include('template.header')
        @endif
        {{-- Body --}}
        @yield('body')
        {{-- Ajax Message Helper --}}
        <div class="ajax_message_helper"></div>
        {{-- Footer --}}
        @if($view_service->footer === true)
            @include('template.footer')
        @endif
        {{-- Script Yielding --}}
        <script src="{{ asset('assets/vendor/uikit/jquery.js') }}"></script>
        <script src="{{ asset('assets/vendor/uikit/uikit.min.js') }}"></script>
        @yield('scripts')
        <script src="{{ asset('assets/js/app.js') }}"></script>
    </body>
</html>