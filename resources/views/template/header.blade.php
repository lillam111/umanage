<header>
    {{-- Top Half Navigation --}}
    <div class="top">
        <a href="/" class="logo {{ $view_service->ref === 'dashboard' ? 'active' : '' }}">
            <span class="link_badge">{{ env('APP_NAME_SHORT') }}</span>
            <span class="link_text">Umanage</span>
        </a>
        <a href="" class="icon" id="favourites_sidebar_button">
            <span class="link_badge"><i class="fa fa-star"></i></span>
            <span class="link_text">Favourites</span>
        </a>
        <a href="" class="icon" id="search_sidebar_button">
            <span class="link_badge"><i class="fa fa-search"></i></span>
            <span class="link_text">Search</span>
        </a>
        <a href="" class="icon" id="add_sidebar_button">
            <span class="link_badge"><i class="fa fa-plus"></i></span>
            <span class="link_text">Create</span>
        </a>
    </div>
    {{-- Bottom Half Navigation --}}
    <div class="bottom">
        @if(Auth::check())
            <a href="" class="icon" id="notifications_sidebar_button">
                <span class="link_badge">
                    <i class="fa fa-bell"></i>
                    <span class="notification_number_badge">20</span>
                </span>
                <span class="link_text">Notifications</span>
            </a>
            <a href="" class="icon" id="help_sidebar_button">
                <span class="link_badge"><i class="fa fa-question-circle"></i></span>
                <span class="link_text">Help</span>
            </a>
            <a href="" class="icon" id="settings">
                <span class="link_badge"><i class="fa fa-cog"></i></span>
                <span class="link_text">Settings</span>
            </a>
            <a href="" class="{{ $view_service->ref === 'account' ? 'active' : '' }}" id="user_sidebar_button">
                <span class="link_badge">{{ Auth::user()->getInitials() }}</span>
                <span class="link_text">Account</span>
            </a>
        @endif
    </div>
</header>

{{-- Favourite Sidebar --}}
@include('template.sidebar.favourite_sidebar')
{{-- Search Sidebar --}}
@include('template.sidebar.search_sidebar')
{{-- Add Sidebar --}}
@include('template.sidebar.add_sidebar')
{{-- Notification Sidebar --}}
@include('template.sidebar.notification_sidebar')
{{-- Help Sidebar --}}
@include('template.sidebar.help_sidebar')
{{-- User Sidebar --}}
@include('template.sidebar.user_sidebar')

<div class="sidebar_open_overlay"></div>