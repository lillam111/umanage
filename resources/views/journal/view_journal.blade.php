@extends('template.master')
@section('styles')
    <link href="{{ asset('assets/vendor/summernote/summernote-lite.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('views/journal/view_journal.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('scripts')
    <script src="{{ asset('assets/vendor/summernote/summernote-lite.js') }}"></script>
    <script src="{{ asset('views/journal/view_journal.js') }}"></script>
@endsection
@section('body')
    <div class="banner banner_strip uk-flex" uk-sticky>
        <div class="uk-width-expand uk-flex uk-flex-middle">
            <h2>{{ Auth::user()->getFullName() }}'s {{ $date }} Journal</h2>
        </div>
        <div class="uk-width-auto">
            <a href="{{ action('Journal\JournalController@viewJournalGet', [$yesterday->format('Y-m-d')]) }}" class="uk-button uk-icon-button fa fa-chevron-left journal_navigation"></a>
            <a href="{{ action('Journal\JournalController@viewJournalGet', [$tomorrow->format('Y-m-d')]) }}" class="uk-button uk-icon-button fa fa-chevron-right journal_navigation"></a>
            <a class="uk-button uk-icon-button journal_information"><i class="fa fa-info-circle"></i></a>
        </div>
    </div>
    {{-- Editing content of the journal entry today --}}
    <div id="journal"
        data-journal_id="{{ $journal->id }}"
        data-edit_journal_url="{{ action('Journal\JournalController@ajaxEditJournalPost') }}"
        data-delete_journal_url="{{ action('Journal\JournalController@ajaxDeleteJournalPost') }}">
        <div class="journal_content">
            <div class="section">
                <h2>Overall</h2>
                <div class="overall">
                    {!! $journal->overall !== null ? $journal->overall : '<span class="placeholder">Talk about the overall day</span>' !!}
                </div>
                <div class="uk-hidden overall_options">
                    <a class="save_overall uk-button uk-button-small uk-button-primary">Save</a>
                    <a class="cancel_overall uk-button uk-button-small uk-button-danger">Cancel</a>
                </div>
            </div>
            <div class="section">
                <h2>Lowest Part of the Day?</h2>
                <div class="lowest_point">
                    {!! $journal->lowest_point !== null ? $journal->lowest_point : '<span class="placeholder">Talk about the lowest part of the day</span>' !!}
                </div>
                <div class="uk-hidden lowest_point_options">
                    <a class="save_lowest_point uk-button uk-button-primary">Save</a>
                    <a class="cancel_lowest_point uk-button uk-button-danger">Cancel</a>
                </div>
            </div>
            <div class="section">
                <h2>Highest Part of the Day?</h2>
                <div class="highest_point">
                    {!! $journal->highest_point !== null ? $journal->highest_point : '<span class="placeholder">Talk about the highest part of the day</span>' !!}
                </div>
                <div class="uk-hidden highest_point_options">
                    <a class="save_highest_point uk-button uk-button-primary">Save</a>
                    <a class="cancel_highest_point uk-button uk-button-danger">Cancel</a>
                </div>
            </div>
        </div>
        {{--  Journal Achievements... what have i achieved today. --}}
        <div class="journal_sidebar">
            <h2>Rate the day</h2>
            <div class="">
                @for($i = 1; $i < 6; ++$i)
                    <a class="journal_rating fa {{ $journal->rating >= $i ? 'fa-star' : 'fa-star-o' }}" data-rating="{{ $i }}"></a>
                @endfor
            </div>
            <h2>What have I achieved today?</h2>
            <div id="journal_achievements"
                 data-get_journal_achievements_url="{{ action('Journal\JournalAchievementController@ajaxViewJournalAchievementsGet') }}"
                 data-make_journal_achievements_url="{{ action('Journal\JournalAchievementController@ajaxMakeJournalAchievementPost') }}"
                 data-edit_journal_achievements_url="{{ action('Journal\JournalAchievementController@ajaxEditJournalAchievementPost') }}"
                 data-drop_journal_achievements_url="{{ action('Journal\JournalAchievementController@ajaxDeleteJournalAchievementPost') }}"
            ></div>
            <div class="uk-flex">
                <div class="uk-width-expand">
                    <input type="text" class="new_journal_achievement" placeholder="Add New Achievement" />
                </div>
                <div class="uk-width-auto uk-hidden save_new_journal_achievement_wrapper">
                    <a class="fa fa-check uk-button uk-button-primary disabled save_new_journal_achievement"></a>
                </div>
            </div>
            <div class="uk-margin-small-top uk-text-right ">
                <a class="uk-button uk-button-danger delete_journal"><i class="fa fa-trash"></i> Delete This Journal</a>
            </div>
        </div>
    </div>
@endsection