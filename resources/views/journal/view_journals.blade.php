@extends('template.master')
@section('styles')
    <link href="{{ asset('views/journals/view_journals.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('scripts')
    <script src="{{ asset('views/journals/view_journals.js') }}"></script>
@endsection
@section('body')
    <div class="banner banner_strip">
        <h2>{{ Auth::user()->getFullName() }}'s Journals</h2>
    </div>
    {{-- Journals Container --}}
    <div class="uk-container">
        <div class="journal_navigation">
            <div class="uk-flex">
                <div class="uk-width-expand">
                    <a class="journal_calendar_left fa fa-arrow-left"></a>
                </div>
                <div class="uk-width-auto uk-flex uk-flex-middle">
                    <span class="journal_date">{{ $date->format('F Y') }}</span>
                </div>
                <div class="uk-width-expand uk-text-right">
                    <a class="journal_calendar_right fa fa-arrow-right"></a>
                </div>
            </div>
        </div>
        {{-- Ajax View Journals --}}
        <div id="journals"
             data-view_journals_url="{{ action('Journal\JournalController@ajaxViewJournalsGet') }}"
             data-date="{{ $date->format('Y-m') }}">
        </div>
    </div>
@endsection