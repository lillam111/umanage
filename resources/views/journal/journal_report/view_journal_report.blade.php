@extends('template.master')
@section('styles')
    <link href="{{ asset('views/journal_report/view_journal_report.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('scripts')
    <script src="{{ asset('assets/vendor/chart/chart.js') }}"></script>
    <script src="{{ asset('views/journal_report/view_journal_report.js') }}"></script>
@endsection
@section('body')
    <div class="banner banner_strip">
        <h2>{{ Auth::user()->getFullName() }}'s Journal Report</h2>
    </div>
    <div class="uk-container journal_reports"
        data-view_journals_report_url="{{ action('Journal\JournalController@ajaxViewJournalsReportGet') }}"
        data-date="{{ $date->format('Y-m') }}">
        {{-- Navigation: Click events applied to left and right buttons for cycling into the future and past
        dates so that you can view the differences between months --}}
        <div class="journal_report_navigation">
            <div class="uk-flex">
                <div class="uk-width-expand">
                    <a class="journal_report_calendar_left fa fa-arrow-left"></a>
                </div>
                <div class="uk-width-auto uk-flex uk-flex-middle">
                    <span class="journal_report_date">{{ $date->format('F Y') }}</span>
                </div>
                <div class="uk-width-expand uk-text-right">
                    <a class="journal_report_calendar_right fa fa-arrow-right"></a>
                </div>
            </div>
        </div>
        <div class="uk-grid uk-grid-small" uk-grid>
            <div class="uk-width-1-3@m">
                {{-- Reporting: Total Achievements --}}
                <div class="total_achievements box_wrapper">
                    <p><b>Total</b> achievements: <span class="statistic_achievements"></span></p>
                </div>
                {{-- Reporting: Total 1 Star Days --}}
                <div class="total_1_star_days box_wrapper uk-margin-small-top">
                    <p><b>Total</b> 1 Star days: <span class="statistic_1_star_days"></span></p>
                </div>
                {{-- Reporting: Total 2 Star Days --}}
                <div class="total_2_star_days box_wrapper uk-margin-small-top">
                    <p><b>Total</b> 2 Star days: <span class="statistic_2_star_days"></span></p>
                </div>
                {{-- Reporting: Total 3 Star Days --}}
                <div class="total_3_star_days box_wrapper uk-margin-small-top">
                    <p><b>Total</b> 3 Star days: <span class="statistic_3_star_days"></span></p>
                </div>
                {{-- Reporting: Total 4 Star Days --}}
                <div class="total_4_star_days box_wrapper uk-margin-small-top">
                    <p><b>Total</b> 4 Star days: <span class="statistic_4_star_days"></span></p>
                </div>
                {{-- Reporting: Total 5 Star Days --}}
                <div class="total_5_star_days box_wrapper uk-margin-small-top">
                    <p><b>Total</b> 5 Star days: <span class="statistic_5_star_days"></span></p>
                </div>
            </div>
            {{-- Graph: Canvas, Applied attributes for viewing the data url as well as the data that we are
            wanting to check data by. --}}
            <div class="uk-width-2-3@m">
                <div class="uk-grid uk-grid-small" uk-grid>
                    <div class="uk-width-1-2@l">
                        <div class="box_wrapper">
                            <canvas class="journal_report_overview_graph" width="100vh" height="80vh"></canvas>
                        </div>
                    </div>
                    <div class="uk-width-1-2@l">
                        <div class="box_wrapper">
                            <canvas class="journal_report_words_count_graph" width="100vh" height="80vh"></canvas>
                        </div>
                    </div>
                    <div class="uk-width-1-2@l">
                        <div class="box_wrapper">
                            <canvas class="journal_report_rating_graph" width="100vh" height="80vh"></canvas>
                        </div>
                    </div>
                    <div class="uk-width-1-2@l">
                        <div class="box_wrapper">
                            <canvas class="journal_report_achievements_graph" width="100vh" height="80vh"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection