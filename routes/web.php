<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// initial predefined routes for the user to be able to attempt to access the system.
Route::get( '/login',           'User\UserController@viewUserLoginGet');
Route::post('/login',           'User\UserController@viewUserLoginPost');
Route::get( '/logout',          'User\UserController@userLogout');

Route::group(['middleware' => ['auth']], function () {
    // User Routes
    Route::get('/',                                     'User\UserController@viewUserDashboardGet');
    Route::get('/dashboard',                            'User\UserController@viewUserDashboardGet');
    Route::get('/users',                                'User\UserController@viewUsersGet');
    Route::get('/user/{id}',                            'User\UserController@viewUserGet');

    // Project Routes
    Route::get('/projects',                             'Project\ProjectController@viewProjectsGet');
    Route::get('/project/{id}',                         'Project\ProjectController@viewProjectGet');
    Route::get('/ajax/projects',                        'Project\ProjectController@ajaxViewProjectsGet');
    Route::post('/ajax/make/project',                   'Project\ProjectController@ajaxMakeProjectPost');
    // Project Settings Routes
    Route::get('/projects/{id}/settings',               'Project\ProjectSettingController@viewProjectSettingsGet');

    // Task Routes
    Route::get( '/tasks',                               'Task\TaskController@viewTasksGet');
    Route::get( '/task/{code}/{id}',                    'Task\TaskController@viewTaskGet');
    Route::get( '/ajax/search/tasks',                   'Task\TaskController@ajaxSearchTasksGet');
    Route::get( '/ajax/tasks',                          'Task\TaskController@ajaxViewTasksGet');
    Route::post('/ajax/make/task',                      'Task\TaskController@ajaxMakeTaskPost');
    Route::post('/ajax/task/edit',                      'Task\TaskController@ajaxEditTaskPost');

    // Task Comments
    Route::get( '/ajax/view/task/comments',             'Task\TaskCommentController@ajaxViewTaskCommentsGet');
    Route::post('/ajax/make/task/comment',              'Task\TaskCommentController@ajaxMakeTaskCommentPost');
    Route::post('/ajax/delete/task/comment',            'Task\TaskCommentController@ajaxDeleteTaskCommentPost');

    // Task Checklists
    Route::post('/ajax/make/task/checklist',            'Task\TaskChecklistController@ajaxMakeTaskChecklistPost');
    Route::get( '/ajax/view/task/checklists',           'Task\TaskChecklistController@ajaxViewTaskChecklistsGet');
    Route::post('/ajax/edit/task/checklist',            'Task\TaskChecklistController@ajaxEditTaskChecklistPost');
    Route::post('/ajax/delete/task/checklist',          'Task\TaskChecklistController@ajaxDeleteTaskChecklistPost');
    Route::post('/ajax/edit/task_checklist/order',      'Task\TaskChecklistController@ajaxEditTaskChecklistOrderPost');

    // Task Checklist items
    Route::post('/ajax/make/task/checklist_item',       'Task\TaskChecklistItemController@ajaxMakeTaskChecklistItemPost');
    Route::get( '/ajax/view/task/checklist_items',      'Task\TaskChecklistItemController@ajaxViewTaskChecklistItemsGet');
    Route::post('/ajax/check/task_checklist_item',      'Task\TaskChecklistItemController@ajaxCheckTaskChecklistItemPost');
    Route::post('/ajax/edit/task_checklist_item',       'Task\TaskChecklistItemController@ajaxEditTaskChecklistItemPost');
    Route::post('/ajax/delete/task_checklist_item',     'Task\TaskChecklistItemController@ajaxDeleteTaskChecklistItemPost');
    Route::post('/ajax/edit/task_checklist_item/order', 'Task\TaskChecklistItemController@ajaxEditTaskChecklistItemOrderPost');

    // Task Logs
    Route::get('/ajax/view/task/logs',                  'Task\TaskLogController@ajaxViewTaskLogsGet');

    // Journals
    Route::get('/journals',                             'Journal\JournalController@viewJournalsGet');
    Route::get('/journals/report',                      'Journal\JournalController@viewJournalsReportGet');
    Route::get('/ajax/view/journals/report',            'Journal\JournalController@ajaxViewJournalsReportGet');
    Route::get('/ajax/view/journals',                   'Journal\JournalController@ajaxViewJournalsGet');
    Route::get('/journal/{date}',                       'Journal\JournalController@viewJournalGet');
    Route::post('/ajax/journal/edit',                   'Journal\JournalController@ajaxEditJournalPost');
    Route::post('/ajax/delete/journal',                 'Journal\JournalController@ajaxDeleteJournalPost');

    Route::get( '/ajax/view/journal/achievements',      'Journal\JournalAchievementController@ajaxViewJournalAchievementsGet');
    Route::post('/ajax/add/journal/achievement',        'Journal\JournalAchievementController@ajaxMakeJournalAchievementPost');
    Route::post('/ajax/edit/journal/achievement',       'Journal\JournalAchievementController@ajaxEditJournalAchievementPost');
    Route::post('/ajax/delete/journal/achievement',     'Journal\JournalAchievementController@ajaxDeleteJournalAchievementPost');

    // Timelogging
    Route::get( '/timelog',                             'Timelog\TimelogController@viewTimelogCalendarGet');
    Route::get( '/ajax/view/timelogs',                  'Timelog\TimelogController@ajaxViewTimelogsGet');
    Route::get( '/ajax/view/timelogs_calendar',         'Timelog\TimelogController@ajaxViewTimelogsCalendarGet');
    Route::post('/ajax/make/timelog',                   'Timelog\TimelogController@ajaxMakeTimelogPost');
});