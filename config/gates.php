<?php

return [
    // Project Gates
    'App\Gates\Project\ProjectGate@viewProject' => 1,
    'App\Gates\Project\ProjectGate@editProject' => 1,
    // Task Gates
    'App\Gates\Task\TaskGate@viewTask' => 1,
    'App\Gates\Task\TaskGate@editTask' => 1,
    // User Gates
    'App\Gates\User\UserGate@viewUser' => 1,
    'App\Gates\User\UserGate@editUser' => 1,
];