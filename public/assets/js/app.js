/**
* @type {{
*   a: (w.fn.init|jQuery|HTMLElement),
*   c: (w.fn.init|jQuery|HTMLElement),
*   s: (w.fn.init|jQuery|HTMLElement),
*   f: (w.fn.init|jQuery|HTMLElement),
*   h: (w.fn.init|jQuery|HTMLElement),
*   n: (w.fn.init|jQuery|HTMLElement)
*   m: (w.fn.init|jQuery|HTMLElement)
* }} */
window.key_maps = {
    a: { element: $('#user_sidebar_button') },
    n: { element: $('#notifications_sidebar_button') },
    c: { element: $('#add_sidebar_button') },
    s: { element: $('#search_sidebar_button') },
    h: { element: $('#help_sidebar_button') },
    f: { element: $('#favourites_sidebar_button') },
    m: { element: $('#menu_sidebar_button') }
};

/**
* @type {{toolbar: *[][]}}
* we are setting a global default for the summernote objects that we are going to be working with around the system
* anywhere that requires a summernote will be utilising these options so we have a sense of standardisation and not
* re-using and recreating the same set of options on every page that is requiring them.
*/
window.summernote_options = {
    toolbar: [
        ['style', ['bold', 'italic', 'underline', 'clear']],
        ['font', ['strikethrough', 'superscript', 'subscript']],
        ['fontsize', ['fontsize']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']]
    ],
    followingToolbar: false,
};

$(() => {
    // Setting up ajax with the necessary headers so that no matter where im making the ajax call im not going to need
    // to pass in the csrf token every time, this makes ajaxing everywhere much easier to deal with.
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    // defining some global variables that will be used throughout any page that is including this...
    let $html = $('html'),
        $body = $('body');

    // when the user is pressing buttons when anywhere around the system then we are going to be handling this process
    // via the handle_keypress_process function, passing in the event so that we know which key has been processed
    // this can be made more efficient to only attempt to process something that we are wanting to process.
    $body.on('keyup', function (event) {
        handle_keypress_process(event);
    });

    // these buttons are the sidebar buttons that we are pre defining, when are clicking on them we are then going to be
    // deciding which sidebar that these are going to open
    let $buttons = '' +
        '#user_sidebar_button, ' +
        '#help_sidebar_button, ' +
        '#add_sidebar_button, ' +
        '#search_sidebar_button, ' +
        '#favourites_sidebar_button, ' +
        '#notifications_sidebar_button';

    $body.on('click', $buttons, function (event) {
        handle_button_process(event, $(this));
        // this logic set is specifically for the search, if the user has opted to select search (press S), or clicking
        // on this particular element, then the user is going to want quick access for searching after pressing the
        // corresponding button or key...
        if ($(this).attr('id') === 'search_sidebar_button') {
            $('.search_sidebar_input').val('');
            $('.search_sidebar_input').trigger('focus');
        }
    });

    // when the user has opted to click on the html page, and it doesn't really matter where they have clicked
    // then we are going to attempt to remove the open class, from all sidebars... so that they will be then re-hidden
    // this is a makeshift blur for the sidebar...
    $html.on('click', function (event) {
        $('.open').removeClass('open');
        $('.sidebar_open').removeClass('sidebar_open');
    });

    // if the user is clicking within the sidebar, due to the method above, we are attempting to close the sidebar open
    // class from the sidebar, however, if we are clicking inside the sidebar then we are going to want to attempt to
    // keep this particular sidebar in question open...
    $body.on('click', '.sidebar', function (event) {
       event.stopPropagation();
    });

    // this method is for closing the sidebar, inside each sidebar will be a close button that if the user opts to click
    // it will be another method for being able to close the sidebar...
    $body.on('click', '.sidebar_close', function (event) {
        $(this).closest('.sidebar').removeClass('open');
        $('body').removeClass('sidebar_open');
    });

    /** some extra sidebar functionality which will be completely related to creating projects or tasks... */
    // inside the create sidebar, if the user clicks on add new project, then we are going to cycle the user to create
    // a new project... and provide the user with the visuals to be able to do just that...
    $body.on('click', '.sidebar_add_new_project, .sidebar_add_new_task', function (event) {
        let $this = $(this),
            the_class = $this.attr('class').toString().split(' ')[0].replace('sidebar_', '');

        // remove the active class from both of these...
        $('.sidebar_add_new_project').removeClass('active');
        $('.sidebar_add_new_task').removeClass('active');

        // add active to the one we just clicked on.
        $this.addClass('active');

        // after clicking on one we are going to want to find the designated wrapper form and show that so the
        // user is able to interact with being able to add a new form, one will be visible by default, when clicking
        // on one, the other will become visible.
        $('.add_new_project').removeClass('active');
        $('.add_new_task').removeClass('active');
        $(`.${the_class}`).addClass('active');
    });

    // when the user is keying up on the searchbar inside the search sidebar... then we are going to want to capture
    // what button was pressed, and the only two buttons in this case are (escape, enter) if either of these are pressed
    // then some further logic is going to fire off of these events...
    $body.on('keyup', '.search_sidebar_input', function (event) {
        event.stopPropagation();
        if (event.key === 'Escape') {
            $(this).blur();
        }
    });

    // when wea re dealing with ajax requests... there is going to need to be a method of which allows me to close the
    // alert... this will pretty much simply just remove the 'active' class from the ajax message helper container...
    // which will just display none on the message and then when we call this again, the new html will be inserted
    // and the element will be updated with the new html...
    $body.on('click', '.ajax_message_helper_close', function (event) {
        event.preventDefault();
        $('.ajax_message_helper').html('').removeClass('active');
    });

    /**
    * Dropdown functionality.
    */
    $body.on('click', '.dropdown_button', function (event) {
        event.stopPropagation();
        let $this = $(this);
        $this.next().toggleClass('open');
    });

    // when clicking on any element inside the dropdown then we are going to just bubbling up and causing the
    // dropdown closing on itself, meaning that the user can click on elements inside the dropdown.
    $body.on('click', '.dropdown', function (event) {
        event.stopPropagation();
    });

    // when the user has opted to click on save new project, this will run the logic through the (create project) method
    // which will handle all of the checking as to whether or not the details required to create a project has been
    // filled or not, handling the responses in kind depending on what might be missing or yet needed.
    $body.on('click', '.save_new_project', function (event) {
        event.stopPropagation();
        event.preventDefault();
        create_project();
    });

    // when the user has opted to click on save new task, this will run the logic through the (create task) method
    // which will handle all of the checking as to whether or not the details required for creating a new task has been
    // filled or not, handling the responses in kind depending on what might be missing or yet needed.
    $body.on('click', '.save_new_task', function (event) {
        event.stopPropagation();
        event.preventDefault();
        create_task();
    });
});

$.fn.extend({
    placeCursorAtEnd: function() {
        // Places the cursor at the end of a contenteditable container (should also work for textarea / input)
        if (this.length === 0) {
            throw new Error("Cannot manipulate an element if there is no element!");
        }
        var element = this[0],
            range = document.createRange(),
            selection = window.getSelection(),
            childLength = element.childNodes.length;

        if (childLength > 0) {
            var lastNode = element.childNodes[childLength - 1];
            var lastNodeChildren = lastNode.childNodes.length;
            range.setStart(lastNode, lastNodeChildren);
            range.collapse(true);
            selection.removeAllRanges();
            selection.addRange(range);
        }
        return this;
    }
});

/**
* This method is for handling which button we are opting to click on, this will be stripping of the _button from the
* specific button we've clicked on and attempting to find the sidebar that matches the identifier that is closest
* matching against the ID/Class sync. if you have clicked on search_sidebar_button then we'll replace
* search_sidebar_button for search_sidebar and look for an element with the id of search_sidebar and apply the open class
* to this sidebar, to which CSS will take care of the prettified opening.
*
* @param event
* @param $this
*/
let handle_button_process = function (event, $this) {
    event.preventDefault();
    event.stopPropagation();
    $('.open').removeClass('open');
    let target = $this.attr('id').replace('_button', '');
    $(`#${target}`).addClass('open');
    $('body').addClass('sidebar_open');
};

/**
* This function is essentially mapping the key that has been pressed to the window.key_maps and what ever has been
* pressed we will be returning a click that it maps to inside the object, the object will be storing an array of
* jquery button objects that we are going to be clicking. if one existsm then we are simply going to emulate a click
* on the object that will have been found from the map. this will also require the ctrl key to be pressed otherwise
* this method is just going to return null.
*
* @param event
*/
let handle_keypress_process = function (event) {
    if (event.key === 'Escape') {
        $('body').removeClass('sidebar_open');
        $('.open').removeClass('open');
        return;
    }

    if (event.ctrlKey === true && typeof (window.key_maps[event.key]) !== "undefined") {
        window.key_maps[event.key].element.click();
    }
};

/**
* This method is entirely for handling the frontend visuals when we are dealing with responses that come back from ajax.
* this is a prettified alert that will appear on screen when the user has made some changes that did not require a page
* refresh, this method will give the user some feedback that some changes have been made. this is useful for instant
* feedback to the user.
*
* @param $object | Where are we injecting the data to.
* @param data    | The data of which we are injecting into Object.
*/
let ajax_message_helper = function ($object, data) {
    var html = data.response;
        html += '<a class="ajax_message_helper_close"><i class="fa fa-close"></i></a>';

    $object.html(html);
    $object.addClass('active');
    setTimeout(function() {
        $object.removeClass('active');
    }, 1500);
};

/**
* when the user begins to open a summernote object, we are going to check to see if there is a placeholder element inside
* and set it's value to nothing, we are going to delete the object in question from memory, store it's value in the
* cache, so that once we are done editing or messing with this instance, if nothing has changed then we set the value
* back to what the cache value was.
*
* @param key   | The key of which the value will be stored against, subsequently, this will also want to be the class or
*              | Identifier of the element.
* @param value | the value for the key that is being stored.
*/
var handle_summernote_open = function (key, value) {
    let $object = $('.' + key);
    if ($object.find('.placeholder').length >= 1) {
        $object.html('');
    }
    window[key] = value;
    // unset the object, we no longer need it.
    delete $object;
};

/**
* This method will be doing the exact opposite of what the handling of the summernote open does, however this will be
* undoing everything and removing the value from cache, after it has applied that cache to the element in question's
* placeholder element, to simulate that nothing has been changed, and that it is now ready for editing again.
*
* @param $object | jquery object
* @param handle  | point of reference (save or cancel)
* @param key     | the identifier for the content should the content have been saved into the system.
*/
var handle_summernote_leave = function ($object, handle, key, update_on_save = true) {
    // regardless of whether or not we are saving or cancelling, we are wanting to reset the value of the object
    // back to its' original value... on the exit of the summernote, the opbject will be set to the previous value
    // and the cache key will be set to nothing.
    if (update_on_save === true) {
        $object.html(window[key]);
    }
    delete window[key];
    delete $object;
};

/**
* Cache functionality.
*/

/**
* This method will be acquiring elements from cache that we have stored, cache_get('storage_key'); will return whatever
* has been stored within the storage_key value. everything will have been stored within the window object.
*
* @param key
* @return {*}
*/
let cache_get = function (key) {
    return window[key];
};

/**
* This method will be saving a value into a key identifier that is set by the developer, if we are opting to save a
* particular value, then we are going to need the key as well as the value. cache_save('key', 1);
* cache_get('key') will print 1, all this method does is storing the value into the window object.
*
* @param key
* @param value
* @return void
*/
let cache_save = function (key, value) {
    window[key] = value;
};

/**
* We are going to need a method for deleting the cache objects when we are no longer needing to store the information
* there's no sense in storing a mass amount of data into the window object, so this method will simply unset and delete
* the cache key so that we are no longer storing that value anymore. as referencing the above method:
* cache_delete('key'); and cache_get('key') will output undefined.
*
* @param key
* @return void
*/
let cache_delete = function (key) {
    delete window[key];
};

/**
* Method for creating a project, if the user clicks on the create project button, this method will take care of all the
* processing for handling the details, if there are any details that haven't been filled then the system will handle it
* here, otherwise, this will collect all the data we need to push up to the database and then query from that.
*
* @return void
*/
var create_project = function () {
    let $add_sidebar = $('#add_sidebar'),
        make_project_url = $add_sidebar.data('make_project_url'),
        $project_name = $add_sidebar.find('.project_name'),
        $project_description = $add_sidebar.find('.project_description'),
        $project_code = $add_sidebar.find('.project_code'),
        $project_color = $add_sidebar.find('.project_color'),
        $project_icon = $add_sidebar.find('.project_icon');

    $.ajax({
        method: 'post',
        url: make_project_url,
        data: {
            name: $project_name.val(),
            description: $project_description.val(),
            code: $project_code.val(),
            color: $project_color.val(),
            icon: $project_icon.val()
        },
        success: function (data) {
            // if we are on a page that has the function of (view_projects) defined then we are going to want to
            // call it so that the projects are showing the up to date version of all projects that are in the system
            // ready for this particular user.
            if (typeof (view_projects) !== "undefined") {
                view_projects();
            }

            // hiding the sidebar after we have saved the specific project.
            $add_sidebar.removeClass('open');

            // after we have hidden the sidebar we are going to want to clear the inputs of any current values so that
            // the user in question will be able to add another project without having to delete the values themselves.
            $project_name.val('');
            $project_description.val('');
            $project_code.val('');
            $project_color.val('');
            $project_icon.val('');
        }
    });
};

/**
* Method for creating a task, if the user clicks on the create task button, this method will take care of the processing
* for handling the details. If there are any details that haven't been filled then the system will handle it here.
* otherwise, this will collect all teh data that we are needing to push up to the database and query from that.
*/
var create_task = function () {
    let $add_sidebar = $('#add_sidebar'),
        make_task_url = $add_sidebar.data('make_task_url'),
        $task_name = $('.task_name'),
        $task_description = $('.task_description'),
        $task_project = $('.task_project');

    $.ajax({
        method: 'post',
        url: make_task_url,
        data: {
            name: $task_name.val(),
            description: $task_description.val(),
            project_id: $task_project.val()
        },
        success: function (data) {
            if (typeof (view_projects) !== "undefined") {
                view_projects();
            }

            if (typeof (view_tasks) !== "undefined") {
                view_tasks();
            }

            $task_name.val('');
            $task_description.val('');
            $task_project.val('');
        }
    })
};