$(() => {
    let $body = $('body');

    // when the page loads and there is a $('.tasks') object present in the dom... then we are going to be making a
    // a query to the database which will look for all tasks against the parameters that are set initially.
    view_tasks();

    $body.on('click', '.tasks .paginate_previous, .tasks .paginate_next', function (event) {
        let $this = $(this),
            direction = $this[0].classList[0];
        view_tasks(direction.split('paginate_')[1]);
    });
});

/**
* Default method for collecting tasks from the database, this will fire a request to the TaskController which will check
* what variables are currently existing on the page. the current variables of which will be utilised and needed are:
*
*  (string) title
*     (int) project_id.
*  (string) task_statuses
*  (string) task_issue_types
*  (string) task_priorities
*  (string) search
* (boolean) pagination
* (integer) items_per_page
* (integer) page
*
* Assuming these variables are present the system will utilise them otherwise they will be inserted as null and blank
* values and in essence, be ignored.
*
* @param direction
*/
var view_tasks = function (direction = false) {
    let $tasks = $('.tasks'),
        url = $tasks.data('get_tasks_url'),
        page = parseInt($tasks.attr('data-page'));

    if (direction === 'previous' && page !== 0) page -= 1;
    if (direction === 'next') page += 1;

    $tasks.attr('data-page', page);

    if (typeof (url) !== "undefined" || url !== '') {
        $.ajax({
            method: 'get',
            url: url,
            data: {
                title: $tasks.data('title'),
                project_id: $tasks.data('project_id'),
                task_statuses: $('#task_statuses').val(),
                task_issue_types: $('#task_issue_types').val(),
                task_priorities: $('#task_priorities').val(),
                search: $('#task_search').val(),
                pagination: $tasks.data('pagination'),
                items_per_page: parseInt($tasks.data('items_per_page')),
                page: page
            },
            success: function (data) {
                $tasks.html(data);
            }
        });

        // if we have managed to accomplish the above, then we are going to want to return from this method here, so
        // that the console log below is never achieved. (if it does, then the url has not been set)
        return;
    }

    // we shouldn't get here providing that the url exists.
    console.log('The url was not found, check to see if there is $(.tasks) html dom element');
};