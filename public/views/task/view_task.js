$(() => {
    let $body = $('body');

    $body.on('keypress', '.task_name', function (event) {
        if (event.key === 'Enter') {
            event.preventDefault();
            update_task('name', $(this).text().trim());
        }
    });

    // when clicking on the description, we are going to be instantiating summernote for the user to begin start making
    // amends to the description of the task, clicking on description will also look to it's parent element and find the
    // options for the description and unhide them as they are hidden by default, we don't need to see these to begin
    // with as this will be wasting space, and rather than saving on blur, (which to me feels like a stupid functionality
    // it was best to offer the user the option to cancel or save their changes)
    $body.on('click', '.description', function (event) {
        let $this = $(this);

        handle_summernote_open('description', $this.html().trim());

        $this.parent().find('.description_options').removeClass('uk-hidden');

        $this.summernote(window.summernote_options).next().find('.note-editable').placeCursorAtEnd();
    });

    // this set of logic is devoted to handling the process logic for description editing, if you are to click cancel,
    // it will fire off a summernote leave function... which will simply put the content back to what it was prior to
    // clicking on it, however if you are to click submit, then it will proceed to editing the task in question and then.
    $body.on('click', '.save_description, .cancel_description', function (event) {
        event.preventDefault();
        let $this = $(this),
            $description = $('.description'),
            handle = $this.hasClass('cancel') ? 'cancel' : 'save',
            field = 'description',
            update_on_save = $this.hasClass('cancel') ? true : false;

        $description.summernote('destroy');

        $this.parent().addClass('uk-hidden');

        // if the element has the class of cancel, then we are simply going to want to pass in the summernote object
        // so that we are able to do something particular with it, and revert back to what the content once used to be
        // rather than viewing what the content is currently set to be.
        handle_summernote_leave($description, handle, field, update_on_save);

        // if the user has opted to press on save, rather than cancel then we are going to look to saving these changes
        // in the database after we have uninstantiated the summernote object, so that we can take the html of the
        // description jquery object.
        if ($this.hasClass('save_description')) {
            update_task(field, $description.html());
        }
    });

    $body.on('click', '.task_info', function (event) {
        let $task = $('#task');

        if (window.innerWidth <= 991) {
            $task.removeClass('task_sidebar_closed');
            $task.toggleClass('task_sidebar_open');
        }

        if (window.innerWidth > 991) {
            $task.toggleClass('task_sidebar_closed');
            $task.removeClass('task_sidebar_open');
        }
    });

    // when the page loads, we are going to want to load in the task widgets.
    // currently we are going to be loading in the following:
    // task comments
    // task checklists
    // task logs
    view_task_comments();
    view_task_checklists();
    view_task_logs();
});

/**
* Method for handling the process of updating a task, this will simply only the field and the value that we are hoping
* to pass through to the controller to process. if there needs to be a callback that happens after this has been
* completed then we are able to do so.
*
* @param field    (string)
* @param value    (variable)
* @param callback (function)
*/
var update_task = function (field, value, callback = null) {
    let $task = $('#task'),
        url = $task.data('edit_task_url'),
        task_id = $task.data('task_id');

    $.ajax({
        method: 'post',
        url: url,
        data: {
            task_id: task_id,
            field: field,
            value: value
        },
        success: function (data) {
            ajax_message_helper($('.ajax_message_helper'), data);
            if (callback !== null) {
                callback();
            }

            view_task_logs();
        }
    });
};