$(() => {
    view_projects();
});

var view_projects = function () {
    let $projects_wrapper = $('.projects'),
        view_projects_url = $projects_wrapper.data('view_projects_url');

    $.ajax({
        method: 'get',
        url: view_projects_url,
        data: {
            title: $projects_wrapper.data('title')
        },
        success: function (data) {
            $projects_wrapper.html(data);
        }
    });
};